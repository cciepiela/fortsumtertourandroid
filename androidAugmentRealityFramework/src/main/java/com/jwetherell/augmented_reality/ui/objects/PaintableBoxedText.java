package com.jwetherell.augmented_reality.ui.objects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jwetherell.augmented_reality.R;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * This class extends PaintableObject to draw text with a box surrounding it.
 *
 * @author Justin Wetherell <phishman3579@gmail.com>
 */
public class PaintableBoxedText extends PaintableObject {

    private float width = 0, height = 0;
    private float areaWidth = 0, areaHeight = 0;
    private ArrayList<CharSequence> lineList = null;
    private float linesHeight = 0;
    private float maxLinesWidth = 0;
    private float pad = 0;

    private CharSequence txt = null;
    private float fontSize = 12;
    private int borderColor = Color.rgb(255, 255, 255);
    private int backgroundColor = Color.rgb(0, 0, 0);
    private int textColor = Color.rgb(32, 98, 159);
    private int titleBackgroundColor = Color.rgb(255, 255, 255);

    View annotationView;
    private String title;
    private String description;
    private byte[] poiImageData;

    private Context mContext;
    Map<Integer, int[]> dimensions = new HashMap<Integer, int[]>() {{
        put(R.id.poi_title, new int[]{16, 20, 176, 24});
        put(R.id.poi_description, new int[]{0, 68, 220, 172});
        put(R.id.poi_read_more, new int[]{0, 240, 220, 40});
    }};


    public PaintableBoxedText(Context context, CharSequence txtInit, String description, byte[] poiImageData, float fontSizeInit, float maxWidth) {
        this(context, txtInit, description, poiImageData, fontSizeInit, maxWidth, Color.rgb(255, 255, 255), Color.argb(128, 0, 0, 0), Color.rgb(255, 255, 255));
    }

    public PaintableBoxedText(Context context, CharSequence txtInit, String description, byte[] poiImageData, float fontSizeInit, float maxWidth, int borderColor, int bgColor, int textColor) {
        set(context, txtInit, description, poiImageData, fontSizeInit, maxWidth, borderColor, bgColor, textColor);
        title = txtInit.toString();
        this.description = description;
    }

    /**
     * Set this objects parameters. This should be used instead of creating new
     * objects.
     *
     * @param txtInit      CharSequence to paint.
     * @param fontSizeInit Font size to use.
     * @param maxWidth     max width of the text.
     * @param borderColor  Color of the border.
     * @param bgColor      Background color of the surrounding box.
     * @param textColor    Color of the text.
     * @throws NullPointerException if String param is NULL.
     */
    public void set(Context context, CharSequence txtInit, String description, byte[] poiImageData, float fontSizeInit, float maxWidth, int borderColor, int bgColor, int textColor) {
        if (txtInit == null) throw new NullPointerException();
        mContext = context;
        this.borderColor = borderColor;
        this.backgroundColor = bgColor;
        this.textColor = textColor;
        this.pad = getTextAsc();
        this.poiImageData = poiImageData;


        set(txtInit, fontSizeInit, maxWidth);
    }

    /**
     * Set this objects parameters. This should be used instead of creating new
     * objects. Note: This uses previously set or default values for border
     * color, background color, and text color.
     *
     * @param txtInit      CharSequence to paint.
     * @param fontSizeInit Font size to use.
     * @param maxWidth     max width of the text.
     * @throws NullPointerException if String param is NULL.
     */
    public void set(CharSequence txtInit, float fontSizeInit, float maxWidth) {
        if (txtInit == null) throw new NullPointerException();

        try {
            calcBoxDimensions(txtInit, fontSizeInit, maxWidth);
        } catch (Exception ex) {
            ex.printStackTrace();
            calcBoxDimensions("TEXT PARSE ERROR", 12, 200);
        }
    }

    private void calcBoxDimensions(CharSequence txtInit, float fontSizeInit, float maxWidth) {
        if (txtInit == null) throw new NullPointerException();

        setFontSize(fontSizeInit);

        txt = txtInit;
        fontSize = fontSizeInit;
        areaWidth = maxWidth - pad;
        linesHeight = getTextAsc() + getTextDesc();

        if (lineList == null) lineList = new ArrayList<CharSequence>();
        else lineList.clear();

        BreakIterator boundary = BreakIterator.getWordInstance();
        boundary.setText(txt.toString());

        int start = boundary.first();
        int end = boundary.next();
        int prevEnd = start;
        while (end != BreakIterator.DONE) {
            CharSequence line = txt.subSequence(start, end);
            CharSequence prevLine = txt.subSequence(start, prevEnd);
            float lineWidth = getTextWidth(line, 0, line.length());

            if (lineWidth > areaWidth) {
                // If the first word is longer than lineWidth
                // prevLine is empty and should be ignored
                if (prevLine.length() > 0) lineList.add(prevLine);

                start = prevEnd;
            }

            prevEnd = end;
            end = boundary.next();
        }
        CharSequence line = txt.subSequence(start, prevEnd);
        lineList.add(line);

        maxLinesWidth = 0;
        for (CharSequence seq : lineList) {
            float lineWidth = getTextWidth(seq, 0, seq.length());
            if (maxLinesWidth < lineWidth) maxLinesWidth = lineWidth;
        }
        areaWidth = maxLinesWidth;
        areaHeight = linesHeight * lineList.size();

        width = areaWidth + pad * 2;
        height = areaHeight + pad * 2;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void paint(Canvas canvas) {
        if (canvas == null) throw new NullPointerException();
        //theirCode(canvas);
        myCode(canvas);

    }

    private void myCode(Canvas canvas) {

        canvas.save();
        setFill(true);
        LayoutInflater inflater = (LayoutInflater) mContext.getApplicationContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        if (annotationView == null) {
            annotationView = (RelativeLayout) inflater.inflate(R.layout.annotation_view, null);
//            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) width, 220);
//            annotationView.setLayoutParams(layoutParams);
            Log.d("MyTAG", "ANNOTATION_VIEW: " + annotationView);
            TextView titleTextView = (TextView) annotationView.findViewById(R.id.poi_title);
            TextView descriptionTextView = (TextView) annotationView.findViewById(R.id.poi_description);
            ImageView poiImageView = (ImageView) annotationView.findViewById(R.id.poi_image);
            TextView readMoreTextView = (TextView) annotationView.findViewById(R.id.poi_read_more);

            titleTextView.setText(title);
            descriptionTextView.setText(description);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            Bitmap bmp = BitmapFactory.decodeByteArray(poiImageData, 0, poiImageData.length, options);
            poiImageView.setImageBitmap(bmp);
            int containerWidth = 800;
            int containerHeight = getPixels(400);

            Log.d("ArTag", "titleTextView's width, height are: " + titleTextView.getWidth() + ", " + titleTextView.getHeight());
            Log.d("ArTag", "ImageView's id is: " + poiImageView.getId());

            annotationView.layout(containerWidth, containerHeight, containerWidth, containerHeight);
            titleTextView.layout(
                    0,
                    0,
                    getPixels(180),
                    getPixels(20)
            );
            titleTextView.offsetLeftAndRight(getPixels(200));
            titleTextView.setPadding(4, 0, 0, 0);
            poiImageView.layout(
                    0,
                    0,
                    getPixels(200),
                    getPixels(200)
            );
            descriptionTextView.layout(
                    0,
                    0,
                    getPixels(180),
                    getPixels(150)
            );
            descriptionTextView.setEllipsize(TextUtils.TruncateAt.END);
            descriptionTextView.offsetLeftAndRight(getPixels(200));
            descriptionTextView.setPadding(4, 0, 0, 0);
            descriptionTextView.offsetTopAndBottom(getPixels(20));
            readMoreTextView.layout(
                    0,
                    0,
                    getPixels(180),
                    getPixels(30)
            );
            readMoreTextView.offsetTopAndBottom(getPixels(170));
            readMoreTextView.offsetLeftAndRight(getPixels(200));
        }
        canvas.save();
        canvas.translate(-width / 2, -height / 2);
        annotationView.draw(canvas);
        canvas.restore();

//        canvas.save();
//        canvas.translate(-width/2, -height/2);
//        setFill(true);
//        setColor(backgroundColor);
//        //paintRect(canvas, x, y, width, height);
//
//        setColor(titleBackgroundColor);
//
//        int titleHeight = 100;
//        paintRect(canvas, x, y, width, titleHeight);
////
//        setFill(true);
//        setStrokeWidth(0);
//        setColor(Color.rgb(32, 98, 159));
//        paint.setTextAlign(Paint.Align.CENTER);
//        paintText(canvas, x, y, "Hello", 0, "Hello".length());

    }

    private int getPixels(int fromDPs) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, fromDPs, mContext.getResources().getDisplayMetrics());
    }

    private void theirCode(Canvas canvas) {
        canvas.save();
        canvas.translate(-width / 2, -height / 2);

        setFontSize(fontSize);

        setFill(true);
        setColor(backgroundColor);
        paintRoundedRect(canvas, x, y, width, height);

        setFill(false);
        setColor(borderColor);
        paintRoundedRect(canvas, x, y, width, height);

        float lineX = x + pad;
        float lineY = y + pad + getTextAsc();
        int i = 0;
        for (CharSequence line : lineList) {
            setFill(true);
            setStrokeWidth(0);
            setColor(textColor);
            paintText(canvas, lineX, lineY + (linesHeight * i), line, 0, line.length());
            i++;
        }

        canvas.restore();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public float getWidth() {
        return width;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public float getHeight() {
        return height;
    }
}
