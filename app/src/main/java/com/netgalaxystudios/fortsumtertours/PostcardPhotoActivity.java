package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.cameraview.CameraView;

import java.util.ArrayList;

public class PostcardPhotoActivity extends AppCompatActivity {

    private CameraView cameraView;
    private RecyclerView recyclerView;
    private RelativeLayout confirmLayout;
    private Button takePictureButton;
    private ImageView flipButton;
    private byte[] pictureData;
    private Button cancelButton;
    private Button checkButton;
    private RelativeLayout cameraContainerLayout;
    ArrayList<PostcardFilter> filters = new ArrayList<>();
    private FilterAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    private SnapHelper snapHelper;
    int recyclerPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        setContentView(R.layout.activity_postcard_photo);

        connectUI();
        setListeners();

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new FilterAdapter(this);
        recyclerView.setAdapter(adapter);
        snapHelper = new LinearSnapHelper(){
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                recyclerPosition = super.findTargetSnapPosition(layoutManager, velocityX, velocityY);
                return recyclerPosition;
            }
        };
        snapHelper.attachToRecyclerView(recyclerView);
        recyclerView.setHasFixedSize(true);
        PostcardFilter.getFilters(new PostcardFilter.GetFiltersCallback() {
            @Override
            public void gotFilters(ArrayList<PostcardFilter> filters) {
                PostcardPhotoActivity.this.filters = filters;
                adapter.notifyDataSetChanged();
            }
        });



    }

    private void setListeners () {
        cameraView.addCallback(new CameraView.Callback() {
            @Override
            public void onPictureTaken(CameraView cameraView, byte[] data) {
                super.onPictureTaken(cameraView, data);
                pictureTaken(data);
            }
        });

        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePictureButtonPressed();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupContainers();
            }
        });
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pictureData != null && filters.size() > 0 && recyclerPosition < filters.size()) {
                    //OOMS at line below
                    Bitmap oldBitmap = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.length);
                    Bitmap fixedBitmap = MethodHelper.fixOrientation(oldBitmap, getResources().getConfiguration().orientation);
                    BitmapDrawable ob = new BitmapDrawable(getResources(), fixedBitmap);
                    cameraContainerLayout.setBackground(ob);

//                    if (MethodHelper.saveBitmapToDevice(MethodHelper.getBitMapFromView(cameraContainerLayout), getContentResolver())) {
//                        MethodHelper.showAlert(PostcardPhotoActivity.this, "Success", "Your image has been saved to the gallery.");
//                    }else {
//                        MethodHelper.showAlert(PostcardPhotoActivity.this, "Error", "There was an error saving your photo to the gallery. Please try again.");
//                    }


                }

            }
        });
        flipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pictureData != null) {
//                    ShareIntentBuilder.from(PostcardPhotoActivity.this)
//                            .ignoreSpecification()
//                            .text("Check out the Fort Sumter Tours app! - " + Config.getCurrentConfig().shareUrl)
//                            .stream(MethodHelper.getImageUri(PostcardPhotoActivity.this, MethodHelper.getBitMapFromView(cameraContainerLayout)))
//                            .share();
                }else {
                    if (cameraView.getFacing() == CameraView.FACING_BACK) {
                        cameraView.setFacing(CameraView.FACING_FRONT);
                    }else {
                        cameraView.setFacing(CameraView.FACING_BACK);
                    }
                }
            }
        });
    }

    private void setupContainers () {
        pictureData = null;
        if (!cameraView.isCameraOpened()) {
            cameraView.start();
        }
        recyclerView.setVisibility(View.INVISIBLE);
        confirmLayout.setVisibility(View.INVISIBLE);
        takePictureButton.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            flipButton.setImageDrawable(getDrawable(R.drawable.camera_flip));
        }
    }

    private void connectUI () {
        cameraContainerLayout = (RelativeLayout)findViewById(R.id.camera_container_layout);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        confirmLayout = (RelativeLayout)findViewById(R.id.confirm_layout);
        flipButton = (ImageView)findViewById(R.id.flip_button);
        cameraView = (CameraView)findViewById(R.id.camera);
        takePictureButton = (Button)findViewById(R.id.camera_button);
        ImageView backButton = (ImageView)findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        cancelButton = (Button)findViewById(R.id.cancel_button);
        checkButton = (Button)findViewById(R.id.check_button);
    }

    private void takePictureButtonPressed () {
        Log.d(Constants.TAG, "takePictureButtonPressed");
        cameraView.takePicture();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupContainers();
        Log.d(Constants.TAG, "CAMERA: "+cameraView);

    }

    @Override
    protected void onPause() {
        cameraView.stop();
        super.onPause();
    }

    private void pictureTaken (byte[] data) {
        Log.d(Constants.TAG, "PICTURE_TAKEN: "+data);
        if (data != null) {
            if (cameraView.isCameraOpened()){
                Log.d(Constants.TAG, "in pictureTaken camera was opened and .stop() is being called!");
            }
            pictureData = data;
//            cameraView.stop();
            recyclerView.setVisibility(View.VISIBLE);
            confirmLayout.setVisibility(View.VISIBLE);
            takePictureButton.setVisibility(View.INVISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                flipButton.setImageDrawable(getDrawable(R.drawable.ic_share));
            }
        }
    }

    class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolder> {
        private Activity context;

        FilterAdapter(Activity context) {
            this.context = context;
        }

        @Override
        public int getItemCount() {
            return filters.size();
        }

        @Override
        public FilterAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.postcard_filter_recycler_item, parent, false);
            return new FilterAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final FilterAdapter.ViewHolder holder, int position) {
            PostcardFilter filter = null;
            filter = filters.get(position);
            filter.addToView(holder.container, PostcardPhotoActivity.this);
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            RelativeLayout container;

            ViewHolder(final View itemView) {
                super(itemView);
                container = (RelativeLayout)itemView.findViewById(R.id.container);

            }
        }
    }
}
