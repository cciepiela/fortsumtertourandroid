package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by coryciepiela on 3/24/17.
 */

class AnimalAdapter extends RecyclerView.Adapter<AnimalAdapter.ViewHolder>{
    private Activity context;
    ArrayList<Animal> searchResults = new ArrayList<Animal>();

    AnimalAdapter(Activity context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    @Override
    public AnimalAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.animal_recycler_item, parent, false);
        return new AnimalAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final AnimalAdapter.ViewHolder holder, int position) {
        final Animal animal = searchResults.get(position);
        holder.mAnimalPicture.setImageBitmap(null);
        holder.mAnimalTitle.setText(animal.title);
        animal.getAnimalImage(new MethodHelper.ImageCallback() {
            @Override
            public void gotImage(byte[] image) {
                Glide.with(context)
                        .load(image)
                        .centerCrop()
                        .into(holder.mAnimalPicture);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AnimalDetailsActivity.displayAnimalDetailsActivity(animal, context);
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mAnimalTitle;
        private final ImageView mAnimalPicture;

        ViewHolder(final View itemView) {
            super(itemView);
            mAnimalTitle = (TextView)itemView.findViewById(R.id.animal_recycler_title);
            mAnimalPicture = (ImageView)itemView.findViewById(R.id.animal_recycler_picture);
        }
    }
}
