package com.netgalaxystudios.fortsumtertours;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class TimeMachineSelectionActivity extends AppCompatActivity {

    private ImageView mBackButton;
    private TimeMachineAdapter mAdapter = new TimeMachineAdapter(TimeMachineSelectionActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_machine_selection);

        connectUi();
    }

    private void connectUi() {
        mBackButton = (ImageView)findViewById(R.id.back_button);
        TextView headerTitleText = (TextView)findViewById(R.id.header_title_text);
        MethodHelper.setTextViewTypeFaceRegular(headerTitleText, TimeMachineSelectionActivity.this);
        RecyclerView timeMachineRecycler = (RecyclerView)findViewById(R.id.time_machine_recycler_view);
        timeMachineRecycler.setLayoutManager(new LinearLayoutManager(TimeMachineSelectionActivity.this));
        timeMachineRecycler.setAdapter(mAdapter);
        setListeners();
        populateUi();
    }

    private void populateUi(){
        MethodHelper.showProgressHUDWithMessage("Loading...", TimeMachineSelectionActivity.this);
        TimeMachine.getTimeMachines(new TimeMachine.TimeMachineCallback() {
            @Override
            public void gotTimeMachines(ArrayList<TimeMachine> timeMachines) {
                if (timeMachines.size() > 0){
                    mAdapter.searchResults = timeMachines;
                    mAdapter.notifyDataSetChanged();
                    MethodHelper.hideHUD();
                }
            }
        });
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
