package com.netgalaxystudios.fortsumtertours

import android.app.IntentService
import android.app.Notification
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.util.Log

import com.google.android.gms.location.GeofencingEvent

class GeofenceTransitionIntenService : IntentService("DisplayNotification") {

    override fun onHandleIntent(intent: Intent?) {
        val geofencingEvent = GeofencingEvent.fromIntent(intent)
        Log.d(Constants.TAG, "onHandleIntent: $geofencingEvent")
        val requestId = geofencingEvent?.triggeringGeofences?.firstOrNull()?.requestId ?: return
        println("REQUEST_ID: " + requestId);

        val title = SharedPreferencesHelper.getTitle(requestId)
        val message = SharedPreferencesHelper.getMessage(requestId)
        MethodHelper.fireLocalNotification(this, 123, title, message, null)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val geofencingEvent = GeofencingEvent.fromIntent(intent)
        Log.d(Constants.TAG, "onHandleIntent: $geofencingEvent")
        val requestId = geofencingEvent?.triggeringGeofences?.firstOrNull()?.requestId ?: return 0
        println("REQUEST_ID: " + requestId);

        val title = SharedPreferencesHelper.getTitle(requestId)
        val message = SharedPreferencesHelper.getMessage(requestId)
        MethodHelper.fireLocalNotification(this, 123, title, message, null)
        return Service.START_STICKY
    }
}
