package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

public class WebActivity extends AppCompatActivity {
    private ImageView backButtonLayout;
    private WebView mWebview;
    private TextView mTitleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        connectUI();
        setListeners();
        loadURL();
    }

    public static void displayWebActivity (Activity activity, String url, String title) {
        Intent i = new Intent(activity, WebActivity.class);
        i.putExtra(Constants.Extras.URL_EXTRA, url);
        i.putExtra(Constants.Extras.TITLE_EXTRA, title);
        activity.startActivity(i);
    }

    private void connectUI () {
        backButtonLayout = (ImageView) findViewById(R.id.back_button);
        mTitleTextView = (TextView)findViewById(R.id.title_text_view);
        mTitleTextView.setText(getIntent().getExtras().getString(Constants.Extras.TITLE_EXTRA));
        mWebview = (WebView)findViewById(R.id.web_view);
        WebSettings webSettings = mWebview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebview.setWebViewClient(new WebActivity.MyWebClient());

    }

    private void setListeners () {
        backButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void loadURL () {
        String url = getIntent().getStringExtra(Constants.Extras.URL_EXTRA);
        if (url != null) {
            mWebview.loadUrl(url);

        }
    }

    class MyWebClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            MethodHelper.showProgressHUDWithMessage("Loading...", WebActivity.this);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            MethodHelper.hideHUD();
        }
    }
}
