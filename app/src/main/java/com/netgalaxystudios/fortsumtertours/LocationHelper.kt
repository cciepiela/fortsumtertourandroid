package com.netgalaxystudios.fortsumtertours

import android.content.Context
import android.content.pm.PackageManager
import android.location.*
import android.os.Bundle
import android.support.v4.content.ContextCompat
import java.io.IOException
import java.util.*

/**
 * Created by charlesvonlehe on 12/11/17.
 */

class LocationHelper constructor(context:Context):LocationListener {


    private var locationManager:LocationManager? = null

    init {
        locationManager = context.getSystemService(Context.LOCATION_SERVICE) as? LocationManager
        if (locationManager != null) {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && locationManager!!.allProviders.contains(LocationManager.NETWORK_PROVIDER)) {
                println("INITIALIZED_LOCATION_MANAGER: " + locationManager)
                locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1.toLong(), 1.toFloat(), this)
            }
        }
    }

    companion object {
        private var sharedInstance: LocationHelper? = null
        private var currentLoc:Location? = null

        fun shared(context: Context): LocationHelper {
            if (sharedInstance == null) {
                sharedInstance = LocationHelper(context)
            }
            return sharedInstance!!
        }

        fun getCityAndCountry (fromLocation: Location?, context: Context):String {

            val address = getAddress(fromLocation, context)
            var cityAndState = ""
            if (address != null) {
                cityAndState = address.locality + ", " + address.countryName
            }
            return cityAndState
        }

        fun getSubArea (fromLocation: Location?, context: Context):String? {
            val address = getAddress(fromLocation, context)
            return address?.subAdminArea
        }

        fun getCountry(fromLocation: Location?, context: Context):String? {
            val address = getAddress(fromLocation, context)
            return address?.countryName
        }

        fun getLocality(fromLocation: Location?, context: Context):String? {
            val address = getAddress(fromLocation, context)
            return address?.locality
        }

        fun getArea(fromLocation: Location?, context: Context):String? {
            val address = getAddress(fromLocation, context)
            return address?.adminArea
        }

        private fun getAddress (fromLocation: Location?, context: Context):Address? {
            val geocoder = Geocoder(context, Locale.getDefault())
            if (fromLocation != null) {
                try {
                    val addresses = geocoder.getFromLocation(fromLocation.latitude, fromLocation.longitude, 1)
                    val address = addresses[0]
                    return address
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
            return null
        }


        fun getCityAndStateFromLocation(context: Context, location: Location?): String {
            val address = getAddress(location, context)
            var cityAndState = ""
            val states = HashMap<String, String>()
            states.put("Alabama", "AL")
            states.put("Alaska", "AK")
            states.put("Alberta", "AB")
            states.put("American Samoa", "AS")
            states.put("Arizona", "AZ")
            states.put("Arkansas", "AR")
            states.put("Armed Forces (AE)", "AE")
            states.put("Armed Forces Americas", "AA")
            states.put("Armed Forces Pacific", "AP")
            states.put("British Columbia", "BC")
            states.put("California", "CA")
            states.put("Colorado", "CO")
            states.put("Connecticut", "CT")
            states.put("Delaware", "DE")
            states.put("District Of Columbia", "DC")
            states.put("Florida", "FL")
            states.put("Georgia", "GA")
            states.put("Guam", "GU")
            states.put("Hawaii", "HI")
            states.put("Idaho", "ID")
            states.put("Illinois", "IL")
            states.put("Indiana", "IN")
            states.put("Iowa", "IA")
            states.put("Kansas", "KS")
            states.put("Kentucky", "KY")
            states.put("Louisiana", "LA")
            states.put("Maine", "ME")
            states.put("Manitoba", "MB")
            states.put("Maryland", "MD")
            states.put("Massachusetts", "MA")
            states.put("Michigan", "MI")
            states.put("Minnesota", "MN")
            states.put("Mississippi", "MS")
            states.put("Missouri", "MO")
            states.put("Montana", "MT")
            states.put("Nebraska", "NE")
            states.put("Nevada", "NV")
            states.put("New Brunswick", "NB")
            states.put("New Hampshire", "NH")
            states.put("New Jersey", "NJ")
            states.put("New Mexico", "NM")
            states.put("New York", "NY")
            states.put("Newfoundland", "NF")
            states.put("North Carolina", "NC")
            states.put("North Dakota", "ND")
            states.put("Northwest Territories", "NT")
            states.put("Nova Scotia", "NS")
            states.put("Nunavut", "NU")
            states.put("Ohio", "OH")
            states.put("Oklahoma", "OK")
            states.put("Ontario", "ON")
            states.put("Oregon", "OR")
            states.put("Pennsylvania", "PA")
            states.put("Prince Edward Island", "PE")
            states.put("Puerto Rico", "PR")
            states.put("Quebec", "PQ")
            states.put("Rhode Island", "RI")
            states.put("Saskatchewan", "SK")
            states.put("South Carolina", "SC")
            states.put("South Dakota", "SD")
            states.put("Tennessee", "TN")
            states.put("Texas", "TX")
            states.put("Utah", "UT")
            states.put("Vermont", "VT")
            states.put("Virgin Islands", "VI")
            states.put("Virginia", "VA")
            states.put("Washington", "WA")
            states.put("West Virginia", "WV")
            states.put("Wisconsin", "WI")
            states.put("Wyoming", "WY")
            states.put("Yukon Territory", "YT")

            if (address != null) {
                val state = states[address.adminArea]
                cityAndState = address.locality + ", " + state
            }
            return cityAndState
        }

        fun getCurrentLocation():Location? {
            return currentLoc
        }
    }

    override fun onLocationChanged(p0: Location?) {
        println("LOCATION_UPDATES")
        if (p0 != null) {
            currentLoc = p0
        }
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }
}