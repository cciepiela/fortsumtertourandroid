package com.netgalaxystudios.fortsumtertours

import android.content.SharedPreferences
import android.preference.PreferenceManager

class SharedPreferencesHelper {
    companion object {

        fun setTitle(forGeofenceObjectId:String, title:String) {
            val editor = getSharedPreferences().edit()
            editor.putString(forGeofenceObjectId + "-title", title)
            editor.commit()
        }

        fun getTitle(forGeofenceObjectId:String):String {
            return getSharedPreferences().getString(forGeofenceObjectId+"-title", "")
        }

        fun setMessage(forGeofenceObjectId:String, title:String) {
            val editor = getSharedPreferences().edit()
            editor.putString(forGeofenceObjectId + "-message", title)
            editor.commit()
        }

        fun getMessage(forGeofenceObjectId:String):String {
            return getSharedPreferences().getString(forGeofenceObjectId+"-message", "")
        }

        private fun getSharedPreferences (): SharedPreferences {
            return PreferenceManager.getDefaultSharedPreferences(DefaultApplication.application)
        }
    }
}