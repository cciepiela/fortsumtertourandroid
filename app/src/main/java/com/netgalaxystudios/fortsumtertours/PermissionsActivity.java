package com.netgalaxystudios.fortsumtertours;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.parse.ParseUser;

import static com.netgalaxystudios.fortsumtertours.SplashActivity.sLocationRequestCode;

public class PermissionsActivity extends AppCompatActivity {

    private TextView mEnableLocationButton;
    private TextView mDisableLocationButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);

        connectUi();
    }

    private void connectUi() {
        mEnableLocationButton = (TextView)findViewById(R.id.enable_location_button);
        mDisableLocationButton = (TextView)findViewById(R.id.disable_location_button);
        setListeners();
    }

    private void setListeners() {
        mEnableLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermissions();
            }
        });
        mDisableLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.displayMainActivity(PermissionsActivity.this);
            }
        });
    }

    private void checkPermissions(){
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ){
            ActivityCompat.requestPermissions(this, new String[]{
                    android.Manifest.permission.ACCESS_COARSE_LOCATION,
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.BLUETOOTH,
                    Manifest.permission.BLUETOOTH_ADMIN,
            }, sLocationRequestCode);
        }else  {
            MethodHelper methodHelper = new MethodHelper();
            methodHelper.startTrackingLocation(this);
            goToNextView();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case sLocationRequestCode: {
                // If request is cancelled, the result arrays are empty.
                goToNextView();
                return;
            }
            default: {
                goToNextView();
            }

            // OTHER 'case' lines to check for OTHER
            // permissions this app might request
        }
    }

    private void goToNextView () {
//        startManager();
        Intent i;
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.FortSumter.className, Context.MODE_PRIVATE);
        boolean loggedIn = sharedPreferences.getBoolean("LoggedIn", false);
        if (loggedIn) {
            Log.d(Constants.TAG, "ParseCurrent user is: "+ ParseUser.getCurrentUser().getUsername());
            i = new Intent(this, MainActivity.class);
        }else {
            i = new Intent(this, LoginActivity.class);
        }
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
}
