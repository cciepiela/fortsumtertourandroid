package com.netgalaxystudios.fortsumtertours;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.AnyRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.parse.LocationCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import fr.quentinklein.slt.LocationTracker;

import static android.provider.MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI;

/**
 * Created by coryciepiela on 3/6/17.
 */

class MethodHelper {
    private static ProgressDialog progressDialog;
    private static Location sCurrentLocation = null;
    private LocationTracker mTracker;

    public MethodHelper() {
    }

    void startTrackingLocation(Context context) {
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mTracker = new LocationTracker(context) {
            @Override
            public void onLocationFound(@NonNull Location location) {
                sCurrentLocation = location;
                mTracker.stopListening();
            }

            @Override
            public void onTimeout() {

            }
        };
        mTracker.startListening();
    }

    interface SuccessCallback {
        void processCompleted(Boolean success);
    }

    static void switchToActivity (Activity activity, Class nextClass) {
//        startManager();
        Intent i = new Intent(activity, nextClass);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(i);
        activity.finish();
    }

    static ParseFile convertBitmapToParseFile(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Bitmap newBitmap = getResizedBitmap(bitmap, 300);
        MethodHelper.fixOrientation(newBitmap);
        newBitmap.compress(Bitmap.CompressFormat.PNG, 40, stream);
        byte[] data = stream.toByteArray();
        return new ParseFile(data);
    }

    private static void fixOrientation(Bitmap bitmap) {
        if (bitmap.getWidth() > bitmap.getHeight()) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        }
    }

    static void checkDistanceForViewfinder(Context context, final ViewfinderDistanceCallback callback){
        if (Config.getCurrentConfig().harborMidpoint != null && ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            getCurrentLocation(context, new GetLocationCallback() {
                @Override
                public void gotLocation(Location location, ParseException e) {
                    if (location != null){
                        float distanceFromHarbor = location.distanceTo(Config.getCurrentConfig().harborMidpoint);
                        if (distanceFromHarbor >= Config.getCurrentConfig().viewFinderDistance){
                            callback.gotDistance(false);
                        } else {
                            callback.gotDistance(true);
                        }
                    } else {
                        callback.gotDistance(true);
                        if (e != null){
                            Log.e(Constants.TAG, "Error Location was null! ParseException is: "+e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }
            });
        }else {
            Log.d(Constants.TAG, "HARBOR_MIDPOINT: "+Config.getCurrentConfig().harborMidpoint);
            callback.gotDistance(false);
        }
    }

    interface ViewfinderDistanceCallback{
        void gotDistance(boolean withinRange);
    }

    static void fireLocalNotification (Context context, int id, String title, String body, String beaconObjectId) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context.getApplicationContext(), "notify_001");
        Intent ii = new Intent(context.getApplicationContext(), MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, ii, 0);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.setBigContentTitle(title);
        bigText.bigText(body);

        bigText.setSummaryText(body);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.ic_boat_logo_white);
        mBuilder.setContentTitle(title);
        mBuilder.setContentText(body);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("notify_001",
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(channel);
        }

        mNotificationManager.notify(0, mBuilder.build());
    }

    public static void showProgressHUDWithMessage(String message, Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    static void setScreenNameForGa(Activity activity){
        Log.d(Constants.TAG, "setScreenNameForGa was called!");
        DefaultApplication defaultApplication = new DefaultApplication();
        defaultApplication.getDefaultTracker(activity).setScreenName(activity.getLocalClassName());
        defaultApplication.getDefaultTracker(activity).send(new HitBuilders.ScreenViewBuilder().build());
        Log.d(Constants.TAG, "DefaultApplication.mTracker initialization status is: "+defaultApplication.getDefaultTracker(activity).isInitialized());
    }

    public static void hideHUD() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    static final Uri getUriToResource(@NonNull Context context, @AnyRes int resId) throws Resources.NotFoundException {
        /** Return a Resources instance for your application's package. */
        Resources res = context.getResources();
        /**
         * Creates a Uri which parses the given encoded URI string.
         * @param uriString an RFC 2396-compliant, encoded URI
         * @throws NullPointerException if uriString is null
         * @return Uri for this given uri string
         */
        Uri resUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + res.getResourcePackageName(resId)
                + '/' + res.getResourceTypeName(resId)
                + '/' + res.getResourceEntryName(resId));
        /** return uri */
        return resUri;
    }

    static void showAlert(Context context, String title, String message) {
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message);
            builder.setTitle(title);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {


                //@Override
                public void onClick(DialogInterface dialog, int id) {

                    dialog.cancel();
                }
            });
            builder.create().show();
        }
    }

    static void goBackToFirstActivity(Activity context) {
        Intent intent = new Intent(context, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        context.onBackPressed();
    }

    interface GetImageCallback{
        void gotImage(byte[] image);
    }

    static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;


        int inSampleSize = (height > 2500 || width > 3500) ? 4 : 3;


        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private static Bitmap decodeSampledBitmapFromResource(Context c, Uri uri,
                                                         int reqWidth, int reqHeight) throws FileNotFoundException {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, options);

//        int imageHeight = options.outHeight;
//        Log.d("BITMAP SIZING", "Before HEIGHT " + imageHeight);
//        int imageWidth = options.outWidth;
//        Log.d("BITMAP SIZING", "Before WIDTH " + imageWidth);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, options);
    }

    static byte[] convertUriToByteArray(Context context, Uri uri){
        InputStream iStream = null;
        try {
            iStream = context.getContentResolver().openInputStream(uri);
        }catch (FileNotFoundException e){
            Log.e(Constants.TAG, "Error getting uri in InputStream: "+e.getMessage());
            e.printStackTrace();
        }

        try {
            byte[] inputData = getBytes(iStream);
            return inputData;
        }
        catch (IOException e){
            Log.e(Constants.TAG, "Error getting bytes from inputStream: "+e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    static byte[] convertParseFileToBitmap(ParseFile parseFile) {
        if (parseFile != null) {
            try {
                return parseFile.getData();

            } catch (ParseException e) {
                e.printStackTrace();
            }

        } else {
            return null;
        }
        return null;
    }

    static Bitmap getBitmap(Uri pictureUri, Context context) {

        Uri uri = pictureUri;
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = context.getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();

            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d(Constants.TAG, "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap bitmap = null;
            in = context.getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                bitmap = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = bitmap.getHeight();
                int width = bitmap.getWidth();
                Log.d(Constants.TAG, "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) x,
                        (int) y, true);
                bitmap.recycle();
                bitmap = scaledBitmap;

                System.gc();
            } else {
                bitmap = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d(Constants.TAG, "bitmap size - width: " +bitmap.getWidth() + ", height: " +
                    bitmap.getHeight());
            return bitmap;
        } catch (IOException e) {
            Log.e(Constants.TAG, e.getMessage(),e);
            return null;
        }
    }

    private static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 4096;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    static ParseFile convertBitmapToParseFile(Bitmap bitmap, int orientation) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Bitmap newBitmap = getResizedBitmap(bitmap, 300);
        MethodHelper.fixOrientation(newBitmap, orientation);
        newBitmap.compress(Bitmap.CompressFormat.PNG, 40, stream);
        byte[] data = stream.toByteArray();
        return new ParseFile(data);
    }

    static byte[] bitmapToByteArray(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    private static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    static Bitmap fixOrientation(Bitmap bitmap, int orientation) {
        if (bitmap.getWidth() > bitmap.getHeight() && orientation == Configuration.ORIENTATION_PORTRAIT) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            return bitmap;
        }else if (bitmap.getWidth() < bitmap.getHeight() && orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            return bitmap;
        }
        return bitmap;
    }

    static void setTextViewTypeFaceRegular(TextView textView, Activity activity) {
        Typeface typeFace = Typeface.createFromAsset(activity.getAssets(), "fonts/avenir-next-regular.ttf");
        textView.setTypeface(typeFace);
    }

    static void setEditTextTypeFaceRegular(EditText editText, Activity activity) {
        Typeface typeFace = Typeface.createFromAsset(activity.getAssets(), "fonts/avenir-next-regular.ttf");
        editText.setTypeface(typeFace);
    }

    static void setTextViewTypeFaceItalicized(TextView textView, Activity activity) {
        Typeface typeFace = Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_medium.ttf");
        textView.setTypeface(typeFace);
    }

    static void setEditTextTypeFaceItalicized(EditText editText, Activity activity) {
        Typeface typeFace = Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_medium.ttf");
        editText.setTypeface(typeFace);
    }

    interface ImageCallback{
        void gotImage(byte[] image);
    }

    static Uri UrlToUri(String url){
        return Uri.parse(url);
    }

    static String generateRandomString() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(28);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    static String getCityAndStateFromLocation(Context context, Location location){
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        String cityAndState = "";
        Map<String, String> states = new HashMap<>();
        states.put("Alabama","AL");
        states.put("Alaska","AK");
        states.put("Alberta","AB");
        states.put("American Samoa","AS");
        states.put("Arizona","AZ");
        states.put("Arkansas","AR");
        states.put("Armed Forces (AE)","AE");
        states.put("Armed Forces Americas","AA");
        states.put("Armed Forces Pacific","AP");
        states.put("British Columbia","BC");
        states.put("California","CA");
        states.put("Colorado","CO");
        states.put("Connecticut","CT");
        states.put("Delaware","DE");
        states.put("District Of Columbia","DC");
        states.put("Florida","FL");
        states.put("Georgia","GA");
        states.put("Guam","GU");
        states.put("Hawaii","HI");
        states.put("Idaho","ID");
        states.put("Illinois","IL");
        states.put("Indiana","IN");
        states.put("Iowa","IA");
        states.put("Kansas","KS");
        states.put("Kentucky","KY");
        states.put("Louisiana","LA");
        states.put("Maine","ME");
        states.put("Manitoba","MB");
        states.put("Maryland","MD");
        states.put("Massachusetts","MA");
        states.put("Michigan","MI");
        states.put("Minnesota","MN");
        states.put("Mississippi","MS");
        states.put("Missouri","MO");
        states.put("Montana","MT");
        states.put("Nebraska","NE");
        states.put("Nevada","NV");
        states.put("New Brunswick","NB");
        states.put("New Hampshire","NH");
        states.put("New Jersey","NJ");
        states.put("New Mexico","NM");
        states.put("New York","NY");
        states.put("Newfoundland","NF");
        states.put("North Carolina","NC");
        states.put("North Dakota","ND");
        states.put("Northwest Territories","NT");
        states.put("Nova Scotia","NS");
        states.put("Nunavut","NU");
        states.put("Ohio","OH");
        states.put("Oklahoma","OK");
        states.put("Ontario","ON");
        states.put("Oregon","OR");
        states.put("Pennsylvania","PA");
        states.put("Prince Edward Island","PE");
        states.put("Puerto Rico","PR");
        states.put("Quebec","PQ");
        states.put("Rhode Island","RI");
        states.put("Saskatchewan","SK");
        states.put("South Carolina","SC");
        states.put("South Dakota","SD");
        states.put("Tennessee","TN");
        states.put("Texas","TX");
        states.put("Utah","UT");
        states.put("Vermont","VT");
        states.put("Virgin Islands","VI");
        states.put("Virginia","VA");
        states.put("Washington","WA");
        states.put("West Virginia","WV");
        states.put("Wisconsin","WI");
        states.put("Wyoming","WY");
        states.put("Yukon Territory","YT");
        if (location != null){
            try {
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                Address address = addresses.get(0);
                String state = states.get(address.getAdminArea());
                cityAndState = address.getLocality() + ", " + state;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return cityAndState;
    }

    static void getCurrentLocation(Context ctx, final GetLocationCallback callback) {
        Location location = LocationHelper.Companion.getCurrentLocation();
        callback.gotLocation(location, null);
    }

    interface GetLocationCallback {
        void gotLocation(Location location, ParseException e);
    }

    static String getVersionNumberString(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String versionName = packageInfo.versionName;
            String versionCode = "" + packageInfo.versionCode;
            return "Version Name: " + versionName + " - Version Code: " + versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    static Bitmap getBitMapFromView (View view) {
        Bitmap viewCapture;

        view.setDrawingCacheEnabled(true);
        Log.d(Constants.TAG, "in MethodHelper the view's width is: "+view.getWidth());
//, (int)view.getX(), (int)view.getY(), viewWidth, viewHeight
//        viewCapture = Bitmap.createBitmap(view.getDrawingCache());

        // enabling below line BEFORE returning the drawingCache will cause save functionality to NOT WORK!!!
//        view.setDrawingCacheEnabled(false);
        return view.getDrawingCache();
    }

    static void saveBitmapToExternalStorage(Bitmap finalBitmap, Activity activity){
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root);
        Log.d(Constants.TAG, "makeDirectory boolean returns: "+myDir.mkdirs());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhsss", Locale.getDefault());
        String fname = "fs"+dateFormat.format(new Date()) + ".jpg";
        File file = new File (myDir, fname);
//        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            Toast.makeText(activity, "Image saved successfully!", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            Log.e(Constants.TAG, "Error saving bitmap to ExternalStorage: "+e.getMessage());
            e.printStackTrace();
        }
    }

    static String innerCode(ContentResolver cr, Bitmap source,
                            String title, String description){
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, title);
        values.put(MediaStore.Images.Media.DESCRIPTION, description);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");

        Uri url = null;
        String stringUrl = null;    /* value to be returned */

        try {
            Log.d(Constants.TAG, "Environment.getExternalStorageDirectory() returns: "+Environment.getExternalStorageDirectory()+" When calling .toURI() ExternalStorageDirectory returns: "+Environment.getExternalStorageDirectory().toURI());
            url = cr.insert(Uri.fromFile(Environment.getExternalStorageDirectory()), values);
            Log.d(Constants.TAG, "returned url in innerCode = "+url+" And EXTERNAL_CONTENT_URI constant returns: "+EXTERNAL_CONTENT_URI);

            if (source != null) {
                OutputStream imageOut = cr.openOutputStream(url);
                try {
                    source.compress(Bitmap.CompressFormat.JPEG, 50, imageOut);
                } finally {
                    if (imageOut != null){
                        imageOut.close();
                    }
                }

                long id = ContentUris.parseId(url);
                // Wait until MINI_KIND thumbnail is generated.
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                Bitmap miniThumb = MediaStore.Images.Thumbnails.getThumbnail(cr, id,
                        MediaStore.Images.Thumbnails.MINI_KIND, options);
                // This is for backward compatibility.
                Bitmap microThumb = StoreThumbnail(cr, miniThumb, id, 50F, 50F,
                        MediaStore.Images.Thumbnails.MICRO_KIND);
            } else {
                Log.e(Constants.TAG, "Failed to create thumbnail, removing original");
                if (url != null){
                    cr.delete(url, null, null);
                }
                url = null;
            }
        } catch (Exception e) {
            Log.e(Constants.TAG, "Failed to insert image", e);
            if (url != null) {
                cr.delete(url, null, null);
                url = null;
            }
        }

        if (url != null) {
            stringUrl = url.toString();
        }

        return stringUrl;
    }

    private static Bitmap StoreThumbnail(
            ContentResolver cr,
            Bitmap source,
            long id,
            float width, float height,
            int kind) {
        // create the matrix to scale it
        Matrix matrix = new Matrix();

        float scaleX = width / source.getWidth();
        float scaleY = height / source.getHeight();

        matrix.setScale(scaleX, scaleY);

        Bitmap thumb = Bitmap.createBitmap(source, 0, 0,
                source.getWidth(),
                source.getHeight(), matrix,
                true);

        ContentValues values = new ContentValues(4);
        values.put(MediaStore.Images.Thumbnails.KIND,     kind);
        values.put(MediaStore.Images.Thumbnails.IMAGE_ID, (int)id);
        values.put(MediaStore.Images.Thumbnails.HEIGHT,   thumb.getHeight());
        values.put(MediaStore.Images.Thumbnails.WIDTH,    thumb.getWidth());

        Uri url = cr.insert(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, values);

        try {
            OutputStream thumbOut = cr.openOutputStream(url);

            thumb.compress(Bitmap.CompressFormat.JPEG, 100, thumbOut);
            thumbOut.close();
            return thumb;
        }
        catch (FileNotFoundException ex) {
            return null;
        }
        catch (IOException ex) {
            return null;
        }
    }

    static boolean saveBitmapToDevice(Bitmap finalBitmap, ContentResolver contentResolver) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhsss", Locale.getDefault());
        String title = "fs"+dateFormat.format(new Date()) + ".jpg";
        String path = MediaStore.Images.Media.insertImage(contentResolver, finalBitmap, title, title);
        Log.d(Constants.TAG, "IMAGE_PATH: "+path);
        return path != null;
    }

    static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
