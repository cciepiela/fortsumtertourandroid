package com.netgalaxystudios.fortsumtertours;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class BoatSelectionActivity extends AppCompatActivity {

    private ImageView mBackButton;
    private BoatAdapter mBoatAdapter = new BoatAdapter(BoatSelectionActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boat_selection);

        MethodHelper.setScreenNameForGa(BoatSelectionActivity.this);
        connectUi();
    }

    private void connectUi() {
        mBackButton = (ImageView)findViewById(R.id.back_button);
        RecyclerView boatRecyclerView = (RecyclerView)findViewById(R.id.boat_recycler_view);
        boatRecyclerView.setLayoutManager(new LinearLayoutManager(BoatSelectionActivity.this));
        boatRecyclerView.setAdapter(mBoatAdapter);
        TextView departureHeaderText = (TextView)findViewById(R.id.departure_header_text);
        MethodHelper.setTextViewTypeFaceRegular(departureHeaderText, BoatSelectionActivity.this);
        setListeners();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBoatData();
    }

    private void getBoatData(){
        MethodHelper.showProgressHUDWithMessage("Loading...", BoatSelectionActivity.this);
        Boat.fetchAllBoats(new Boat.BoatInfoCallback() {
            @Override
            public void gotBoatInfo(ArrayList<Boat> boatArrayList) {
                MethodHelper.hideHUD();
                mBoatAdapter.searchResults = boatArrayList;
                mBoatAdapter.notifyDataSetChanged();
            }
        });
    }


}
