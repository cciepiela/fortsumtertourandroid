package com.netgalaxystudios.fortsumtertours;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.vr.sdk.base.GvrView;
import com.google.vr.sdk.widgets.video.VrVideoEventListener;
import com.google.vr.sdk.widgets.video.VrVideoView;

import java.io.IOException;

public class VirtualTourPlayerActivity extends AppCompatActivity implements VideoListener.VideoListenerCallback {
    static VrVideo mVrVideo;
    private GvrView mGvrView;
    private Uri mVideoUri;
    private VrVideoView mVrVideoView;
    private VideoListener mVideoEventListener;
    private static boolean mCardboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtual_tour_player);

        Log.d(Constants.TAG, "Video uri being generated is: "+Uri.parse(mVrVideo.url));
        mVideoUri = Uri.parse(mVrVideo.url);
        connectUi();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mVrVideoView.pauseRendering();
        mVrVideoView.shutdown();
    }

    private void connectUi() {
        mVrVideoView = (VrVideoView)findViewById(R.id.vr_video_view);
        mVrVideoView.setEnabled(false);
        try {
            VrVideoView.Options options = new VrVideoView.Options();
            options.inputFormat = VrVideoView.Options.TYPE_MONO;
            options.inputType = VrVideoView.Options.FORMAT_DEFAULT;
            if (mCardboard){
                mVrVideoView.setDisplayMode(VrVideoView.DisplayMode.FULLSCREEN_STEREO);
            } else {
                mVrVideoView.setDisplayMode(VrVideoView.DisplayMode.FULLSCREEN_MONO);
            }
            mVrVideoView.loadVideo(mVideoUri, null);
            mVideoEventListener = new VideoListener(this);
            mVrVideoView.setEventListener(mVideoEventListener);
//            vrVideoView.playVideo();
        } catch (IOException e) {
            Log.e(Constants.TAG, "Error loading VrVideo: "+e.getMessage());
            e.printStackTrace();
        } catch (Exception e){
            Log.e(Constants.TAG, "Error loading VrVideo. Generic error is: "+e.getMessage());
            e.printStackTrace();
        }
        setListeners();
    }

    private void setListeners() {

    }

    static void display(VrVideo vrVideo, boolean cardboard, Context context){
        mVrVideo = vrVideo;
        mCardboard = cardboard;
        Intent i = new Intent(context, VirtualTourPlayerActivity.class);
        context.startActivity(i);
    }

    @Override
    public void closeVrView() {
        finish();
    }

    @Override
    public void didLoad() {
        mVrVideoView.setEnabled(true);
    }
}
