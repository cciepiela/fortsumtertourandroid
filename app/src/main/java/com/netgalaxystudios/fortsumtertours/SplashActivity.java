package com.netgalaxystudios.fortsumtertours;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.parse.ParseException;

public class SplashActivity extends AppCompatActivity {

    public static final int sLocationRequestCode = 131;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH)) != PackageManager.PERMISSION_GRANTED
                || (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) != PackageManager.PERMISSION_GRANTED
                || (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN)) != PackageManager.PERMISSION_GRANTED
                || (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_PRIVILEGED)) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{
                    android.Manifest.permission.ACCESS_COARSE_LOCATION,
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.BLUETOOTH,
                    Manifest.permission.BLUETOOTH_ADMIN,
                    Manifest.permission.BLUETOOTH_PRIVILEGED
            }, sLocationRequestCode);
        }else  {
            MethodHelper methodHelper = new MethodHelper();
            methodHelper.startTrackingLocation(this);
            goToNextView();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case sLocationRequestCode: {
                // If request is cancelled, the result arrays are empty.
                DefaultApplication application = (DefaultApplication)getApplication();
                DefaultApplication.loadConfig();
                goToNextView();
                return;
            }
            default: {
                goToNextView();
            }

            // OTHER 'case' lines to check for OTHER
            // permissions this app might request
        }
    }

    private void goToNextView () {
//        startManager();

        LocationHelper.Companion.shared(getApplicationContext());

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.FortSumter.className, Context.MODE_PRIVATE);
        boolean loggedIn = sharedPreferences.getBoolean("LoggedIn", false);
        if (loggedIn && User.userExists()) {
            goToMainView();
        }else {
            String id = android.provider.Settings.System.getString(super.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            if (id != null) {
                User.loginUser(id, id, new User.LoginUserCallback() {
                    @Override
                    public void loggedInWithParseException(boolean success, boolean isNew, ParseException e) {
                        if (success) {
                            goToMainView();
                        }
                    }
                });
            }
        }

    }

    private void goToMainView () {
        Intent i;
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.FortSumter.className, Context.MODE_PRIVATE);
        boolean loggedIn = sharedPreferences.getBoolean("LoggedIn", false);
        Log.d(Constants.TAG, "loggedIn in SplashActivity returning: "+loggedIn);
        i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
}
