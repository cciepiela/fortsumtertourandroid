package com.netgalaxystudios.fortsumtertours;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.cameraview.CameraView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.cketti.shareintentbuilder.ShareIntentBuilder;

public class PostcardActivity extends AppCompatActivity {

    private ImageView mBackButton;
    private CameraView mCameraView;
    private ImageView mPictureImageView;
    private RelativeLayout mConfirmLayout;
    private Button mCancelButton;
    private Button mCheckButton;
    private ImageView mFlipButton;
    private Button mCameraButton;
    private byte[] pictureData;
    private RelativeLayout mCameraContainerView;
    private ImageView mFilterImageView;
    private ArrayList<PostcardFilter> mPhotoFilters = new ArrayList<>();
    private int mPhotoFilterPosition = 0;
    private Bitmap mPictureTakenBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        setContentView(R.layout.activity_postcard);


        connectUi();
    }

    private void connectUi() {
        mBackButton = (ImageView) findViewById(R.id.back_button);
        mCameraView = (CameraView) findViewById(R.id.camera);
        mPictureImageView = (ImageView) findViewById(R.id.picture_image_view);
        mFilterImageView = (ImageView) findViewById(R.id.filter_image_view);
        mConfirmLayout = (RelativeLayout) findViewById(R.id.confirm_layout);
        mCancelButton = (Button) findViewById(R.id.cancel_button);
        mCheckButton = (Button) findViewById(R.id.check_button);
        mFlipButton = (ImageView) findViewById(R.id.flip_button);
        mCameraButton = (Button) findViewById(R.id.camera_button);
        mCameraContainerView = (RelativeLayout) findViewById(R.id.camera_container_layout);
        mCameraContainerView.setDrawingCacheEnabled(true);
        setListeners();
        initializeActivity();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCameraView.takePicture();
            }
        });
        mFlipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pictureData != null) {
                    ShareIntentBuilder.from(PostcardActivity.this)
                            .ignoreSpecification()
                            .text("Check out the Fort Sumter Tours app! - " + Config.getCurrentConfig().shareUrl)
                            .stream(MethodHelper.getImageUri(PostcardActivity.this, mCameraContainerView.getDrawingCache()))
                            .share();
                } else {
                    if (mCameraView.getFacing() == CameraView.FACING_BACK) {
                        mCameraView.setFacing(CameraView.FACING_FRONT);
                    } else {
                        mCameraView.setFacing(CameraView.FACING_BACK);
                    }
                }
            }
        });
        mCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pictureData != null) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhsss", Locale.getDefault());
                    String title = "fs"+dateFormat.format(new Date()) + ".jpg";
//                    getImageFromLayout(mCameraContainerView);
                    Bitmap bitmap = MethodHelper.getBitMapFromView(mCameraContainerView);
                    if (mCameraView.getFacing() == CameraView.FACING_FRONT){
//                        Log.d(Constants.TAG, "bitmap.getWidth returns: "+bitmap.getWidth()+" and getHeight returns: "+bitmap.getHeight());
//                        Bitmap bitmap1 = Bitmap.createBitmap(mCameraContainerView.getDrawingCache(), 0, 0, mCameraContainerView.getDrawingCache().getWidth(), mCameraContainerView.getDrawingCache().getHeight(), matrix, false);
//                        mCameraContainerView.buildDrawingCache(true);
//                        Bitmap bitmap1 = mCameraContainerView.getDrawingCache();
//                        MethodHelper.saveBitmapToDevice(bitmap, getContentResolver());
                        MethodHelper.saveBitmapToExternalStorage(bitmap, PostcardActivity.this);
                    }else {
                        //Rear-facing camera operable with below line; BUT NOT front-facing camera.
//                        MethodHelper.saveBitmapToDevice(bitmap, getContentResolver());
                        MethodHelper.saveBitmapToExternalStorage(bitmap, PostcardActivity.this);
                    }
//                    savePostcard(MethodHelper.getBitMapFromView(mCameraContainerView));
                }

            }
        });
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPictureImageView.setImageBitmap(null);
                mPictureTakenBitmap = null;
                mPictureImageView.setVisibility(View.GONE);
                mFilterImageView.setVisibility(View.GONE);
                mCameraButton.setVisibility(View.VISIBLE);
                mConfirmLayout.setVisibility(View.GONE);
                Glide.with(PostcardActivity.this)
                        .load(R.drawable.camera_flip)
                        .into(mFlipButton);
                if (!mCameraView.isCameraOpened()){
                    mCameraView.start();
                }
            }
        });
        mCameraView.addCallback(new CameraView.Callback() {
            @Override
            public void onPictureTaken(CameraView cameraView, byte[] data) {
                super.onPictureTaken(cameraView, data);
                pictureData = data;
                Glide.with(PostcardActivity.this)
                        .load(R.drawable.ic_share_triangle_white)
                        .into(mFlipButton);
                if (cameraView.getFacing() == CameraView.FACING_FRONT){
                    mPictureTakenBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    Bitmap rotatedBitmap = rotatePortraitImage(mPictureTakenBitmap);
                    mPictureImageView.setImageBitmap(rotatedBitmap);
                } else {
                    Glide.with(PostcardActivity.this)
                            .load(data)
                            .override(300, 300)
                            .into(mPictureImageView);
                }
                mPhotoFilterPosition = 0;
                mPictureImageView.setVisibility(View.VISIBLE);
                mFilterImageView.setVisibility(View.VISIBLE);
                mCameraButton.setVisibility(View.INVISIBLE);
                mConfirmLayout.setVisibility(View.VISIBLE);

                if (mPhotoFilters.size() > 0){
                    sizeImageViewToFilter(mPhotoFilters.get(0), mFilterImageView);
                    mPhotoFilters.get(0).getImage(new History.ImageCallback() {
                        @Override
                        public void gotImage(byte[] image) {
                            Glide.with(PostcardActivity.this)
                                    .load(image)
                                    .into(mFilterImageView);
                        }
                    });
                } else {
                    Log.d(Constants.TAG, "mPhotoFilters size was 0!");
                }
            }
        });
        mPictureImageView.setOnTouchListener(new View.OnTouchListener() {
            float initialX;
            float finalX;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = event.getX();
                        break;
                    case MotionEvent.ACTION_UP:
                        if (finalX < initialX) {
                            //Swiped right
                            if (mPhotoFilterPosition < mPhotoFilters.size()) {
                                mPhotoFilterPosition++;
                                if (mPhotoFilterPosition >= mPhotoFilters.size()) {
                                    mPhotoFilterPosition = mPhotoFilters.size() - 1;
                                }
                            }
                            sizeImageViewToFilter(mPhotoFilters.get(mPhotoFilterPosition), mFilterImageView);
                            mPhotoFilters.get(mPhotoFilterPosition).getImage(new History.ImageCallback() {
                                @Override
                                public void gotImage(byte[] image) {
                                    Glide.with(PostcardActivity.this)
                                            .fromBytes()
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .load(image)
                                            .override(300, 300)
                                            .into(mFilterImageView);
                                }
                            });
                        } else {
                            //Swiped left
                            if (mPhotoFilterPosition > -1) {
                                mPhotoFilterPosition--;
                                if (mPhotoFilterPosition <= -1) {
                                    mPhotoFilterPosition = 0;
                                }
                            }
                            sizeImageViewToFilter(mPhotoFilters.get(mPhotoFilterPosition), mFilterImageView);
                            mPhotoFilters.get(mPhotoFilterPosition).getImage(new History.ImageCallback() {
                                @Override
                                public void gotImage(byte[] image) {
                                    Glide.with(PostcardActivity.this)
                                            .fromBytes()
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .load(image)
                                            .override(300, 300)
                                            .into(mFilterImageView);
                                }
                            });
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        finalX = event.getX();
                        break;
                }
                return true;
            }
        });
    }

    private Bitmap rotatePortraitImage(Bitmap bitmap){
        Matrix matrix = new Matrix();
        matrix.setRotate(270);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
    }

    private void sizeImageViewToFilter(PostcardFilter postcardFilter, ImageView imageView) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) imageView.getWidth(), (int) imageView.getHeight());
        double x = 0.0;
        double y = 0.0;
        double width = postcardFilter.width;
        double height = postcardFilter.height;
        List<PostcardFilter.FillType> fillTypes = postcardFilter.mFillTypes;
        boolean xSet = false;
        boolean ySet = false;
        boolean widthSet = false;
        boolean heightSet = false;

        if (fillTypes.contains(PostcardFilter.FillType.Full)) {
            params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
            params.height = RelativeLayout.LayoutParams.MATCH_PARENT;
        }

        if (fillTypes.contains(PostcardFilter.FillType.FullWidth)) {
            heightSet = true;
            widthSet = true;
            xSet = true;
            params.height = (int) (height * (imageView.getWidth() / width));
            params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
            params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        }

        if (!heightSet && (fillTypes.contains(PostcardFilter.FillType.FullHeight))) {
            heightSet = true;
            widthSet = true;
            ySet = true;
            params.width = (int) (width * (imageView.getHeight() / height));
            params.height = RelativeLayout.LayoutParams.MATCH_PARENT;
            params.addRule(RelativeLayout.CENTER_VERTICAL);
        }
        if (!ySet && fillTypes.contains(PostcardFilter.FillType.AlignBottom)) {
            ySet = true;
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        }
        if (!ySet && fillTypes.contains(PostcardFilter.FillType.AlignTop)) {
            ySet = true;
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        }
        if (!xSet && fillTypes.contains(PostcardFilter.FillType.AlignRight)) {
            xSet = true;
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        }
        if (!xSet && fillTypes.contains(PostcardFilter.FillType.AlignLeft)) {
            xSet = true;
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        }
        if (!xSet && fillTypes.contains(PostcardFilter.FillType.CenterX)) {
            xSet = true;
            params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        }
        if (!ySet && fillTypes.contains(PostcardFilter.FillType.CenterY)) {
            ySet = true;
            params.addRule(RelativeLayout.CENTER_VERTICAL);
        }
        if (!xSet) {
            params.leftMargin = (int) x;
        }
        if (!ySet) {
            params.topMargin = (int) y;
        }
        imageView.setLayoutParams(params);
    }

    private void initializeActivity() {
        if (pictureData == null){
            if (!mCameraView.isCameraOpened()) {
                mCameraView.start();
            }
            mConfirmLayout.setVisibility(View.INVISIBLE);
            mPictureImageView.setVisibility(View.INVISIBLE);
            mPictureImageView.setImageBitmap(null);
            mCameraButton.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mFlipButton.setImageDrawable(getDrawable(R.drawable.camera_flip));
            }
        } else {
            mConfirmLayout.setVisibility(View.VISIBLE);
            mPictureImageView.setVisibility(View.VISIBLE);
            mCameraButton.setVisibility(View.INVISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mFlipButton.setImageDrawable(getDrawable(R.drawable.ic_share_triangle_white));
            }
        }

        PostcardFilter.getFilters(new PostcardFilter.GetFiltersCallback() {
            @Override
            public void gotFilters(ArrayList<PostcardFilter> filters) {
                mPhotoFilters.addAll(filters);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(Constants.TAG, "onPause was called!");
        if (mCameraView.isCameraOpened()) {
            Log.d(Constants.TAG, "cameraView was opened in onPause and will be stopped!");
            mCameraView.stop();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:sss", Locale.getDefault());
        Log.d(Constants.TAG, "onResume was called at: "+dateFormat.format(new Date()));
    }

    @Override
    protected void onStart() {
        super.onStart();
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:sss", Locale.getDefault());
        Log.d(Constants.TAG, "onStart was called at: "+dateFormat.format(new Date()));
        initializeActivity();
    }
}
