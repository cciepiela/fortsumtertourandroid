package com.netgalaxystudios.fortsumtertours;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by charlie on 3/29/17.
 */

class PostcardFilter {
    private String objectId;
    String filterName = "CIPhotoEffectFade";
    ParseFile imageFile;
    byte[] image;
    double x = 0.0;
    double y = 0.0;
    double width = 0.0;
    double height = 0.0;
    List<FillType> mFillTypes = new ArrayList<>();
    private ImageView imageView;

    private PostcardFilter(ParseObject object) {
        if (object.getString(Constants.PostcardFilter.filterName) != null) {
            filterName = object.getString(Constants.PostcardFilter.filterName);
        }
        if (object.getString(Constants.History.objectId) != null) {
            objectId = object.getString(Constants.History.objectId);
        }
        if (object.getParseFile(Constants.PostcardFilter.imageFile) != null) {
            imageFile = object.getParseFile(Constants.PostcardFilter.imageFile);
        }

        x = object.getDouble(Constants.PostcardFilter.x);
        y = object.getDouble(Constants.PostcardFilter.y);
        width = object.getDouble(Constants.PostcardFilter.width) * 2;
        height = object.getDouble(Constants.PostcardFilter.height) * 2;

        if (object.getBoolean(Constants.PostcardFilter.full)) {
            mFillTypes.add(FillType.Full);
        }
        if (object.getBoolean(Constants.PostcardFilter.fullWidth)) {
            mFillTypes.add(FillType.FullWidth);
        }
        if (object.getBoolean(Constants.PostcardFilter.fullHeight)) {
            mFillTypes.add(FillType.FullHeight);
        }
        if (object.getBoolean(Constants.PostcardFilter.alignBottom)) {
            mFillTypes.add(FillType.AlignBottom);
        }
        if (object.getBoolean(Constants.PostcardFilter.alignTop)) {
            mFillTypes.add(FillType.AlignTop);
        }
        if (object.getBoolean(Constants.PostcardFilter.alignRight)) {
            mFillTypes.add(FillType.AlignRight);
        }
        if (object.getBoolean(Constants.PostcardFilter.alignLeft)) {
            mFillTypes.add(FillType.AlignLeft);
        }
        if (object.getBoolean(Constants.PostcardFilter.centerX)) {
            mFillTypes.add(FillType.CenterX);
        }
        if (object.getBoolean(Constants.PostcardFilter.centerY)) {
            mFillTypes.add(FillType.CenterY);
        }
    }

    void getImage(final History.ImageCallback callback) {
        if (image != null) {
            callback.gotImage(image);
        } else if (imageFile != null) {
            imageFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null) {
                        image = data;
                        callback.gotImage(data);
                    } else {
                        Log.e(Constants.TAG, "Error getting History image: " + e.getMessage());
                        e.printStackTrace();
                        callback.gotImage(null);
                    }
                }
            });
        } else {
            callback.gotImage(null);
        }
    }

    private static ParseQuery<ParseObject> getQuery() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PostcardFilter.className);
        query.whereNotEqualTo("active",false);
        return query;
    }

    static void getFilters(final GetFiltersCallback callback) {
        getQuery().findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                ArrayList<PostcardFilter> filters = new ArrayList<PostcardFilter>();
                if (objects != null) {
                    for (ParseObject object : objects) {
                        filters.add(new PostcardFilter(object));
                    }
                }
                callback.gotFilters(filters);
            }
        });
    }

    interface GetFiltersCallback {
        void gotFilters(ArrayList<PostcardFilter> filters);
    }

    enum FillType {
        Full, FullWidth, FullHeight, AlignBottom, AlignTop, AlignRight, AlignLeft, CenterX, CenterY
    }

    void addToView(final RelativeLayout relativeLayout, final Context context) {
        getImage(new History.ImageCallback() {
            @Override
            public void gotImage(byte[] image) {
                if (image != null) {
                    if (imageView != null) {
                        Log.d(Constants.TAG, "imageView wasn't null and will be invalidated!");
                        imageView.setImageBitmap(null);
                        imageView.invalidate();
                    }
                    imageView = new ImageView(context);
                    imageView.setLayoutParams(getParams(relativeLayout));
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    relativeLayout.addView(imageView, getParams(relativeLayout));
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length, options);
                    int width = imageView.getLayoutParams().width;
                    if (imageView.getLayoutParams().width == RelativeLayout.LayoutParams.MATCH_PARENT) {
                        Log.d(Constants.TAG, "Width if state returned true in addToView");
                        width = relativeLayout.getWidth();
                    }
                    int height = imageView.getLayoutParams().height;
                    if (imageView.getLayoutParams().height == RelativeLayout.LayoutParams.MATCH_PARENT) {
                        Log.d(Constants.TAG, "Height if state returned true in addToView");
                        height = relativeLayout.getHeight();
                    }
                    if (width > 0 && height > 0) {
                        Log.d(Constants.TAG, "width and height were > 0! they are: width - " + width + " height - " + height);
                        //OOMS at line below
                        imageView.setImageBitmap(Bitmap.createScaledBitmap(bmp, width, height, false));
                    }
                    //Glide.with(context).load(image).into(imageView);
                } else {
                    Log.d(Constants.TAG, "NO_IMAGE_FOUND");
                }
            }
        });
    }

    private RelativeLayout.LayoutParams getParams(RelativeLayout layout) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) width, (int) height);
        Log.d(Constants.TAG, "FILTER: " + objectId + " FILL_TYPES: " + mFillTypes);
        boolean xSet = false;
        boolean ySet = false;
        boolean widthSet = false;
        boolean heightSet = false;

        if (mFillTypes.contains(FillType.Full)) {
            params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
            params.height = RelativeLayout.LayoutParams.MATCH_PARENT;
            return params;
        }

        if (mFillTypes.contains(FillType.FullWidth)) {
            heightSet = true;
            widthSet = true;
            xSet = true;
            params.height = (int) (height * (layout.getWidth() / width));
            params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
            params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        }

        if (!heightSet && (mFillTypes.contains(FillType.FullHeight))) {
            heightSet = true;
            widthSet = true;
            ySet = true;
            params.width = (int) (width * (layout.getHeight() / height));
            params.height = RelativeLayout.LayoutParams.MATCH_PARENT;
            params.addRule(RelativeLayout.CENTER_VERTICAL);
        }
        if (!ySet && mFillTypes.contains(FillType.AlignBottom)) {
            ySet = true;
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        }
        if (!ySet && mFillTypes.contains(FillType.AlignTop)) {
            ySet = true;
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        }
        if (!xSet && mFillTypes.contains(FillType.AlignRight)) {
            xSet = true;
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        }
        if (!xSet && mFillTypes.contains(FillType.AlignLeft)) {
            xSet = true;
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        }
        if (!xSet && mFillTypes.contains(FillType.CenterX)) {
            xSet = true;
            params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        }
        if (!ySet && mFillTypes.contains(FillType.CenterY)) {
            ySet = true;
            params.addRule(RelativeLayout.CENTER_VERTICAL);
        }
        if (!xSet) {
            params.leftMargin = (int) x;
        }
        if (!ySet) {
            params.topMargin = (int) y;
        }
        return params;
    }
}