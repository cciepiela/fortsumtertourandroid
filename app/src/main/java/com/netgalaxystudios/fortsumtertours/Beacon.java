package com.netgalaxystudios.fortsumtertours;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by charlie on 4/20/17.
 */

public class Beacon {
    String objectId;
    String title;
    String subtitle;
    String body;
    int distance = 2;
    int minor = 2;
    int major = 2;
    int timeInterval = 2;
    int secondaryMessageTimeFrame = 2;
    String title2;
    String subtitle2;
    String body2;
    String details = "";
    private ParseFile imageFile;
    private byte[] image;

    Beacon(ParseObject object) {
        if (object.getObjectId() != null) {
            objectId = object.getObjectId();
        }
        if (object.getString(Constants.Beacon.title) != null) {
            title = object.getString(Constants.Beacon.title);
        }
        if (object.getString(Constants.Beacon.subtitle) != null) {
            subtitle = object.getString(Constants.Beacon.subtitle);
        }
        if (object.getString(Constants.Beacon.body) != null) {
            body = object.getString(Constants.Beacon.body);
        }
        if (object.get(Constants.Beacon.distance) != null) {
            distance = object.getInt(Constants.Beacon.distance);
        }
        if (object.get(Constants.Beacon.minor) != null) {
            minor = object.getInt(Constants.Beacon.minor);
        }
        if (object.get(Constants.Beacon.major) != null) {
            major = object.getInt(Constants.Beacon.major);
        }
        if (object.get(Constants.Beacon.timeInterval) != null) {
            timeInterval = object.getInt(Constants.Beacon.timeInterval);
        }
        if (object.get(Constants.Beacon.secondaryMessageTimeFrame) != null) {
            secondaryMessageTimeFrame = object.getInt(Constants.Beacon.secondaryMessageTimeFrame);
        }
        if (object.getString(Constants.Beacon.title2) != null) {
            title2 = object.getString(Constants.Beacon.title2);
        }
        if (object.getString(Constants.Beacon.subtitle2) != null) {
            subtitle2 = object.getString(Constants.Beacon.subtitle2);
        }
        if (object.getString(Constants.Beacon.body2) != null) {
            body2 = object.getString(Constants.Beacon.body2);
        }
        if (object.getString(Constants.Beacon.details) != null) {
            details = object.getString(Constants.Beacon.details);
        }
        if (object.get(Constants.Beacon.imageFile) != null) {
            imageFile = object.getParseFile(Constants.Beacon.imageFile);
        }
    }


    void getImage(final Boat.BoatImageCallback callback){
        if (image != null) {
            callback.gotBoatImage(image);
        }else if (imageFile != null){
                imageFile.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {
                        if (e == null){
                            image = data;
                            callback.gotBoatImage(data);
                        } else {
                            callback.gotBoatImage(null);
                            Log.e(Constants.TAG, "Error getting boat image: "+e.getMessage());
                            e.printStackTrace();
                        }
                    }
                });
        }

    }


    boolean hasSecondaryMessage () {
        return secondaryMessageTimeFrame > 0 && title2 != null && subtitle2 != null && body2 != null;
    }

    static void getBeacons (final GetBeaconsCallback callback) {
        if (DefaultApplication.beacons != null) {
            if (DefaultApplication.beacons.size() > 0) {
                callback.gotBeacons(DefaultApplication.beacons);
                return;
            }
        }
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.Beacon.className);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                ArrayList<Beacon> beacons = new ArrayList<Beacon>();
                if (objects != null) {
                    for (ParseObject object:objects) {
                        beacons.add(new Beacon(object));
                    }
                }
                DefaultApplication.beacons = beacons;
                callback.gotBeacons(beacons);
            }
        });

    }

    interface GetBeaconsCallback {
        void gotBeacons (ArrayList<Beacon> beacons);
    }

    interface GetBeaconCallback {
        void gotBeacon (Beacon beacon);
    }

    static void getBeacon (final String forObjectId, final GetBeaconCallback callback) {
        getBeacons(new GetBeaconsCallback() {
            @Override
            public void gotBeacons(ArrayList<Beacon> beacons) {
                Beacon foundBeacon = null;
                for (Beacon beacon:beacons) {
                    if (beacon.objectId.equals(forObjectId)) {
                        foundBeacon = beacon;
                    }
                }
                callback.gotBeacon(foundBeacon);
            }
        });
    }

    boolean shouldFire (Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.Beacon.className, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.commit();
        if (sharedPreferences.getLong(objectId, -1) <= 0) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(objectId, new Date().getTime());
            editor.commit();
            return true;
        }
        Date lastFiredDate = new Date(sharedPreferences.getLong(objectId, 0));
        long timeSince = (new Date().getTime() - lastFiredDate.getTime()) / 1000;
        if (timeSince > timeInterval) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(objectId, new Date().getTime());
            editor.commit();
            return true;
        }
        return false;
    }
}
