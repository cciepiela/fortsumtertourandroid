package com.netgalaxystudios.fortsumtertours;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;

import java.util.Date;

import static android.content.Context.SENSOR_SERVICE;
import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by charlesvonlehe on 5/30/17.
 */

public class SensorListener implements SensorEventListener {

    SensorManager sensorManager;
    private Sensor sensorAccelerometer;
    private Sensor sensorMagneticField;

    private float[] valuesAccelerometer;
    private float[] valuesMagneticField;

    private float[] matrixR;
    private float[] matrixI;
    private float[] matrixValues;
    Context context;
    private Date lastLogDate = new Date();

    public void setup (Context context) {
        this.context = context;
        sensorManager = (SensorManager)context.getSystemService(SENSOR_SERVICE);
        sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorMagneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        valuesAccelerometer = new float[3];
        valuesMagneticField = new float[3];
        matrixR = new float[9];
        matrixI = new float[9];
        matrixValues = new float[3];
    }

    public void onResume() {
        sensorManager.registerListener(this, sensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorMagneticField, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void onPause () {
        sensorManager.unregisterListener(this,
                sensorAccelerometer);
        sensorManager.unregisterListener(this,
                sensorMagneticField);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                for (int i = 0; i < 3; i++) {
                    valuesAccelerometer[i] = sensorEvent.values[i];
                }
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                for (int i = 0; i < 3; i++) {
                    valuesMagneticField[i] = sensorEvent.values[i];
                }
                break;
        }

        boolean success = SensorManager.getRotationMatrix(
                matrixR,
                matrixI,
                valuesAccelerometer,
                valuesMagneticField);

        if (success) {
            SensorManager.getOrientation(matrixR, matrixValues);

            double azimuth = Math.toDegrees(matrixValues[0]);
            //double pitch = Math.toDegrees(matrixValues[1]);
            //double roll = Math.toDegrees(matrixValues[2]);

            WindowManager mWindowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
            Display mDisplay = mWindowManager.getDefaultDisplay();

            Float degToAdd = 0f;

            if (mDisplay.getRotation() == Surface.ROTATION_0)
                degToAdd = 0.0f;
            if (mDisplay.getRotation() == Surface.ROTATION_90)
                degToAdd = 90.0f;
            if (mDisplay.getRotation() == Surface.ROTATION_180)
                degToAdd = 180.0f;
            if (mDisplay.getRotation() == Surface.ROTATION_270)
                degToAdd = 270.0f;
            float degrees = (float)(azimuth + degToAdd);
            if (new Date().getTime() - lastLogDate.getTime() > 500) {
                Log.d(Constants.TAG, "CURRENT_DEGREES: "+degrees);
                lastLogDate = new Date();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
