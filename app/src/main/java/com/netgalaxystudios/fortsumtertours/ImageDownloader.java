package com.netgalaxystudios.fortsumtertours;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by coryciepiela on 3/6/17.
 */

class ImageDownloader extends AsyncTask<ImageView, Void, Bitmap> {

    private final ImageDownloaderCallback mCallback;
    private final String mURL;

    ImageDownloader(String URL, ImageDownloaderCallback callback) {
        mCallback = callback;
        mURL = URL;
    }


    @Override
    protected Bitmap doInBackground(ImageView... params) {
        return download_Image(mURL);
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        mCallback.downloadedImage(result);

    }

    interface ImageDownloaderCallback {
        void downloadedImage(Bitmap bitmap);
    }

    private Bitmap download_Image(String url) {

        Bitmap bmp = null;
        try {
            URL ulrn = new URL(url);
            HttpURLConnection con = (HttpURLConnection) ulrn.openConnection();
            InputStream is = con.getInputStream();
            bmp = BitmapFactory.decodeStream(is);
            if (null != bmp)
                return bmp;

        } catch (Exception e) {
        }
        return bmp;
    }
}
