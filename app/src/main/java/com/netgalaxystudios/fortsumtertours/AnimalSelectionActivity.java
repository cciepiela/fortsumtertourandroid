package com.netgalaxystudios.fortsumtertours;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AnimalSelectionActivity extends AppCompatActivity {

    private ImageView mBackButton;
    private AnimalAdapter mAdapter = new AnimalAdapter(AnimalSelectionActivity.this);
    private ArrayList<Animal> mAnimals = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_selection);

        connectUi();
    }

    private void connectUi() {
        MethodHelper.showProgressHUDWithMessage("Loading...", AnimalSelectionActivity.this);
        mBackButton = (ImageView)findViewById(R.id.back_button);
        RecyclerView animalRecycler = (RecyclerView)findViewById(R.id.animal_recycler);
        animalRecycler.setLayoutManager(new LinearLayoutManager(AnimalSelectionActivity.this));
        TextView headerTitleText = (TextView)findViewById(R.id.header_title_text);
        MethodHelper.setTextViewTypeFaceRegular(headerTitleText, AnimalSelectionActivity.this);
        animalRecycler.setAdapter(mAdapter);
        setListeners();
        populateUi();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void populateUi(){
        Animal.getAnimals(new Animal.GetAnimalsCallback() {
            @Override
            public void gotAnimals(ArrayList<Animal> animals) {
                MethodHelper.hideHUD();
                mAnimals = animals;
                mAdapter.searchResults = mAnimals;
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodHelper.setScreenNameForGa(AnimalSelectionActivity.this);
    }

}
