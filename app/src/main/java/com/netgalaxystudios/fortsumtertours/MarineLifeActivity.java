package com.netgalaxystudios.fortsumtertours;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MarineLifeActivity extends AppCompatActivity {

    private ImageView mBackButton;
    private TextView mOverviewButton;
    private TextView mImpactButton;
    private TextView mAnimalsButton;
    private TextView mContentText;
    private TextView mTitleText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marine_life);

        connectUi();
    }

    private void connectUi() {
        mBackButton = (ImageView)findViewById(R.id.back_button);
        mOverviewButton = (TextView)findViewById(R.id.overview_button);
        mImpactButton = (TextView)findViewById(R.id.impact_button);
        mAnimalsButton = (TextView)findViewById(R.id.animals_button);
        mContentText = (TextView)findViewById(R.id.marine_life_text);
        TextView headerTitleText = (TextView)findViewById(R.id.header_title_text);
        MethodHelper.setTextViewTypeFaceRegular(headerTitleText, MarineLifeActivity.this);
        mContentText.setText(Html.fromHtml(Config.getCurrentConfig().marineLifeText));
        mTitleText = (TextView)findViewById(R.id.history_title_text);
        setListeners();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mOverviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContentText.setText(Html.fromHtml(Config.getCurrentConfig().marineLifeText));
                mTitleText.setText(R.string.overview);
            }
        });
        mImpactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContentText.setText(Html.fromHtml(Config.getCurrentConfig().ourImpactText));
                mTitleText.setText(R.string.our_impact);
            }
        });
        mAnimalsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MarineLifeActivity.this, AnimalSelectionActivity.class);
                startActivity(i);
            }
        });
    }
}
