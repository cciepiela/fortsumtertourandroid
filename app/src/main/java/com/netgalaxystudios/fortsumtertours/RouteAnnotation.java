package com.netgalaxystudios.fortsumtertours;

import android.graphics.Color;
import android.location.Location;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by coryciepiela on 3/29/17.
 */

class RouteAnnotation {
    private String objectId;
    String title;
    private ParseGeoPoint coordinateGeoPoint;
    Location coordinate;
    String url;
    int pinColor = Color.RED;

    private RouteAnnotation(ParseObject parseObject) {
        objectId = parseObject.getObjectId();
        if (parseObject.getString(Constants.RouteAnnotation.title) != null){
            title = parseObject.getString(Constants.RouteAnnotation.title);
        }
        if (parseObject.getParseGeoPoint(Constants.RouteAnnotation.location) != null){
            coordinateGeoPoint = parseObject.getParseGeoPoint(Constants.RouteAnnotation.location);
        }
        if (parseObject.getString(Constants.RouteAnnotation.url) != null){
            url = parseObject.getString(Constants.RouteAnnotation.url);
        }
        if (parseObject.getString(Constants.RouteAnnotation.pinColor) != null){
            pinColor = Color.parseColor("#"+parseObject.getString(Constants.RouteAnnotation.pinColor));
        }
    }

    static void getRouteAnnotations(final RouteAnnotationCallback callback){
        final ArrayList<RouteAnnotation> annotations = new ArrayList<>();
        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery(Constants.RouteAnnotation.className);
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null){
                    if (objects.size() > 0){
                        for (ParseObject object:objects){
                            RouteAnnotation annotation = new RouteAnnotation(object);
                            if (annotation.coordinateGeoPoint != null) {
                                annotation.coordinate = new Location("dummyprovider");
                                double latitude = annotation.coordinateGeoPoint.getLatitude();
                                double longitude = annotation.coordinateGeoPoint.getLongitude();

                                annotation.coordinate.setLatitude(latitude);
                                annotation.coordinate.setLongitude(longitude);
                                if (annotation.coordinateGeoPoint.getLatitude() != 0 && annotation.coordinateGeoPoint.getLongitude() != 0){
                                    Log.d(Constants.TAG, "Created annotation's coordinates's coords are: ("+latitude+","+longitude+")");
                                    annotations.add(annotation);
                                }
                            }
                        }
                        callback.gotRouteAnnotations(annotations);
                    }
                } else {
                    Log.e(Constants.TAG, "Error getting RouteAnnotations from database: "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    interface RouteAnnotationCallback{
        void gotRouteAnnotations(ArrayList<RouteAnnotation> routeAnnotations);
    }
}
