package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class HistoryDetailsActivity extends AppCompatActivity {

    private static History mHistory;
    private TextView mHeaderTitle;
    private ImageView mHeaderPicture;
    private TextView mHistoryText;
    private ImageView mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_details);

        connectUi();
    }

    private void connectUi() {
        mHeaderTitle = (TextView)findViewById(R.id.header_title_text);
        mHeaderPicture = (ImageView)findViewById(R.id.header_picture);
        mHistoryText = (TextView)findViewById(R.id.history_text);
        mBackButton = (ImageView)findViewById(R.id.back_button);
        setListeners();
        populateUi();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void populateUi(){
        if (mHistory != null){
            mHeaderTitle.setText(mHistory.title);
            mHistoryText.setText(Html.fromHtml(mHistory.text));
            mHistory.getHistoryImage(new History.ImageCallback() {
                @Override
                public void gotImage(byte[] image) {
                    if (image != null){
                        Glide.with(HistoryDetailsActivity.this)
                                .load(image)
                                .centerCrop()
                                .into(mHeaderPicture);
                    }
                }
            });
        }
    }

    static void displayHistoryDetailsActivity(History history, Activity activity){
        mHistory = history;
        Intent i = new Intent(activity, HistoryDetailsActivity.class);
        activity.startActivity(i);
    }
}
