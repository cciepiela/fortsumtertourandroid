package com.netgalaxystudios.fortsumtertours;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by coryciepiela on 3/24/17.
 */

class History {
    String objectId;
    String text;
    String title;
    ParseFile imageFile;

    private History(ParseObject object) {
        if (object.getString(Constants.History.title) != null){
            title = object.getString(Constants.History.title);
        }
        if (object.getString(Constants.History.text) != null){
            text = object.getString(Constants.History.text);
        }
        if (object.getString(Constants.History.objectId) != null){
            objectId = object.getString(Constants.History.objectId);
        }
        if (object.getParseFile(Constants.History.imageFile) != null){
            imageFile = object.getParseFile(Constants.History.imageFile);
        }
    }

    void getHistoryImage(final ImageCallback callback){
        if (imageFile != null){
            imageFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null){
                        callback.gotImage(data);
                    } else {
                        Log.e(Constants.TAG, "Error getting History image: "+e.getMessage());
                        e.printStackTrace();
                        callback.gotImage(null);
                    }
                }
            });
        }
    }

    static void getHistories(String forObjectId, final GetHistoriesCallback callback){
        final ArrayList<History> histories = new ArrayList<>();
        ParseQuery<ParseObject> query = getQuery();
        Log.d(Constants.TAG, "forObjectId string is: "+forObjectId);
        if (forObjectId != null){
            query.whereEqualTo(Constants.History.objectId, forObjectId);
        }
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null){
                    Log.d(Constants.TAG, "e was null and objects' size is: "+objects.size());
                    for (ParseObject object:objects){
                        History history = new History(object);
                        histories.add(history);
                    }
                    callback.gotHistories(histories);
                } else {
                    Log.e(Constants.TAG, "Error getting histories: "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private static ParseQuery<ParseObject> getQuery(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.History.className);
        query.orderByAscending(Constants.History.title);
        return query;
    }

    interface ImageCallback{
        void gotImage(byte[] image);
    }

    interface GetHistoriesCallback{
        void gotHistories(ArrayList<History> histories);
    }

}
