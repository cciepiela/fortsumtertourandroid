package com.netgalaxystudios.fortsumtertours;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class OverviewActivity extends AppCompatActivity {

    private TextView mWelcomeTextOne;
    private TextView mWelcomeTextTwo;
    private TextView mBodyText;
    private ImageView mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);

        connectUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(Constants.TAG, "Overview Activity's onResume was called!");
        if (mBodyText != null){
            mBodyText.setText(Html.fromHtml(Config.getCurrentConfig().overviewText));
        }
    }

    private void connectUi() {
        Log.d(Constants.TAG, "Overview Activity's connectUi was called!");
        mWelcomeTextOne = (TextView)findViewById(R.id.welcome_text_view_one);
        mWelcomeTextTwo = (TextView)findViewById(R.id.welcome_text_view_two);
        mBodyText = (TextView)findViewById(R.id.body_text_view);
        mBodyText.setText(Html.fromHtml(Config.getCurrentConfig().overviewText));
        mBodyText.setMovementMethod(LinkMovementMethod.getInstance());
        mBackButton = (ImageView)findViewById(R.id.back_button);
        setListeners();
        setTypeFace();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setTypeFace(){
        MethodHelper.setTextViewTypeFaceRegular(mWelcomeTextOne, OverviewActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(mWelcomeTextTwo, OverviewActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(mBodyText, OverviewActivity.this);
    }
}
