package com.netgalaxystudios.fortsumtertours;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import static java.lang.Math.*;

import com.google.android.cameraview.CameraView;
import com.hoan.dsensor_master.DProcessedSensor;
import com.hoan.dsensor_master.DSensorEvent;
import com.hoan.dsensor_master.DSensorManager;
import com.hoan.dsensor_master.interfaces.DProcessedEventListener;
import com.parse.ParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

public class CustomARActivity extends AppCompatActivity implements LocationListener, SensorEventListener, ARMarker.ARMarkerListener {
    Location currentLocation = null;
    double currentAzimuth = -50;
    private LocationManager mLocationManager;
    private final double DISTANCE_CONSTANT = 15.0;
    private final double MAXIMUM_AR_DISTANCE = 15.0;
    static ArrayList<PoI> locations = new ArrayList<>();
    private CameraView mCameraView;
    private RelativeLayout containerLayout;
    HashMap<String, ARMarker> textViewHashMap = new HashMap<>();
    static int AR_SENSITIVITY = 100;
    private float[] mGravity;
    private float[] mGeomagnetic;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    // Gravity rotational data
    private float gravity[];
    // Magnetic rotational data
    private float magnetic[]; //for magnetic rotational data
    private float accels[] = new float[3];
    private float mags[] = new float[3];
    private float[] values = new float[3];

    // azimuth, pitch and roll
    private float azimuth;
    private float pitch;
    private float roll;
    private CountDownTimer countDownTimer;
    private int inclination;

    private int arMarkerY = 0;
    private int arMarkerHeight = 0;
    private int arMarkerWidth = 0;
    private ImageView mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        setContentView(R.layout.activity_custom_ar);

        currentLocation = (Location) getIntent().getParcelableExtra("coordinates");
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, (long) 0, (float) 0, this);
        } else {
            onBackPressed();
        }
        connectUi();
        setListeners();
        arMarkerY = containerLayout.getHeight() / 2;


    }

    private void connectUi() {
        containerLayout = (RelativeLayout) findViewById(R.id.container_layout);
        mBackButton = (ImageView) findViewById(R.id.back_button);

        mCameraView = (CameraView) findViewById(R.id.camera);
        initializeActivity();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initializeActivity() {
        if (!mCameraView.isCameraOpened()) {
            mCameraView.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL);

        DSensorManager.startDProcessedSensor(this, DProcessedSensor.TYPE_3D_COMPASS, SensorManager.SENSOR_DELAY_NORMAL,
                new DProcessedEventListener() {
                    @Override
                    public void onProcessedValueChanged(DSensorEvent dSensorEvent) {
                        // update UI
                        // dSensorEvent.values[0] is the azimuth.
                        float azimuthRadians = -50000;
                        if (dSensorEvent.values.length > 0) {
                            azimuthRadians = dSensorEvent.values[0];
                        }
                        if (azimuthRadians <= -50000) {
                            return;
                        }
                        double azimuthDegrees = Math.toDegrees(azimuthRadians);

//                        if (azimuthDegrees < 0) {
//                            azimuthDegrees = 360.0 - azimuthDegrees;
//                        }
                        currentAzimuth = azimuthDegrees;
                        for (PoI location : locations) {
                            showMarker(location);
                        }

                    }
                });

    }

    private void showMarker(PoI poI) {
        if (poI.location == null) {
            Log.d(Constants.TAG, "NO_LOCATION: " + poI.location);
            return;
        }
        Location azimuthLocation = getAzimuthLocation(poI.location);
        ARMarker markerView = textViewHashMap.get(poI.objectId);

        if (azimuthLocation != null && shouldShowMarker(azimuthLocation, poI.location)) {
            if (markerView == null) {
                addMarker(poI);
            } else {
                updateMarker(markerView, poI, azimuthLocation);

            }

        }else if (markerView != null && markerView.getVisibility() == View.VISIBLE) {
            markerView.setVisibility(View.INVISIBLE);
        }
    }

    private ARMarker addMarker(PoI poI) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ARMarker markerView = (ARMarker) inflater.inflate(R.layout.ar_marker, null);

        textViewHashMap.put(poI.objectId, markerView);
        markerView.setVisibility(View.INVISIBLE);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(1500, 1000);
        params.topMargin = containerLayout.getHeight() / 2;
        params.leftMargin = containerLayout.getWidth() / 2;


        containerLayout.addView(markerView, params);
        markerView.populate(poI, this, this);
        return markerView;
    }

    private void updateMarker(ARMarker markerView, PoI poI, Location azimuthLocation) {
        float distanceBetweenARLocationAndAzimuth = azimuthLocation.distanceTo(poI.location);
        double adjustedDistance = (DISTANCE_CONSTANT / currentLocation.distanceTo(azimuthLocation)) * distanceBetweenARLocationAndAzimuth;
        arMarkerHeight = markerView.getHeight();
        arMarkerWidth = markerView.getWidth();
        int x = getX(adjustedDistance, poI.location);
        //  Log.d(Constants.TAG, "GOT_X: "+x);
        if (!markerView.animating) {
            final ARMarker finalMarkerView = markerView;
            markerView.animate().setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    finalMarkerView.animating = true;
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    finalMarkerView.animating = false;
                    if (finalMarkerView.getVisibility() == View.INVISIBLE || finalMarkerView.getVisibility() == View.GONE) {
                        finalMarkerView.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            }).setDuration(300).x(x).y(arMarkerY + new Random().nextInt(10));
        }
    }


    private int getX(double fromAdjustedDistance, Location location) {
        if (locationIsToTheRight(location)) {
            return (containerLayout.getWidth() / 2 - arMarkerWidth / 2) + (int) fromAdjustedDistance * AR_SENSITIVITY;
        } else {
            return (containerLayout.getWidth() / 2 - arMarkerWidth / 2) - (int) fromAdjustedDistance * AR_SENSITIVITY;
        }
    }

    public int pxToDp(int px) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private boolean locationIsToTheRight(Location location) {
        Location azimuthLocation = getAzimuthLocation(location);
        // Log.d(Constants.TAG, "locationIsToTheRight: " + azimuthLocation.bearingTo(location));
        double azimuthBearing = currentLocation.bearingTo(azimuthLocation);
        double arBearing = currentLocation.bearingTo(location);
        if (azimuthBearing < 90 && arBearing > 270) {
            return false;
        } else if (azimuthBearing > 270 && arBearing < 90) {
            return true;
        } else {
            return azimuthBearing < arBearing;
        }

    }

    private boolean shouldShowMarker(Location azimuthLocation, Location arLocation) {
        if (inclination < 50 || inclination > 140) {
            return false;
        }
        float distanceBetweenARLocationAndAzimuth = azimuthLocation.distanceTo(arLocation);
        double adjustedDistance = (DISTANCE_CONSTANT / currentLocation.distanceTo(azimuthLocation)) * distanceBetweenARLocationAndAzimuth;
        // Log.d(Constants.TAG, "DISTANCE_BETWEEN: "+adjustedDistance);
        return adjustedDistance < MAXIMUM_AR_DISTANCE;
    }

    Location getAzimuthLocation(Location forLocation) {
        if (currentLocation == null) {
            Log.d(Constants.TAG, "NO_LOCATION");
            return null;
        }
        double brngRad = toRadians(currentAzimuth);
        double latRad = toRadians(currentLocation.getLatitude());
        double lonRad = toRadians(currentLocation.getLongitude());
        int earthRadiusInMetres = 6371000;
        double distFrac = forLocation.distanceTo(currentLocation) / earthRadiusInMetres;

        double latitudeResult = asin(sin(latRad) * cos(distFrac) + cos(latRad) * sin(distFrac) * cos(brngRad));
        double a = atan2(sin(brngRad) * sin(distFrac) * cos(latRad), cos(distFrac) - sin(latRad) * sin(latitudeResult));
        double longitudeResult = (lonRad + a + 3 * PI) % (2 * PI) - PI;

        Location azimuthLocation = new Location("");
        azimuthLocation.setLatitude(toDegrees(latitudeResult));
        azimuthLocation.setLongitude(toDegrees(longitudeResult));
        return azimuthLocation;
    }

    @Override
    protected void onPause() {
        DSensorManager.stopDSensor();
        super.onPause();
        Log.d(Constants.TAG, "onPause was called!");
        if (mCameraView.isCameraOpened()) {
            Log.d(Constants.TAG, "cameraView was opened in onPause and will be stopped!");
            mCameraView.stop();
        }
        mSensorManager.unregisterListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:sss", Locale.getDefault());
        Log.d(Constants.TAG, "onStart was called at: " + dateFormat.format(new Date()));
        initializeActivity();
    }

    static void display(final Activity activity) {
        MethodHelper.showProgressHUDWithMessage("Loading...", activity);
        MethodHelper.getCurrentLocation(activity, new MethodHelper.GetLocationCallback() {
            @Override
            public void gotLocation(final Location location, ParseException e) {
                MethodHelper.hideHUD();
                if (location != null) {
                    MethodHelper.showProgressHUDWithMessage("Loading...", activity);
                    PoI.fetchPoIs(new PoI.PoICallback() {
                        @Override
                        public void gotPoIs(ArrayList<PoI> poIs) {
                            MethodHelper.hideHUD();
                            CustomARActivity.locations = poIs;
                            Intent i = new Intent(activity, CustomARActivity.class);
                            i.putExtra("coordinates", location);
                            activity.startActivity(i);
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CustomARActivity.locations = null;
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_MAGNETIC_FIELD:
                mags = sensorEvent.values.clone();
                break;
            case Sensor.TYPE_ACCELEROMETER:
                accels = sensorEvent.values.clone();
                break;
        }

        if (mags != null && accels != null) {
            gravity = new float[9];
            magnetic = new float[9];
            SensorManager.getRotationMatrix(gravity, magnetic, accels, mags);
            setDeviceTilt();
            float[] outGravity = new float[9];
            int rotation = ((WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
            if (rotation == 0) // Default display rotation is portrait
                SensorManager.remapCoordinateSystem(gravity, SensorManager.AXIS_MINUS_X, SensorManager.AXIS_Y, outGravity);
            else   // Default display rotation is landscape
                SensorManager.remapCoordinateSystem(gravity, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, outGravity);
            SensorManager.getOrientation(outGravity, values);

            azimuth = values[0];
            pitch = values[1];
            roll = values[2];
            mags = null;
            accels = null;

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private void setDeviceTilt() {
        float[] rotationMatrix = new float[9];
        float[] inclinationMatrix = new float[9];

        SensorManager.getRotationMatrix(rotationMatrix, inclinationMatrix, accels, mags);
        inclination = (int) Math.round(Math.toDegrees(Math.acos(rotationMatrix[8])));


        if (inclination < 90) {
            // face up

            arMarkerY = ((containerLayout.getHeight() / 2) - (arMarkerHeight / 2)) - ((90 - inclination) * 10);

        }

        if (inclination > 90) {
            // face down
            arMarkerY = ((containerLayout.getHeight() / 2) - (arMarkerHeight / 2)) + ((inclination - 90) * 10);


        }
    }

    @Override
    public void clickedReadMore(PoI forPoi) {
        PoIDetailsActivity.display(forPoi, this);

    }
}
