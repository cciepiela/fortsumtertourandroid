package com.netgalaxystudios.fortsumtertours;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.FutureTask;

/**
 * Created by coryciepiela on 3/14/17.
 */

class Boat {
    String mObjectId;
    String mDetails;
    int mWidth;
    int mLength;
    int mLevels;
    String mTitle;
    private ArrayList<String> downtownDepartureRanges= new ArrayList<>();
    private String mPatsPointDepartureString;
    private ParseFile mAmenitiesLvlOneFile;
    private ParseFile mAmenitiesLvlTwoFile;
    private ParseFile mAmenitiesLvlThreeFile;
    File mImage;
    private ParseFile mBeaconsLvlOneFile;
    private byte[] mBeaconsLvlOne;
    private ParseFile mBeaconesLvlTwoFile;
    private byte[] mBeaconsLvlTwo;
    private ParseFile mBeaconsLvlThreeFile;
    private byte[] mBeaconsLvlThree;
    VrVideo mVrVideo;
    private byte[] mAmenitiesLvlOne;
    private byte[] mAmenitiesLvlTwo;
    private byte[] mAmenitiesLvlThree;
    private ParseFile mFloorPlanImage;
    private ParseFile mImageFile;

    private Boat(ParseObject boatObject) {
        if (boatObject != null){
            if (boatObject.getString(Constants.Boat.title) != null){
                mTitle = boatObject.getString(Constants.Boat.title);
            }
            if (boatObject.getString(Constants.Boat.details) != null){
                mDetails = boatObject.getString(Constants.Boat.details);
            }
            if (boatObject.getObjectId() != null){
                mObjectId = boatObject.getObjectId();
            }
            if (boatObject.getNumber(Constants.Boat.width) != null){
                mWidth = (int)boatObject.getNumber(Constants.Boat.width);
            }
            if (boatObject.getNumber(Constants.Boat.length) != null){
                mLength = (int)boatObject.getNumber(Constants.Boat.length);
            }
            if (boatObject.getParseFile(Constants.Boat.imageFile) != null){
                mImageFile = boatObject.getParseFile(Constants.Boat.imageFile);
            }
            if (boatObject.getParseFile(Constants.Boat.amenitiesLvlOneFile) != null){
                mAmenitiesLvlOneFile = boatObject.getParseFile(Constants.Boat.amenitiesLvlOneFile);
            }
            if (boatObject.getParseFile(Constants.Boat.amenitiesLvlTwoFile) != null){
                mAmenitiesLvlTwoFile = boatObject.getParseFile(Constants.Boat.amenitiesLvlTwoFile);
            }
            if (boatObject.getParseFile(Constants.Boat.amenitiesLvlThreeFile) != null){
                mAmenitiesLvlThreeFile = boatObject.getParseFile(Constants.Boat.amenitiesLvlThreeFile);
            }
            if (boatObject.getParseFile(Constants.Boat.beaconsLvlOneFile) != null){
                mBeaconsLvlOneFile = boatObject.getParseFile(Constants.Boat.beaconsLvlOneFile);
            }
            if (boatObject.getParseFile(Constants.Boat.beaconsLvlTwoFile) != null){
                mBeaconesLvlTwoFile = boatObject.getParseFile(Constants.Boat.beaconsLvlTwoFile);
            }
            if (boatObject.getParseFile(Constants.Boat.beaconsLvlThreeFile) != null){
                mBeaconsLvlThreeFile = boatObject.getParseFile(Constants.Boat.beaconsLvlThreeFile);
            }
            if (boatObject.getList(Constants.Boat.downtownDepartureRanges) != null){
                List<String> stringList = boatObject.getList(Constants.Boat.downtownDepartureRanges);
                downtownDepartureRanges.addAll(stringList);
            }
            if (boatObject.getNumber(Constants.Boat.levels) != null){
                mLevels = (int)boatObject.getNumber(Constants.Boat.levels);
            }
        }
    }

    static void fetchAllBoats(final BoatInfoCallback callback){
        final ArrayList<Boat> boats = new ArrayList<>();
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Boat.className);
        query.whereNotEqualTo(Constants.Boat.active, false);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null){
                    for (ParseObject object:objects){
                        Boat boat = new Boat(object);
                        boats.add(boat);
                    }
                    callback.gotBoatInfo(boats);
                } else {
                    Log.e(Constants.TAG, "Error fetching boat information: "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    void getBoatImage(final BoatImageCallback callback){
        if (mImageFile != null){
            mImageFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null){
                        callback.gotBoatImage(data);
                    } else {
                        callback.gotBoatImage(null);
                        Log.e(Constants.TAG, "Error getting boat image: "+e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    void getFloorPlanImage(boolean forAmenities, int level, final ImageCallback callback){
        if (level > 3){
            Log.d(Constants.TAG, "Level can not be greater than three!");
            callback.gotImage(null);
            return;
        }
        byte[] image;
        if (!forAmenities){
            image = mBeaconsLvlOne;
            mFloorPlanImage = mBeaconsLvlOneFile;
            if (level == 2){
                image = mBeaconsLvlTwo;
                mFloorPlanImage = mBeaconesLvlTwoFile;
            } else if (level == 3){
                image = mBeaconsLvlThree;
                mFloorPlanImage = mBeaconsLvlThreeFile;
            }
        } else {
            image = mAmenitiesLvlOne;
            mFloorPlanImage = mAmenitiesLvlOneFile;
            if (level == 2){
                image = mAmenitiesLvlTwo;
                mFloorPlanImage = mAmenitiesLvlTwoFile;
            } else if (level == 3){
                image = mAmenitiesLvlThree;
                mFloorPlanImage = mAmenitiesLvlThreeFile;
            }
        }

        if (image != null){
            callback.gotImage(image);
        }else if (mFloorPlanImage != null){
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    Log.d(Constants.TAG, "run() was called!");
                    mFloorPlanImage.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] data, ParseException e) {
                            if (e != null){
                                Log.e(Constants.TAG, "Error getting floorPlanImage: "+e.getMessage());
                                e.printStackTrace();
                            }
                            if (data != null){
                                callback.gotImage(data);
                            } else {
                                callback.gotImage(null);
                                if (e != null){
                                    Log.e(Constants.TAG, "Error getting image: "+e.getMessage());
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
            };
            FutureTask<?> futureTask = new FutureTask<Void>(r, null);
            futureTask.run();

        } else {
            callback.gotImage(null);
        }
    }

    String getDepartureLocation(){
        String departureLocation = "Patriot's Point - Mt. Pleasant";
        for (String range:downtownDepartureRanges) {
            String[] downtownStringArray = range.split("-");
            if (downtownStringArray.length >= 2) {
                String beginningString = downtownStringArray[0];
                String endingString = downtownStringArray[1];
                if (!beginningString.contains(":")) {
                    beginningString = beginningString + ":00";
                }
                if (!endingString.contains(":")) {
                    endingString = endingString + ":00";
                }
                int beginningMinutes = getMinutesFromString(beginningString);
                int endingMinutes = getMinutesFromString(endingString);
                int currentMinutes = getMinutesFromCurrentDate();
                Log.d(Constants.TAG, "beginningMinutes: "+beginningMinutes+" endingMinutes: "+beginningMinutes +" currentMinutes: "+beginningMinutes + " LENGTH: ");
                if (currentMinutes >= beginningMinutes && currentMinutes <= endingMinutes) {
                    departureLocation = "Liberty Square - Charleston";
                }
            }
        }
        return departureLocation;
    }

    private int getMinutesFromCurrentDate () {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        String dateString = format.format(new Date());
        return getMinutesFromString(dateString);
    }

    private int getMinutesFromString (String string) {
        String[] array = string.split(":");
        if (array.length <= 1) {
            return 0;
        }else {
            int hours=getInt(array[0]);
            int minutes=getInt(array[1]);
            return minutes + (60 * hours);
        }
    }

    private int getInt(String fromString) {
        try {
            int myNum = Integer.parseInt(fromString);
            return myNum;
        } catch(NumberFormatException nfe) {
            return 0;
        }
    }

    interface ImageCallback{
        void gotImage(byte[] image);
    }

    interface BoatImageCallback{
        void gotBoatImage(byte[] imageData);
    }

    interface BoatInfoCallback{
        void gotBoatInfo(ArrayList<Boat> boatArrayList);
    }
}
