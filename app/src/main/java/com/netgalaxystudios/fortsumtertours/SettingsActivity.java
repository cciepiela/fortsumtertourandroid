package com.netgalaxystudios.fortsumtertours;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import de.cketti.shareintentbuilder.ShareIntentBuilder;

public class SettingsActivity extends AppCompatActivity {

    private ImageView mBackArrow;
    private ImageView mShareButton;
    private TextView mSettingsVersionBuild;
    private RelativeLayout mSettingsTermsOfUsePrivacyPolicy;
    private RelativeLayout mSettingsCustomerSupport;
    private TextView mEmailTextView;
    private TextView firstNameTextView;
    private TextView lastNameTextView;
    private TextView locationTextView;
    private RelativeLayout frequentlyAskedQuestions;
    private RelativeLayout privacyPolicy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        connectUI();
        populateUI();
        setListeners();
    }

    private void setListeners () {
        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        frequentlyAskedQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextActivity.displayTextActivity(SettingsActivity.this, TextActivity.TextType.FAQ);
            }
        });
        mSettingsCustomerSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { Config.getCurrentConfig().supportEmail });
                intent.putExtra(Intent.EXTRA_SUBJECT, "Fort Sumter Tours Customer Support");
                intent.putExtra(Intent.EXTRA_TEXT, "I need help with Fort Sumter Tours. Here is my app information. "
                        + MethodHelper.getVersionNumberString(SettingsActivity.this));
                startActivity(Intent.createChooser(intent, ""));
            }
        });
        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextActivity.displayTextActivity(SettingsActivity.this, TextActivity.TextType.PrivacyPolicy);
            }
        });
        mSettingsTermsOfUsePrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextActivity.displayTextActivity(SettingsActivity.this, TextActivity.TextType.TermsAndConditions);
            }
        });
        mShareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareButtonClicked();
            }
        });
    }

    private void shareButtonClicked () {
        ShareIntentBuilder.from(this)
                .ignoreSpecification()
                .text("Check out this cool new app called Fort Sumter Tours! Available for download today! - " + Config.getCurrentConfig().shareUrl)
                .stream(MethodHelper.getUriToResource(SettingsActivity.this, R.drawable.sl_logo), "image/png")
                .share();
    }

    private void connectUI () {
        mSettingsCustomerSupport = (RelativeLayout) findViewById(R.id.customer_support_layout);
        mSettingsTermsOfUsePrivacyPolicy = (RelativeLayout) findViewById(R.id.terms_and_conditions_layout);
        frequentlyAskedQuestions = (RelativeLayout) findViewById(R.id.faq_layout);
        privacyPolicy = (RelativeLayout) findViewById(R.id.privacy_policy_layout);

        mBackArrow = (ImageView) findViewById(R.id.back_arrow);
        mShareButton = (ImageView) findViewById(R.id.share_button);

        mEmailTextView = (TextView) findViewById(R.id.email_text_view);
        firstNameTextView = (TextView) findViewById(R.id.first_name_text_view);
        lastNameTextView = (TextView) findViewById(R.id.last_name_text_view);
        locationTextView = (TextView) findViewById(R.id.location_text_view);

        mSettingsVersionBuild = (TextView) findViewById(R.id.version_build_text_view);

    }

    private void populateUI () {
        mEmailTextView.setText("");
        mEmailTextView.setText(User.getCurrentUser().email);
        firstNameTextView.setText("");
        firstNameTextView.setText(User.getCurrentUser().firstName);
        lastNameTextView.setText("");
        lastNameTextView.setText(User.getCurrentUser().lastName);
        locationTextView.setText("");
        MethodHelper.getCurrentLocation(this, new MethodHelper.GetLocationCallback() {
                    @Override
                    public void gotLocation(Location location, com.parse.ParseException e) {
                        if (location != null) {
                            String locationString = MethodHelper.getCityAndStateFromLocation(SettingsActivity.this, location);
                            if (locationString != null) {
                                locationTextView.setText(locationString);
                            }
                        }
                    }
                });
                mSettingsVersionBuild.setText("");
        mSettingsVersionBuild.setText(MethodHelper.getVersionNumberString(SettingsActivity.this));
    }
}
