package com.netgalaxystudios.fortsumtertours;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by coryciepiela on 4/11/17.
 */

class PoI {
    String objectId = "";
    String title;
    String subTitle;
    String url;
    String bodyText;
    LatLng coordinates;
    Location location;
    ParseFile imageFile;
    double azimuthTheoritical;

    private PoI(ParseObject object) {
        if (object.getObjectId() != null) {
            objectId = object.getObjectId();
        }
        if (object.getString(Constants.PoI.title) != null){
            title = object.getString(Constants.PoI.title);
        }
        if (object.getString(Constants.PoI.subTitle) != null){
            subTitle = object.getString(Constants.PoI.subTitle);
        }
        if (object.getString(Constants.PoI.url) != null){
            url = object.getString(Constants.PoI.url);
        }
        if (object.getString(Constants.PoI.bodyText) != null){
            bodyText = object.getString(Constants.PoI.bodyText);
        }
        if (object.getParseGeoPoint(Constants.PoI.location) != null){
            ParseGeoPoint geoPoint = object.getParseGeoPoint(Constants.PoI.location);
            coordinates = new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude());
            location = new Location(objectId);
            location.setLatitude(geoPoint.getLatitude());
            location.setLongitude(geoPoint.getLongitude());
        }
        if (object.getParseFile(Constants.PoI.imageFile) != null){
            imageFile = object.getParseFile(Constants.PoI.imageFile);
        }
    }

    void getPoiPicture(final History.ImageCallback callback){
        if (imageFile != null){
            imageFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null){
                        if (data.length > 0){
                            callback.gotImage(data);
                        }
                    } else {
                        callback.gotImage(null);
                        Log.e(Constants.TAG, "Error fetching PoI image from file: "+e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    static void fetchPoIs(final PoICallback callback){
        final ArrayList<PoI> poIs = new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PoI.className);
        query.whereNotEqualTo(Constants.PoI.active, false);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null){
                    if (objects.size() > 0){
                        for (ParseObject object:objects){
                            PoI poi = new PoI(object);
                            poIs.add(poi);
                        }
                        callback.gotPoIs(poIs);
                    }
                } else {
                    callback.gotPoIs(null);
                    Log.e(Constants.TAG, "Error fetching ARLocations: "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    interface PoICallback{
        void gotPoIs(ArrayList<PoI> poIs);
    }
}
