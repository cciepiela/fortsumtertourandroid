package com.netgalaxystudios.fortsumtertours;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class FortSumterActivity extends AppCompatActivity {

    private TextView mHistoryButton;
    private TextView mAmenitiesButton;
    private TextView mBeaconLocationButton;
    private TextView mThreeDeeToursButton;
    private TextView mTimeMachineButton;
    private ImageView mBackButton;
    private TextView mHistoryText;
    private TextView mHeaderTitleText;
    private int mOrientation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fort_sumter);

        connectUi();
        DefaultApplication.sBoat = null;
    }

    private void connectUi() {
        MethodHelper.showProgressHUDWithMessage("Loading...", FortSumterActivity.this);
        mHistoryButton = (TextView) findViewById(R.id.history_button);
        mAmenitiesButton = (TextView) findViewById(R.id.amenities_button);
        mBeaconLocationButton = (TextView) findViewById(R.id.beacon_locations_button);
        mThreeDeeToursButton = (TextView) findViewById(R.id.three_d_tours_button);
        mTimeMachineButton = (TextView) findViewById(R.id.time_machine_button);
        mBackButton = (ImageView) findViewById(R.id.back_button);
        mHistoryText = (TextView) findViewById(R.id.history_text);
        mHeaderTitleText = (TextView) findViewById(R.id.header_title_text);
        FortSumter.fetchFortSumterInfo(new FortSumter.GotFortSumterCallback() {
            @Override
            public void gotFortSumterInfo(FortSumter fortSumterObject) {
                DefaultApplication.sFortSumter = fortSumterObject;
                populateUi(fortSumterObject);
            }
        });
        setFonts();
        setListeners();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(FortSumterActivity.this, HistorySelectionActivity.class);
                startActivity(i);
            }
        });
        mAmenitiesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FloorPlanActivity.displayFloorplanActivity(FortSumterActivity.this, FloorPlanActivity.FloorPlanType.AMENITIES, mOrientation);
            }
        });
        mBeaconLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FloorPlanActivity.displayFloorplanActivity(FortSumterActivity.this, FloorPlanActivity.FloorPlanType.BEACONS, mOrientation);
            }
        });
        mThreeDeeToursButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VirtualTourSelectionActivity.display(FortSumterActivity.this, DefaultApplication.sFortSumter.objectId);
            }
        });
        mTimeMachineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(FortSumterActivity.this, TimeMachineSelectionActivity.class);
                startActivity(i);
            }
        });
    }

    private void populateUi(FortSumter fortSumter) {
        mHistoryText.setText(Html.fromHtml(fortSumter.getDetails()));
        mHeaderTitleText.setText(fortSumter.getTitle());
        fortSumter.getFortSumterImage(new FortSumter.GotFortSumterImageCallback() {
            @Override
            public void gotFortSumterImage(byte[] fortSumterImage) {
                if (fortSumterImage != null) {
                    MethodHelper.hideHUD();
                } else {
                    MethodHelper.hideHUD();
                }
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mOrientation = newConfig.orientation;
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mOrientation = newConfig.orientation;
        }
    }

    private void setFonts() {
        TextView historyTitleText = (TextView) findViewById(R.id.history_title_text);
        MethodHelper.setTextViewTypeFaceItalicized(historyTitleText, FortSumterActivity.this);
        TextView headerTitleText = (TextView)findViewById(R.id.header_title_text);
        MethodHelper.setTextViewTypeFaceRegular(headerTitleText, FortSumterActivity.this);
    }
}
