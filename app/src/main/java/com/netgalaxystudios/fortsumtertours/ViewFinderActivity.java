package com.netgalaxystudios.fortsumtertours;

import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ViewFinderActivity extends AppCompatActivity implements
        SurfaceHolder.Callback, MyCurrentLocation.OnLocationChangedListener, MyCurrentAzimuth.OnAzimuthChangedListener {

    private Camera mCamera;
    private SurfaceHolder mSurfaceHolder;
    private boolean isCameraviewOn = false;

    private double mAzimuthReal = 0;
    private static double AZIMUTH_ACCURACY = 5;
    private double mMyLatitude = 0;
    private double mMyLongitude = 0;


    private MyCurrentAzimuth myCurrentAzimuth;
    private MyCurrentLocation myCurrentLocation;

    TextView descriptionTextView;
    private ArrayList<PoI> mPoIs = new ArrayList<>();
    private ImageView mBackButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_finder);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setupLayout();
        setupListeners();
        setAugmentedRealityPoint();
    }

    private void setAugmentedRealityPoint() {
        mPoIs = new ArrayList<>();
        PoI.fetchPoIs(new PoI.PoICallback() {
            @Override
            public void gotPoIs(ArrayList<PoI> poIs) {
                Log.d(Constants.TAG, "when returning poIs the size of array is: "+poIs);
                if (poIs != null) {
                    Log.d(Constants.TAG, "when returning poIs the size of array is: "+poIs.size());
                    mPoIs = poIs;
                }
            }
        });
    }

    public double calculateTeoreticalAzimuth(PoI poi) {
        Log.d(Constants.TAG, "mMylatitude is: " + mMyLatitude + " mMyLongitude is: " + mMyLongitude);
        double dX = poi.coordinates.latitude - mMyLatitude;
        double dY = poi.coordinates.longitude - mMyLongitude;

        double phiAngle;
        double tanPhi;

        Log.d(Constants.TAG, "dX is: " + dX + " dY is: " + dY);

        tanPhi = Math.abs(dY / dX);
        phiAngle = Math.atan(tanPhi);
        phiAngle = Math.toDegrees(phiAngle);

        Log.d(Constants.TAG, "tanPhi is: " + tanPhi + " phiAngle is: " + phiAngle);

        if (dX > 0 && dY > 0) { // I quarter
            Log.d(Constants.TAG, "I quarter called!");
            return phiAngle;
        } else if (dX < 0 && dY > 0) { // II
            Log.d(Constants.TAG, "II quarter called!");
            return 180 - phiAngle;
        } else if (dX < 0 && dY < 0) { // III
            Log.d(Constants.TAG, "III quarter called!");
            return 180 + phiAngle;
        } else if (dX > 0 && dY < 0) { // IV
            Log.d(Constants.TAG, "IV quarter called!");
            return 360 - phiAngle;
        }

        return phiAngle;
    }

    private List<Double> calculateAzimuthAccuracy(double azimuth) {
        double minAngle = azimuth - AZIMUTH_ACCURACY;
        double maxAngle = azimuth + AZIMUTH_ACCURACY;
        List<Double> minMax = new ArrayList<Double>();

        if (minAngle < 0)
            minAngle += 360;

        if (maxAngle >= 360)
            maxAngle -= 360;

        minMax.clear();
        minMax.add(minAngle);
        minMax.add(maxAngle);

        return minMax;
    }

    private boolean isBetween(double minAngle, double maxAngle, double azimuth) {
//        Log.d(Constants.TAG, "in isBetween method, passed in vars are: minAngle: " + minAngle + " maxAngle: " + maxAngle + " azimuth: " + azimuth);
        if (minAngle > maxAngle) {
            if (isBetween(0, maxAngle, azimuth) && isBetween(minAngle, 360, azimuth))
                Log.d(Constants.TAG, "PEANUT BUTTER N JELLY");
            return true;
        } else {
            if (azimuth > minAngle - 7.5 && azimuth < maxAngle + 7.5) {
//                Log.d(Constants.TAG, "DOTA 2 IS A GAME");
                return true;
            }
        }
        return false;
    }

    private void updateDescription() {
        descriptionTextView.setText(" azimuthReal " + mAzimuthReal + " latitude "
                + mMyLatitude + " longitude " + mMyLongitude);
    }

    @Override
    public void onLocationChanged(Location location) {
        mMyLatitude = location.getLatitude();
        mMyLongitude = location.getLongitude();
        for (PoI poi : mPoIs) {
            poi.azimuthTheoritical = calculateTeoreticalAzimuth(poi);
        }
//        Toast.makeText(this, "latitude: " + coordinates.getLatitude() + " longitude: " + coordinates.getLongitude(), Toast.LENGTH_SHORT).show();
//        updateDescription();
    }

    @Override
    public void onAzimuthChanged(float azimuthChangedFrom, float azimuthChangedTo) {
        mAzimuthReal = azimuthChangedTo;
        for (PoI poi : mPoIs) {
            poi.azimuthTheoritical = calculateTeoreticalAzimuth(poi);
        }

        LinearLayout dialogLayout = (LinearLayout) findViewById(R.id.dialog_layout);
        PoI augmentedPOI = null;
        for (PoI poi : mPoIs) {
            double minAngle = calculateAzimuthAccuracy(poi.azimuthTheoritical).get(0);
            double maxAngle = calculateAzimuthAccuracy(poi.azimuthTheoritical).get(1);

            if (isBetween(minAngle, maxAngle, mAzimuthReal)) {
                augmentedPOI = poi;
            }
        }

        if (augmentedPOI != null) {
            setDialogBox(augmentedPOI);
            dialogLayout.setVisibility(View.VISIBLE);
        } else {
            dialogLayout.setVisibility(View.INVISIBLE);
        }

//        updateDescription();
    }

    private void setDialogBox(final PoI poi) {
        TextView poiTitle = (TextView) findViewById(R.id.poi_title);
        if (poi.title != null) {
            poiTitle.setText(poi.title);
        }
//        TextView poiSubTitle = (TextView) findViewById(R.id.poi_sub_title);
//        if (poi.subTitle != null) {
//            poiSubTitle.setText(poi.subTitle);
//        }
        TextView poiDescription = (TextView) findViewById(R.id.poi_description);
        if (poi.bodyText != null) {
            poiDescription.setText(poi.bodyText);
        }
        TextView poiReadMore = (TextView) findViewById(R.id.poi_read_more);
        poiReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebActivity.displayWebActivity(ViewFinderActivity.this, poi.url, poi.title);
            }
        });
        MethodHelper.setTextViewTypeFaceItalicized(poiTitle, ViewFinderActivity.this);
    }

    @Override
    protected void onStop() {
        myCurrentAzimuth.stop();
        myCurrentLocation.stop();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        myCurrentAzimuth.start();
        myCurrentLocation.start();
    }

    private void setupListeners() {
        final RelativeLayout howToUseOverlay = (RelativeLayout) findViewById(R.id.how_to_use_overlay);
        ImageView overlayBackBtn = (ImageView)findViewById(R.id.overlay_back_btn);
        overlayBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        TextView overlayContinueBtn = (TextView)findViewById(R.id.overlay_continue_btn);
        overlayContinueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                howToUseOverlay.setVisibility(View.INVISIBLE);
            }
        });
        myCurrentLocation = new MyCurrentLocation(this);
        myCurrentLocation.buildGoogleApiClient(this);
        myCurrentLocation.start();

        myCurrentAzimuth = new MyCurrentAzimuth(this, this);
        myCurrentAzimuth.start();
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setupLayout() {
//        descriptionTextView = (TextView) findViewById(R.id.cameraTextView);
        mBackButton = (ImageView) findViewById(R.id.back_button);
        getWindow().setFormat(PixelFormat.UNKNOWN);
        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.cameraview);
        mSurfaceHolder = surfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        if (isCameraviewOn) {
            mCamera.stopPreview();
            isCameraviewOn = false;
        }

        if (mCamera != null) {
            try {
                mCamera.setPreviewDisplay(mSurfaceHolder);
                mCamera.startPreview();
                isCameraviewOn = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mCamera = Camera.open();
        mCamera.setDisplayOrientation(90);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
        isCameraviewOn = false;
    }





//    var smoothed   = 0;        // or some likely initial value
//    var smoothing  = 10;       // or whatever is desired
//    var lastUpdate = new Date;
//    function smoothedValue( newValue ){
//        var now = new Date;
//        var elapsedTime = now - lastUpdate;
//        smoothed += elapsedTime * ( newValue - smoothed ) / smoothing;
//        lastUpdate = now;
//        return smoothed;
//    }
}
