package com.netgalaxystudios.fortsumtertours;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class RegisterActivity extends AppCompatActivity {

    private EditText mFirstNameEditText;
    private EditText mLastNameEditText;
    private EditText mEmailEditText;
    private EditText mUsernameEditText;
    private EditText mPasswordEditText;
    private EditText mConfirmEditText;
    private TextView mRegisterButton;
    private ImageView mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        connectUi();
    }

    private void connectUi() {
        mFirstNameEditText = (EditText) findViewById(R.id.first_name_edit_text);
        mLastNameEditText = (EditText) findViewById(R.id.last_name_edit_text);
        mEmailEditText = (EditText) findViewById(R.id.email_edit_text);
        mUsernameEditText = (EditText) findViewById(R.id.username_edit_text);
        mPasswordEditText = (EditText) findViewById(R.id.password_edit_text);
        mConfirmEditText = (EditText) findViewById(R.id.confirm_edit_text);
        mRegisterButton = (TextView) findViewById(R.id.register_button);
        mBackButton = (ImageView)findViewById(R.id.back_button);
        setListeners();
        setFonts();
    }

    private void setListeners() {
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerButtonPressed();
            }
        });
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setFonts(){
        TextView registerText = (TextView)findViewById(R.id.register_text);
        MethodHelper.setTextViewTypeFaceRegular(registerText, RegisterActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(mRegisterButton, RegisterActivity.this);
        MethodHelper.setEditTextTypeFaceRegular(mFirstNameEditText, RegisterActivity.this);
        MethodHelper.setEditTextTypeFaceRegular(mLastNameEditText, RegisterActivity.this);
        MethodHelper.setEditTextTypeFaceRegular(mEmailEditText, RegisterActivity.this);
        MethodHelper.setEditTextTypeFaceRegular(mUsernameEditText, RegisterActivity.this);
        MethodHelper.setEditTextTypeFaceRegular(mPasswordEditText, RegisterActivity.this);
        MethodHelper.setEditTextTypeFaceRegular(mConfirmEditText, RegisterActivity.this);
    }

    private void registerButtonPressed() {
        if (allFieldsAreFilledOut()) {
            MethodHelper.showProgressHUDWithMessage("Registering...", RegisterActivity.this);
            String firstName = mFirstNameEditText.getText().toString().replace(" ", "");
            String lastName = mLastNameEditText.getText().toString().replace(" ", "");
            String email = mEmailEditText.getText().toString().replace(" ", "");
            String username = mUsernameEditText.getText().toString().replace(" ", "");
            String password = mPasswordEditText.getText().toString().replace(" ", "");
            User.registerNewUser(RegisterActivity.this, username, email, password, firstName, lastName);
        }
    }

    private boolean allFieldsAreFilledOut() {
        if (mUsernameEditText.getText() == null) {
            MethodHelper.showAlert(this, "Username Required", "A username is required. Please enter a username and try again");
            return false;
        }
        if (mPasswordEditText.getText() == null) {
            MethodHelper.showAlert(this, "Password Required", "A password is required. Please enter your password and try again");
            return false;
        }
        if (mFirstNameEditText.getText() == null) {
            MethodHelper.showAlert(this, "First Name Required", "A first name is required. Please enter your first name and try again");
            return false;
        }
        if (mLastNameEditText.getText() == null) {
            MethodHelper.showAlert(this, "Last Name Required", "A last name is required. Please enter your last name and try again");
            return false;
        }
        if (mConfirmEditText.getText() == null) {
            MethodHelper.showAlert(this, "Confirmation Password Required", "A confirmation password is required. Please enter your password and try again");
            return false;
        }
        if (mEmailEditText.getText() == null) {
            MethodHelper.showAlert(this, "Email Required", "An email is required. Please enter your email and try again");
            return false;
        }
        if (mPasswordEditText.getText().toString().replace(" ", "").equalsIgnoreCase("")) {
            MethodHelper.showAlert(this, "Password Required", "A password is required. Please enter your password and try again");
            return false;
        }
        if (mConfirmEditText.getText().toString().replace(" ", "").equalsIgnoreCase("")) {
            MethodHelper.showAlert(this, "Confirmation Password Required", "A confirmation password is required. Please enter your password and try again");
            return false;
        }
        if (!mPasswordEditText.getText().toString().equals(mConfirmEditText.getText().toString())) {
            MethodHelper.showAlert(this, "Passwords Do Not Match", "Your passwords do not match. Please re-enter your password and try again");
            return false;
        }
        if (mEmailEditText.getText().toString().replace(" ", "").equalsIgnoreCase("")) {
            MethodHelper.showAlert(this, "Email Required", "An email is required. Please enter your email and try again");
            return false;
        }
        if (mUsernameEditText.getText().toString().replace(" ", "").equalsIgnoreCase("")) {
            MethodHelper.showAlert(this, "Username Required", "A username is required. Please enter a username and try again");
            return false;
        }
        if (mFirstNameEditText.getText().toString().replace(" ", "").equalsIgnoreCase("")) {
            MethodHelper.showAlert(this, "First Name Required", "A first name is required. Please enter your first name and try again");
            return false;
        }
        if (mLastNameEditText.getText().toString().replace(" ", "").equalsIgnoreCase("")) {
            MethodHelper.showAlert(this, "Last Name Required", "A last name is required. Please enter your last name and try again");
            return false;
        }
        return true;
    }

}
