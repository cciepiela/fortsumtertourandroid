package com.netgalaxystudios.fortsumtertours;

/**
 * Created by coryciepiela on 3/24/17.
 */

import android.util.Log;

import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

class FortSumter {
    private ParseFile beaconsParseFile;
    private ParseFile amenitiesParseFile;
    private ParseFile imageParseFile;
    private String title;
    private String details;
    byte[] imageData;
    String objectId;

    private FortSumter(ParseObject object) {
        if (object.getParseFile(Constants.FortSumter.beaconsFile) != null){
            beaconsParseFile = object.getParseFile(Constants.FortSumter.beaconsFile);
        }
        if (object.getParseFile(Constants.FortSumter.amenitiesFile) != null){
            amenitiesParseFile = object.getParseFile(Constants.FortSumter.amenitiesFile);
        }
        if (object.getParseFile(Constants.FortSumter.imageFile) != null){
            imageParseFile = object.getParseFile(Constants.FortSumter.imageFile);
        }
        if (object.getString(Constants.FortSumter.title) != null){
            title = object.getString(Constants.FortSumter.title);
        }
        if (object.getString(Constants.FortSumter.details) != null){
            details = object.getString(Constants.FortSumter.details);
        }
        if (object.getObjectId() != null){
            objectId = object.getObjectId();
        }
    }

    static void fetchFortSumterInfo(final GotFortSumterCallback callback){
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.FortSumter.className);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e == null){
                    FortSumter fortSumter = new FortSumter(object);
                    callback.gotFortSumterInfo(fortSumter);
                } else {
                    Log.e(Constants.TAG, "Error getting FortSumterObject: "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    void getFortSumterImage(final GotFortSumterImageCallback callback){
        if (imageParseFile != null){
            imageParseFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null){
                        imageData = data;
                        callback.gotFortSumterImage(data);
                    } else {
                        Log.e(Constants.TAG, "Error getting FortSumterImage: "+e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    void getFloorPlanImage(final boolean forAmenities, final ImageCallback callback){
        ParseFile imageFile;
        if (!forAmenities){
            imageFile = beaconsParseFile;
        } else {
            imageFile = amenitiesParseFile;
        }
        if (imageFile != null){
            imageFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e != null){
                        Log.e(Constants.TAG, "Error getting floorPlanImage: "+e.getMessage());
                        e.printStackTrace();
                    }
                    if (data != null){
                        callback.gotImage(data);
                    } else {
                        callback.gotImage(null);
                        if (e != null){
                            Log.e(Constants.TAG, "Error getting image: "+e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }
            });
        } else {
            callback.gotImage(null);
        }
    }


    String getTitle() {
        return title;
    }

    String getDetails() {
        return details;
    }

    interface ImageCallback{
        void gotImage(byte[] image);
    }

    interface GotFortSumterImageCallback{
        void gotFortSumterImage(byte[] fortSumterImage);
    }

    interface GotFortSumterCallback{
        void gotFortSumterInfo(FortSumter fortSumterObject);
    }
}

