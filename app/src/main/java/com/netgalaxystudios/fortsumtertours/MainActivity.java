package com.netgalaxystudios.fortsumtertours;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.analytics.GoogleAnalytics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ScheduledExecutorService;


public class MainActivity extends AppCompatActivity {

    private LinearLayout mOverviewButton;
    private LinearLayout mTourButton;
    private LinearLayout mPostcardButton;
    private LinearLayout mViewfinderButton;
    private boolean displayedPermissions = false;
    private RelativeLayout mTimerLayout;
    private View mTimerLayoutBackground;
    private TextView mTapText;
    private TextView mDepartureText;
    private RelativeLayout mTimerButton;
    private TextView mDepartureTimer;
    private Date mTimerDate = null;
    private boolean mViewfinderWithinRange;
    private long mTimerInterval;
    private SimpleDateFormat mTimerDateFormatter;
    boolean layoutHidden = true;
    private ScheduledExecutorService scheduleTaskExecutor;
    private int imageIndex = 0;
    private boolean timerStarted = false;
    private boolean checkingViewfinderDistance = false;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            configureTimer();
        }
    };
    private CountDownTimer countDownTimer;
    private ArrayList<byte[]> photoBitmaps;
    private CountDownTimer mBackgroundImageTimer;
    private ImageView photoImageView;
    private RelativeLayout timerContainerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connectUi();

        Glide.with(this).load(R.drawable.fort_sumter).into(photoImageView);

        checkForBeacon();

        DefaultApplication.initializeGeofences();

    }

    private void checkForBeacon () {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey("beaconObjectId")) {
            String beaconObjectId = this.getIntent().getExtras().getString("beaconObjectId");

            if (beaconObjectId != null) {
                Beacon.getBeacon(beaconObjectId, new Beacon.GetBeaconCallback() {
                    @Override
                    public void gotBeacon(Beacon beacon) {
                        if (beacon != null) {
                            BeaconDetailsActivity.Companion.display(MainActivity.this, beacon);
                        }
                    }
                });
            }
        }
    }



    private void configureTimer() {
        if (shouldShowTimer()) {
            if (countDownTimer == null) {
                toggleTimerLayout(true);
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MY_SHARED_PREFERENCES", MODE_APPEND);
                Long timerDateLong = sharedPreferences.getLong("timerInterval", -1);
                countDownTimer = new CountDownTimer(timerDateLong - (new Date()).getTime(), 1000) {
                    @Override
                    public void onTick(long l) {
                        int secondsUntilFinished = (int) (l / 1000);
                        int minutesToDisplay = secondsUntilFinished / 60;
                        int hoursToDisplay = minutesToDisplay / 60;
                        int secondsToDisplay = secondsUntilFinished % 60;
                        String timeString = "";
                        if (hoursToDisplay > 0) {
                            timeString = hoursToDisplay + ":";
                        }
                        String secondString = secondsToDisplay + "";
                        if (secondsToDisplay < 10) {
                            secondString = "0" + secondsToDisplay;
                        }
                        if (minutesToDisplay < 10) {
                            timeString = timeString + "0" + minutesToDisplay + ":" + secondString;
                        } else {
                            timeString = timeString + minutesToDisplay + ":" + secondString;
                        }
                        mDepartureTimer.setText(timeString);
                    }

                    @Override
                    public void onFinish() {
                        timerContainerView.setVisibility(View.GONE);
                        MethodHelper.showAlert(MainActivity.this, Config.getCurrentConfig().boatDepartingTitleText, Config.getCurrentConfig().boatDepartingText);
                    }
                };
                countDownTimer.start();
            }
        } else {
            timerContainerView.setVisibility(View.GONE);
        }
    }

    private boolean shouldShowTimer() {

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MY_SHARED_PREFERENCES", MODE_APPEND);
        Long timerDateLong = sharedPreferences.getLong("timerInterval", -1);
        if (timerDateLong < 0) {
            return false;
        }
        Date timerDate = new Date(timerDateLong);
        Log.d(Constants.TAG, "SHOW_TIMER: " + timerDate);
        return false;
//        return timerDate.after(new Date());
    }

    private void connectUi() {
        timerContainerView = (RelativeLayout) findViewById(R.id.timer_container_view);

        photoBitmaps = new ArrayList<>();
        photoImageView = (ImageView)findViewById(R.id.background_image);
        mOverviewButton = (LinearLayout) findViewById(R.id.overview_button);
        mTourButton = (LinearLayout) findViewById(R.id.tour_button);
        mPostcardButton = (LinearLayout) findViewById(R.id.postcard_button);
        mDepartureText = (TextView) findViewById(R.id.departure_text);
        mDepartureTimer = (TextView) findViewById(R.id.departure_timer);
        mTimerButton = (RelativeLayout) findViewById(R.id.timer_button);
        mTimerLayoutBackground = (View) findViewById(R.id.red_buffer);
        mTapText = (TextView) findViewById(R.id.tap_text);
        mTimerLayout = (RelativeLayout) findViewById(R.id.timer_layout);
        mViewfinderButton = (LinearLayout) findViewById(R.id.viewfinder_button);
        FeaturedPhoto.fetchFeaturedPhotos(new FeaturedPhoto.GetFeaturedPhotosCallback() {
            @Override
            public void gotFeaturedPhotos(ArrayList<FeaturedPhoto> featuredPhotos) {
                if (featuredPhotos != null) {
                    for (FeaturedPhoto photo : featuredPhotos) {
                        photo.getImage(new MethodHelper.GetImageCallback() {
                            @Override
                            public void gotImage(byte[] image) {
                                if (image != null) {
                                    photoBitmaps.add(image);
                                    if (!timerStarted) {
                                        timerStarted = true;
                                        startImageTimer();
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        MenuLayout mainDrawerLayout = (MenuLayout) findViewById(R.id.activity_main_drawer_layout);
        mainDrawerLayout.setup(this);
        setListeners();
        setFonts();
    }

    private void startImageTimer () {
        Log.d(Constants.TAG, "startImageTimer");
        new CountDownTimer(500000000, 10000) {

            @Override
            public void onTick(long l) {
                if (photoBitmaps.size() <= 0) {
                    return;
                }
                if (imageIndex >= photoBitmaps.size()) {
                    imageIndex = 0;
                }
                if (MainActivity.this != null && !MainActivity.this.isDestroyed()) {
                    Glide
                            .with(MainActivity.this)
                            .load(photoBitmaps.get(imageIndex))
                            .animate(R.anim.slide_in_left)
                            .into(photoImageView);
                }
                imageIndex++;
            }

            @Override
            public void onFinish() {
                startImageTimer();
            }
        }.start();
    }

    private void toggleTimerLayout(final boolean shouldShow) {
        final float amountToMoveDown = pxFromDp(this, (float) 50);
        TranslateAnimation showViewAnimation = new TranslateAnimation(0, 0, -amountToMoveDown, 0);
        if (!shouldShow) {
            showViewAnimation = new TranslateAnimation(0, 0, 0, -amountToMoveDown);
            mTapText.setText(getResources().getString(R.string.tap_to_open));
        } else {
            mTapText.setText(getResources().getString(R.string.tap_to_close));
        }
        showViewAnimation.setDuration(300);
        showViewAnimation.initialize(timerContainerView.getWidth(), timerContainerView.getHeight(), timerContainerView.getWidth(), timerContainerView.getHeight());
        showViewAnimation.setFillAfter(true);
        timerContainerView.startAnimation(showViewAnimation);
        layoutHidden = !shouldShow;
    }

    private static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }


    private void setFonts() {
        TextView phraseTextView = (TextView) findViewById(R.id.phrase_text_view);
        TextView phraseTextViewTwo = (TextView) findViewById(R.id.phrase_text_view_two);
        TextView overviewLabel = (TextView) findViewById(R.id.over_view_label);
        TextView vrTourLabel = (TextView) findViewById(R.id.vr_tour_label);
        TextView postcardLabel = (TextView) findViewById(R.id.postcard_label);
        TextView viewfinderLabel = (TextView) findViewById(R.id.viewfinder_label);
        MethodHelper.setTextViewTypeFaceRegular(phraseTextView, MainActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(phraseTextViewTwo, MainActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(overviewLabel, MainActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(vrTourLabel, MainActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(postcardLabel, MainActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(viewfinderLabel, MainActivity.this);
    }

    private void setListeners() {
        mOverviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, OverviewActivity.class);
                startActivity(i);
            }
        });
        mTourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VirtualTourSelectionActivity.display(MainActivity.this, null);
            }
        });
        mPostcardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, PostcardActivity.class);
                startActivity(i);
            }
        });
        mViewfinderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ARActivity.display(MainActivity.this);
                CustomARActivity.display(MainActivity.this);

            }
        });
        mTimerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleTimerLayout(layoutHidden);
            }
        });
        timerContainerView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleTimerLayout(layoutHidden);
            }
        });
    }

    static void displayMainActivity(Activity activity) {
        Intent i = new Intent(activity, MainActivity.class);
        activity.onBackPressed();

        activity.startActivity(i);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(MainActivity.this).reportActivityStart(MainActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBackgroundImageTimer != null){
            mBackgroundImageTimer.cancel();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

//        MethodHelper.setScreenNameForGa(MainActivity.this);
//        if (!displayedPermissions) {
//            displayedPermissions = true;
//            if (ContextCompat.ch eckSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
//                    || ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                Intent i = new Intent(MainActivity.this, PermissionsActivity.class);
//                startActivity(i);
//            }
//        }
        configureTimer();
        final Handler h = new Handler();
        final int delay = 5000; //milliseconds

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(Constants.BroadcastEventNames.enteredGeofenceWithTimer));

        if (!checkingViewfinderDistance) {
            checkingViewfinderDistance = true;
            h.postDelayed(new Runnable() {
                public void run() {
                    //do something
//                getCurrentLocation();
                    MethodHelper.checkDistanceForViewfinder(MainActivity.this, new MethodHelper.ViewfinderDistanceCallback() {
                        @Override
                        public void gotDistance(boolean withinRange) {
                            checkingViewfinderDistance = false;
                            mViewfinderWithinRange = withinRange;
                        }
                    });

                    h.postDelayed(this, delay);
                }
            }, delay);
        }

    }
}
