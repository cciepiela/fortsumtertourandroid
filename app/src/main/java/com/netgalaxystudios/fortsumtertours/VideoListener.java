package com.netgalaxystudios.fortsumtertours;

import com.google.vr.sdk.widgets.video.VrVideoEventListener;

/**
 * Created by charlesvonlehe on 5/23/17.
 */

public class VideoListener extends VrVideoEventListener {
    private boolean shouldClose = false;
    VideoListenerCallback callback;

    public VideoListener(VideoListenerCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onDisplayModeChanged(int newDisplayMode) {
        if (shouldClose && callback != null) {
            super.onDisplayModeChanged(newDisplayMode);
            callback.closeVrView();
        }
    }

    @Override
    public void onLoadSuccess() {
        super.onLoadSuccess();
        shouldClose = true;
        if (callback != null) {
            callback.didLoad();
        }
    }

    interface VideoListenerCallback {
        void closeVrView();
        void didLoad();
    }
}
