package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private RelativeLayout mFacebookLoginButton;
    private EditText mUsernameEditText;
    private EditText mPasswordEditText;
    private TextView mLoginButton;
    private TextView mForgotPasswordButton;
    private TextView mRegisterButton;
    private SignInButton mGoogleSignInButton;
    private static final int RC_SIGN_IN = 7;
    public static final int rCode = 9343;

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setupGoogle();

        connectUi();
//        Log.d(Constants.TAG, "User.getCurrentUser returns: "+User.getCurrentUser()+" ParseUser.getCurrentUser returns: "+ParseUser.getCurrentUser());
    }

    private void setupGoogle () {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("985055356751-1lt8sj8oq2ksuln65nr0fhqmokq5mgh5.apps.googleusercontent.com")
                .requestEmail().build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                // .addApi(Plus.API, null)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                // .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
    }

    private void connectUi() {
        mFacebookLoginButton = (RelativeLayout) findViewById(R.id.facebook_login_button);
        mUsernameEditText = (EditText) findViewById(R.id.username_edit_text);
        mPasswordEditText = (EditText) findViewById(R.id.password_edit_text);
        mLoginButton = (TextView) findViewById(R.id.login_button);
        mForgotPasswordButton = (TextView) findViewById(R.id.forgot_password_button);
        mRegisterButton = (TextView) findViewById(R.id.register_button);
        ImageView backgroundImage = (ImageView)findViewById(R.id.background_image);
        Glide.with(LoginActivity.this)
                .load(R.drawable.fort_sumter_boat)
                .override(300, 300)
                .into(backgroundImage);
        mGoogleSignInButton = (SignInButton) findViewById(R.id.google_sign_in_button);
        setListeners();
        setFonts();
    }

    private void setFonts(){
        TextView loginText = (TextView)findViewById(R.id.login_text);
        MethodHelper.setTextViewTypeFaceRegular(loginText, LoginActivity.this);
        MethodHelper.setEditTextTypeFaceRegular(mUsernameEditText, LoginActivity.this);
        MethodHelper.setEditTextTypeFaceRegular(mPasswordEditText, LoginActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(mLoginButton, LoginActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(mForgotPasswordButton, LoginActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(mRegisterButton, LoginActivity.this);
    }

    private void setListeners() {
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usernameString;
                String passwordString;
                MethodHelper.showProgressHUDWithMessage("Logging in...", LoginActivity.this);
                if (allFieldsAreFilledOut()) {
                    usernameString = mUsernameEditText.getText().toString().replace(" ", "");
                    passwordString = mPasswordEditText.getText().toString().replace(" ", "");
                    User.loginUser(usernameString, passwordString, new User.LoginUserCallback() {
                        @Override
                        public void loggedInWithParseException(boolean success, boolean isNew, ParseException e) {
                            MethodHelper.hideHUD();
                            if (success) {
                                SharedPreferences sharedPreferences = getSharedPreferences(Constants.FortSumter.className, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean("LoggedIn", true);
                                editor.commit();
                                User.storeCurrentUserObjectId(LoginActivity.this, false);
                                MainActivity.displayMainActivity(LoginActivity.this);
                            } else {
                                Log.d(Constants.TAG, "ERROR NUMBER " + e.getCode());
                                Log.d(Constants.TAG, "ERROR MESSAGE " + e.getMessage());
                                e.printStackTrace();
                                processError(e, LoginActivity.this);
                            }
                        }
                    });
                }

            }
        });
        mGoogleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleLoginPressed();
            }
        });
        mForgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                final EditText edittext = new EditText(LoginActivity.this);
                alert.setMessage("Please enter the email associated with your account.");
                alert.setTitle("Reset Password");

                alert.setView(edittext);

                alert.setPositiveButton("Reset", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String userEmail = edittext.getText().toString().replace(" ", "");
                        forgotPasswordPressed(userEmail);
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });

                alert.show();
            }
        });
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });
        mFacebookLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebookSignInButtonPressed();
            }
        });
    }

    private void forgotPasswordPressed(String userEmail) {
        if (userEmail.replace(" ", "").length() > 0) {
            ParseUser.requestPasswordResetInBackground(userEmail,
                    new RequestPasswordResetCallback() {
                        public void done(ParseException e) {
                            if (e == null) {
                                MethodHelper.showAlert(LoginActivity.this, "Success", "An email was successfully sent with reset instructions.");
                            } else {
                                Log.d(Constants.TAG, "Error resetting password: " + e.getMessage());
                                e.printStackTrace();
                                MethodHelper.showAlert(LoginActivity.this, "Error", e.getMessage());
                            }
                        }
                    });
        } else {
            MethodHelper.showAlert(LoginActivity.this, "Error", "There was an error, please try again.");
        }
    }

    private void googleLoginPressed () {
        if (mGoogleApiClient != null) {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, rCode);
            MethodHelper.showProgressHUDWithMessage("Logging in...", LoginActivity.this);
        }
    }



    private void facebookSignInButtonPressed() {
        MethodHelper.showProgressHUDWithMessage("Logging in...", LoginActivity.this);
        User.loginWithFacebook(this, new User.LoginUserCallback() {
            @Override
            public void loggedInWithParseException(boolean success, boolean isNew, ParseException e) {
                Log.d(Constants.TAG, "faceBookLoginButtonPressed");
                MethodHelper.hideHUD();
                if (success) {
                    SharedPreferences sharedPreferences = getSharedPreferences(Constants.FortSumter.className, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("LoggedIn", true);
                    editor.commit();
                    User.storeCurrentUserObjectId(LoginActivity.this, false);
                    Log.d(Constants.TAG, "The loggedIn(boolean success, ParseException e) method was called!");
                    MethodHelper.switchToActivity(LoginActivity.this, MainActivity.class);
                } else if (e != null) {
                    processError(e, LoginActivity.this);
                } else {
                    MethodHelper.showAlert(LoginActivity.this, "Error", "There was an error logging in. Please try again");
                }
            }
        });
    }

    private void processError(ParseException e, Activity activity) {
        MethodHelper.hideHUD();
        if (e.getCode() == 101) {
            MethodHelper.showAlert(activity, "Bad Credentials", "The username or password was invalid");
        } else if (e.getCode() == 200) {
            MethodHelper.showAlert(activity, "Username Missing", "The username is missing or empty.");
        } else if (e.getCode() == 201) {
            MethodHelper.showAlert(activity, "Password Missing", "The password is missing or empty.");
        } else if (e.getCode() == 202) {
            MethodHelper.showAlert(activity, "Username Taken", "The username has already been taken.");
        } else if (e.getCode() == 203) {
            MethodHelper.showAlert(activity, "Email Taken", "The email is already being used.");
        } else if (e.getCode() == 204) {
            MethodHelper.showAlert(activity, "Email Missing", "The email is missing or empty.");
        } else if (e.getCode() == 206) {
            MethodHelper.showAlert(activity, "Session Missing", "A user object without a valid session could not be altered.");
        } else if (e.getCode() == 207) {
            MethodHelper.showAlert(activity, "Must Create User Through Signup", "A user can only be created through signup.");
        } else if (e.getCode() == 209) {
            MethodHelper.showAlert(activity, "Invalid Session Token", "There was an error logging in. Please try again.");
        } else {
            Log.d(Constants.TAG, "Error logging in: " + e.getMessage() + " Code: " + e.getCode());
            MethodHelper.showAlert(LoginActivity.this, "Error", "There was an error logging in. Please try again");
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(Constants.TAG, "onConnectionFailed:" + connectionResult);
    }

    private boolean allFieldsAreFilledOut() {
        MethodHelper.hideHUD();
        if (mUsernameEditText.getText() == null) {
            MethodHelper.showAlert(this, "Username Required", "A username is required. Please enter a username and try again");
            return false;
        }
        if (mPasswordEditText.getText() == null) {
            MethodHelper.showAlert(this, "Password Required", "A password is required. Please enter a password and try again");
            return false;
        }
        if (mUsernameEditText.getText().toString().replace(" ", "").equalsIgnoreCase("")) {
            MethodHelper.showAlert(this, "Username Required", "A username is required. Please enter a username and try again");
            return false;
        }
        if (mPasswordEditText.getText().toString().replace(" ", "").equalsIgnoreCase("")) {
            MethodHelper.showAlert(this, "Password Required", "A password is required. Please enter a password and try again");
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == rCode) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }else {
            ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(Constants.TAG, "handleSignInResult:" + result.isSuccess());

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            User.loginWithGoogle(this, result, new MethodHelper.SuccessCallback() {
                @Override
                public void processCompleted(Boolean success) {
                    MethodHelper.hideHUD();
                    if (success) {
                        Log.d(Constants.TAG, "The loggedIn(boolean success, ParseException e) method was called!");
                        MethodHelper.switchToActivity(LoginActivity.this, MainActivity.class);
                    }else {
                        MethodHelper.showAlert(LoginActivity.this, "Error", "There was an error logging in. Please try again");
                    }
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient);

                }
            });
        } else {
            // Signed out, show unauthenticated UI.
            MethodHelper.hideHUD();
            Log.d(Constants.TAG, "handleSignInResultError:" + result.getStatus().getStatusMessage());
            Log.d(Constants.TAG, "handleSignInResultError:" + result.getStatus().getStatusCode());

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
