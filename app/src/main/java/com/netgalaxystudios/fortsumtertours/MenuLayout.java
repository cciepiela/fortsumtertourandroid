package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.cketti.shareintentbuilder.ShareIntentBuilder;

/**
 * Created by coryciepiela on 3/6/17.
 */

public class MenuLayout extends DrawerLayout {

    private LinearLayout mOverviewButton;
    private LinearLayout mBoatDetailsButton;
    private LinearLayout mFortSumterButton;
    private LinearLayout mMarineLifeButton;
    private LinearLayout mVrToursButton;
    private LinearLayout mRoutesButton;
    private LinearLayout mTimeMachineButton;
    private LinearLayout mPostcardButton;
    private LinearLayout mViewfinderButton;
    private LinearLayout mShareButton;
    private LinearLayout mSettingsButton;
    private LinearLayout mLogoutButton;
    private ImageView mMenuButton;
    private Activity mActivity;
    private DrawerLayout mMainDrawerLayout;
    private TextView mEditProfileButton;
    private TextView mGreetingText;

    public MenuLayout(Context context) {
        super(context);
    }

    public MenuLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MenuLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setup(Activity activity){
        mActivity = activity;
        mGreetingText = (TextView)findViewById(R.id.menu_greeting_text);
        mGreetingText.setText(getResources().getString(R.string.menu_greeting, User.getCurrentUser().firstName));
        mOverviewButton = (LinearLayout)activity.findViewById(R.id.menu_overview_button);
        mBoatDetailsButton = (LinearLayout)activity.findViewById(R.id.menu_boat_details_button);
        mFortSumterButton = (LinearLayout)activity.findViewById(R.id.menu_fort_sumter_button);
        mMarineLifeButton = (LinearLayout)activity.findViewById(R.id.menu_marine_life_button);
        mVrToursButton = (LinearLayout)activity.findViewById(R.id.menu_vr_tours_button);
        mRoutesButton = (LinearLayout)activity.findViewById(R.id.menu_routes_button);
        mTimeMachineButton = (LinearLayout)activity.findViewById(R.id.menu_time_machine_button);
        mPostcardButton = (LinearLayout)activity.findViewById(R.id.menu_postcard_button);
        mViewfinderButton = (LinearLayout)activity.findViewById(R.id.menu_viewfinder_button);
        mShareButton = (LinearLayout)activity.findViewById(R.id.menu_share_button);
        mSettingsButton = (LinearLayout)activity.findViewById(R.id.menu_settings_button);
        mEditProfileButton = (TextView)activity.findViewById(R.id.menu_edit_profile_button);
        mLogoutButton = (LinearLayout)activity.findViewById(R.id.menu_logout_button);
        mMenuButton = (ImageView)findViewById(R.id.menu_button);
        mMainDrawerLayout = (DrawerLayout) activity.findViewById(R.id.activity_main_drawer_layout);

        setListeners();
        setFonts();
    }

    private void setFonts(){
        TextView overViewText = (TextView)findViewById(R.id.menu_overview_text);
        TextView fleetText = (TextView)findViewById(R.id.menu_fleet_text);
        TextView marineText = (TextView)findViewById(R.id.menu_marine_text);
        TextView fortSumterText = (TextView)findViewById(R.id.menu_fort_sumter_text);
        TextView vrToursText = (TextView)findViewById(R.id.menu_vr_tours_text);
        TextView routesText = (TextView)findViewById(R.id.menu_routes_text);
        TextView timeMachineText = (TextView)findViewById(R.id.menu_time_machine_text);
        TextView postcardText = (TextView)findViewById(R.id.menu_postcard_text);
        TextView viewFinderText = (TextView)findViewById(R.id.menu_viewfinder_text);
        TextView shareText = (TextView)findViewById(R.id.menu_share_text);
        TextView settingsText = (TextView)findViewById(R.id.menu_settings_text);
        TextView logoutText = (TextView)findViewById(R.id.menu_logout_text);
        MethodHelper.setTextViewTypeFaceRegular(mEditProfileButton, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(mGreetingText, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(overViewText, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(fleetText, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(marineText, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(fortSumterText, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(vrToursText, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(routesText, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(timeMachineText, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(postcardText, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(viewFinderText, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(shareText, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(settingsText, mActivity);
        MethodHelper.setTextViewTypeFaceRegular(logoutText, mActivity);
    }

    private void setListeners(){
        mEditProfileButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mActivity, ProfileActivity.class);
                mActivity.startActivity(i);
            }
        });
        mOverviewButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mActivity, OverviewActivity.class);
                mActivity.startActivity(i);
            }
        });
        mBoatDetailsButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mActivity, BoatSelectionActivity.class);
                mActivity.startActivity(i);
            }
        });
        mFortSumterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mActivity, FortSumterActivity.class);
                mActivity.startActivity(i);
            }
        });
        mMarineLifeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mActivity, MarineLifeActivity.class);
                mActivity.startActivity(i);
            }
        });
        mVrToursButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                VirtualTourSelectionActivity.display(mActivity, null);
            }
        });
        mRoutesButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mActivity, RoutesActivity.class);
                mActivity.startActivity(i);
            }
        });
        mTimeMachineButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mActivity, TimeMachineSelectionActivity.class);
                mActivity.startActivity(i);
            }
        });
        mPostcardButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mActivity, PostcardActivity.class);
                mActivity.startActivity(i);
            }
        });
        mViewfinderButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mActivity, ARActivity.class);
                mActivity.startActivity(i);
            }
        });
        mShareButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareIntentBuilder.from(mActivity)
                        .ignoreSpecification()
                        .text("Check out this cool new app called Fort Sumter Tours! Available for download today! - " + Config.getCurrentConfig().shareUrl)
                        .stream(MethodHelper.getUriToResource(mActivity, R.drawable.sl_logo), "image/png")
                        .share();

                //                        .text("Check out this cool new app called Collective Force! Available for download today! - " + Config.getCurrentConfig().shareUrl)
//                        .stream(MethodHelper.getUriToResource(SettingsActivity.this, R.drawable.logo), "image/png")
            }
        });
        mSettingsButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mActivity, SettingsActivity.class);
                mActivity.startActivity(i);
            }
        });
        mLogoutButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                User.getCurrentUser().logout(mActivity);
            }
        });
        mMenuButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mMainDrawerLayout.openDrawer(GravityCompat.START);
                InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindowToken(), 0);
            }
        });
    }
}
