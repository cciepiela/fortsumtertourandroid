package com.netgalaxystudios.fortsumtertours;

import android.support.annotation.Nullable;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by coryciepiela on 3/14/17.
 */

class VrVideo {
    String objectId = "";
    String text = "";
    int seconds = 0;
    String url = "";
    private ParseFile imageFile;
    private byte[] image;
    private String forObjectId = null;

    private VrVideo(ParseObject object) {
        super();
        if (object.getObjectId() != null) {
            objectId = object.getObjectId();
        }
        if (object.getString(Constants.VrVideo.text) != null) {
            text = object.getString(Constants.VrVideo.text);
        }
        seconds = object.getInt(Constants.VrVideo.seconds);
        if (object.getString(Constants.VrVideo.url) != null) {
            url = object.getString(Constants.VrVideo.url);
        }
        if (object.getParseFile(Constants.VrVideo.imageFile) != null) {
            imageFile = object.getParseFile(Constants.VrVideo.imageFile);
        }
        forObjectId = object.getString(Constants.VrVideo.forObjectId);
    }

    void getImage(final MethodHelper.ImageCallback callback) {
        if (image != null) {
            callback.gotImage(image);
        } else if (imageFile != null) {
            imageFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null) {
                        image = data;
                        callback.gotImage(data);
                    } else {
                        Log.e(Constants.TAG, "Error getting History image: " + e.getMessage());
                        e.printStackTrace();
                        callback.gotImage(null);
                    }
                }
            });
        }
    }

    static void getVrVideos(@Nullable String forObjectId, final GetVrVideo callback) {
        final ArrayList<VrVideo> vrVideos = new ArrayList<>();
        ParseQuery<ParseObject> query = getQuery();
        if (forObjectId != null) {
            query.whereEqualTo(Constants.VrVideo.forObjectId, forObjectId);
        }
        query.setLimit(1000);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    Log.d(Constants.TAG, "e was null and objects' size is: " + objects.size());
                    for (ParseObject object : objects) {
                        VrVideo vrVideo = new VrVideo(object);
                        vrVideos.add(vrVideo);
                    }
                    callback.gotVrVideos(vrVideos);
                } else {
                    Log.e(Constants.TAG, "Error getting histories: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private static ParseQuery<ParseObject> getQuery() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.VrVideo.className);
        query.orderByAscending(Constants.VrVideo.text);
        return query;
    }

    interface GetVrVideo {
        void gotVrVideos(ArrayList<VrVideo> vrVideos);
    }
}
