package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by coryciepiela on 3/30/17.
 */

class TimeMachineAdapter extends RecyclerView.Adapter<TimeMachineAdapter.ViewHolder> {
    private Activity context;
    ArrayList<TimeMachine> searchResults = new ArrayList<TimeMachine>();

    TimeMachineAdapter(Activity context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    @Override
    public TimeMachineAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.time_machine_recycler_item, parent, false);
        return new TimeMachineAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final TimeMachineAdapter.ViewHolder holder, int position) {
        final TimeMachine timeMachine = searchResults.get(position);
        timeMachine.getTimeMachineSecondImage(true, new MethodHelper.ImageCallback() {
            @Override
            public void gotImage(byte[] image) {
                Glide.with(context)
                        .load(image)
                        .override(100, 100)
                        .centerCrop()
                        .into(holder.mTimeMachinePicture);
            }
        });
        holder.mTimeMachineTitle.setText(timeMachine.title);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimeMachineActivity.displayTimeMachineActivity(timeMachine, context);
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTimeMachineTitle;
        private final ImageView mTimeMachinePicture;

        ViewHolder(final View itemView) {
            super(itemView);
            mTimeMachineTitle = (TextView)itemView.findViewById(R.id.time_machine_recycler_title);
            mTimeMachinePicture = (ImageView)itemView.findViewById(R.id.time_machine_recycler_picture);
        }
    }
}
