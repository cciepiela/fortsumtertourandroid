package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TextActivity extends AppCompatActivity {

    private ImageView backButtonLayout;
    private TextView mContentTextView;
    private TextView mTitleText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);
        connectUi();
        setListeners();
        String typeString = "";
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        if (getIntent().getStringExtra(Constants.Extras.TEXT_TYPE_EXTRA) != null) {
            typeString = getIntent().getStringExtra(Constants.Extras.TEXT_TYPE_EXTRA);
        }
        if (TextType.PrivacyPolicy.toString().equalsIgnoreCase(typeString)) {
            progressBar.setVisibility(View.GONE);
            mContentTextView.setText(Html.fromHtml(Config.getCurrentConfig().privacyPolicy));
            mTitleText.setText(R.string.privacy_policy);
        }else if (TextType.FAQ.toString().equalsIgnoreCase(typeString)){
            progressBar.setVisibility(View.GONE);
            mContentTextView.setText(Html.fromHtml(Config.getCurrentConfig().faq));
            mTitleText.setText(R.string.faq_spelled_out);
        }else {
            progressBar.setVisibility(View.GONE);
            mContentTextView.setText(Html.fromHtml(Config.getCurrentConfig().termsAndConditions));
            mTitleText.setText(R.string.terms_and_conditions);
        }
    }

    private void connectUi() {
        backButtonLayout = (ImageView) findViewById(R.id.back_button);
        mContentTextView = (TextView)findViewById(R.id.content_text_view);
        mTitleText = (TextView)findViewById(R.id.title_text_view);
    }

    public static void displayTextActivity (Activity activity, TextType type) {
        Intent i = new Intent(activity, TextActivity.class);
        i.putExtra(Constants.Extras.TEXT_TYPE_EXTRA, type.toString());
        activity.startActivity(i);
    }

    private void setListeners () {
        backButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    enum TextType {TermsAndConditions, PrivacyPolicy, AboutUs, FAQ}
}
