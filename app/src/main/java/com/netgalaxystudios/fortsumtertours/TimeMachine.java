package com.netgalaxystudios.fortsumtertours;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by coryciepiela on 3/30/17.
 */

class TimeMachine {
    String title;
    String subTitle;
    private ParseFile firstImageFile;
    private ParseFile secondImageFile;
    private byte[] firstImage;
    private byte[] secondImage;
    String beforeTitle;
    String afterTitle;
    String beforeTitleColor;
    String afterTitleColor;

    private TimeMachine(ParseObject object) {
        if (object.getString(Constants.TimeMachine.title) != null) {
            title = object.getString(Constants.TimeMachine.title);
        }
        if (object.getString(Constants.TimeMachine.subtitle) != null) {
            subTitle = object.getString(Constants.TimeMachine.subtitle);
        }
        if (object.getParseFile(Constants.TimeMachine.firstImageFile) != null) {
            firstImageFile = object.getParseFile(Constants.TimeMachine.firstImageFile);
        }
        if (object.getParseFile(Constants.TimeMachine.secondImageFile) != null) {
            secondImageFile = object.getParseFile(Constants.TimeMachine.secondImageFile);
        }
        if (object.getString(Constants.TimeMachine.beforeTitle) != null){
            beforeTitle = object.getString(Constants.TimeMachine.beforeTitle);
        }
        if (object.getString(Constants.TimeMachine.afterTitle) != null){
            afterTitle = object.getString(Constants.TimeMachine.afterTitle);
        }
        if (object.getString(Constants.TimeMachine.beforeTitleColor) != null){
            beforeTitleColor = object.getString(Constants.TimeMachine.beforeTitleColor);
        }
        if (object.getString(Constants.TimeMachine.afterTitleColor) != null){
            afterTitleColor = object.getString(Constants.TimeMachine.afterTitleColor);
        }
    }

    static void getTimeMachines(final TimeMachineCallback callback) {
        final ArrayList<TimeMachine> timeMachines = new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.TimeMachine.className);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    if (objects.size() > 0) {
                        for (ParseObject object : objects) {
                            TimeMachine timeMachine = new TimeMachine(object);
                            if (timeMachine.title != null) {
                                timeMachines.add(timeMachine);
                            }
                        }
                    }
                    callback.gotTimeMachines(timeMachines);
                } else {
                    Log.e(Constants.TAG, "Error getting timeMachines: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    void getTimeMachineFirstImage(final boolean save, final MethodHelper.ImageCallback callback){
        if (firstImage != null) {
            callback.gotImage(firstImage);
        }else if (firstImageFile != null){
            firstImageFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null){
                        if (save) {
                            firstImage = data;
                        }
                        callback.gotImage(data);
                    } else {
                        Log.e(Constants.TAG, "Error getting first image: "+e.getMessage());
                        e.printStackTrace();
                        callback.gotImage(null);
                    }
                }
            });
        }else {
            callback.gotImage(null);
        }
    }


    void getTimeMachineSecondImage(final boolean save, final MethodHelper.ImageCallback callback){
        if (secondImage != null) {
            callback.gotImage(secondImage);
        }else if (secondImageFile != null){
            secondImageFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null){
                        if (save) {
                            secondImage = data;
                        }
                        callback.gotImage(data);
                    } else {
                        Log.e(Constants.TAG, "Error getting first image: "+e.getMessage());
                        e.printStackTrace();
                        callback.gotImage(null);
                    }
                }
            });
        }else {
            callback.gotImage(null);
        }
    }

    interface TimeMachineCallback {
        void gotTimeMachines(ArrayList<TimeMachine> timeMachines);
    }
}
