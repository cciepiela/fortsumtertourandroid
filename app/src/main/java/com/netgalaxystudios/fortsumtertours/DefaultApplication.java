package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.RemoteException;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.radiusnetworks.proximity.KitConfig;
import com.radiusnetworks.proximity.ProximityKitBeacon;
import com.radiusnetworks.proximity.ProximityKitBeaconRegion;
import com.radiusnetworks.proximity.ProximityKitGeofenceNotifier;
import com.radiusnetworks.proximity.ProximityKitGeofenceRegion;
import com.radiusnetworks.proximity.ProximityKitManager;
import com.radiusnetworks.proximity.ProximityKitMonitorNotifier;
import com.radiusnetworks.proximity.ProximityKitRangeNotifier;
import com.radiusnetworks.proximity.ProximityKitSyncNotifier;
import com.radiusnetworks.proximity.beacon.BeaconManager;
import com.radiusnetworks.proximity.geofence.GeofenceManager;
import com.radiusnetworks.proximity.geofence.GooglePlayServicesException;
import com.radiusnetworks.proximity.model.KitBeacon;
import com.radiusnetworks.proximity.model.KitOverlay;

import org.altbeacon.beacon.BeaconConsumer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;

/**
 * Created by coryciepiela on 3/1/17.
 */

public class DefaultApplication extends Application
        implements
        ProximityKitMonitorNotifier,
        ProximityKitRangeNotifier,
        ProximityKitSyncNotifier,
        ProximityKitGeofenceNotifier, BeaconConsumer {

    static Boat sBoat;
    static FloorPlanActivity.FloorPlanType sFloorPlanType;
    static FortSumter sFortSumter;
    static ArrayList<Beacon> beacons = new ArrayList<Beacon>();
    static HashMap<String, Date> beaconFireDates = new HashMap<String, Date>();
    static Tracker mTracker = null;
    private ProximityKitManager pkManager = null;
    @NonNull
    private static final Object SHARED_LOCK = new Object();
    private boolean haveDetectedBeaconsSinceBoot = false;
    private static final KitConfig KIT_CONFIG = new KitConfig(loadConfig());
    GeofencingClient geofencingClient;
    ArrayList<Geofence> mGeofenceList = new ArrayList<Geofence>();
    private PendingIntent mGeofencePendingIntent;
    public static DefaultApplication application;

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                .applicationId("1HhKdcMa9ziwRn7UFtVOG0V4lMT9PNoMpwj1Tt9R")
                .clientKey("a4jLHT0XMSiFw9DDtrMlahLjhfkmGqR2Ue51wR1X")
                .server("https://parseapi.back4app.com")
                .build()
        );
        MethodHelper.getCurrentLocation(getApplicationContext(), new MethodHelper.GetLocationCallback() {
            @Override
            public void gotLocation(Location location, ParseException e) {
                if (e != null) {
                    Log.e(Constants.TAG, "Error getting coordinates in DefaultApplication!: " + e.getMessage());
                    e.printStackTrace();
                } else if (location != null) {
                    Log.d(Constants.TAG, "Location is: " + location.getLongitude());
                }
            }
        });
        geofencingClient = LocationServices.getGeofencingClient(this);
        application = this;

        Config.getCurrentConfig();
        ParseFacebookUtils.initialize(getApplicationContext());
        FacebookSdk.sdkInitialize(getApplicationContext());
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("GCMSenderId", "<985055356751>");
        installation.saveInBackground();
//        initializePkManager();
    }

    static void initializeGeofences () {
        if (application != null) application.loadGeofences();
    }

    public void loadGeofences() {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Geofence.className);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (objects != null) {
                    for (ParseObject parseObject : objects) {
                        addGeofence(parseObject);
                    }
                    addGeofencesToClient();
                }
            }
        });
    }

    public void addGeofence(ParseObject parseObject) {
        String objectId = parseObject.getObjectId();
        ParseGeoPoint geoPoint = parseObject.getParseGeoPoint(Constants.Geofence.location);
        double radius = parseObject.getDouble(Constants.Geofence.radius);
        String title = parseObject.getString(Constants.Geofence.title);
        String message = parseObject.getString(Constants.Geofence.message);

        SharedPreferencesHelper.Companion.setTitle(objectId, title);
        SharedPreferencesHelper.Companion.setMessage(objectId, message);
        mGeofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this
                // geofence.
                .setRequestId(objectId)

                .setCircularRegion(
                        geoPoint.getLatitude(),
                        geoPoint.getLongitude(),
                        (float) radius
                )
                .setExpirationDuration(999999)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                .build());
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER | GeofencingRequest.INITIAL_TRIGGER_EXIT);
        Log.d(Constants.TAG, "getGeofencingRequest: " + mGeofenceList);

        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionIntenService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    private void addGeofencesToClient() {
        Log.d(Constants.TAG, "addGeofencesToClient1: " + mGeofenceList);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        Log.d(Constants.TAG, "addGeofencesToClient2");

        geofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Geofences added
                        // ...
                        Log.d(Constants.TAG, "GEOFENCES_ADDED: " + mGeofenceList);
                        Log.d(Constants.TAG, "addGeofencesToClient3");

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Failed to add geofences
                        Log.d(Constants.TAG, "addGeofencesToClient4");

                        // ...
                    }
                });
    }

    public void initializePkManager () {
        if (null == pkManager) {
            synchronized (SHARED_LOCK) {
                if (null == pkManager) {
                    pkManager = ProximityKitManager.getInstance(this, KIT_CONFIG);
                    pkManager.getBeaconManager().bind(new BeaconConsumer() {
                        @Override
                        public void onBeaconServiceConnect() {
//                            loadPkManager();

                        }

                        @Override
                        public Context getApplicationContext() {
                            return null;
                        }

                        @Override
                        public void unbindService(ServiceConnection serviceConnection) {

                        }

                        @Override
                        public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) {
                            return false;
                        }
                    });

                }
            }
        }
    }

    synchronized Tracker getDefaultTracker(Context context) {
//        if (mTracker == null) {
//            GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
//            Log.d(Constants.TAG, "in getDefaultTracker, GoogleAnalytics intialization status is: "+analytics.isInitialized());
//            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
//            mTracker = analytics.newTracker(R.xml.global_tracker);
//        }
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
        mTracker = analytics.newTracker(R.xml.global_tracker);
        Log.d(Constants.TAG, "mTracker was not null in DefaultApplication! It is: "+mTracker);
        return mTracker;
    }

    public void startManager() {

        if (pkManager != null) {
            pkManager.start();
        }
    }

    public void stopManager() {

        if (pkManager != null) {

            pkManager.stop();
        }
    }

    public void loadPkManager() {

        /* ----- begin code only for debugging ---- */

//        ProximityKitManager.debugOn();
        // pkManager.debugOn();

        /* ----- end code only for debugging ------ */

        if (servicesConnected()) {
            // As a safety mechanism, `enableGeofences()` throws a checked exception in case the
            // app does not properly handle Google Play support.
            try {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Log.d(Constants.TAG, "ENABLE_GEOFENCES");
                }
                pkManager.enableGeofences();

                /*
                 * No point setting the geofence notifier if we aren't using geofences.
                 *
                 * This should be set prior to calling `start()` on the manager. If the notifier
                 * is set after calling `start()`, it is possible some notifications will be missed
                 * during that window.
                 */
                pkManager.setProximityKitGeofenceNotifier(this);
            } catch (GooglePlayServicesException e) {
                Log.e(Constants.TAG, e.getMessage());
            }
        }

        /*
         * Set desired callbacks before calling `start()`.
         *
         * We can set these notifications after calling `start()`. However, this means we will miss
         * any notifications posted in the time between those actions.
         *
         * You are free to set only the notifiers you want callbacks for. We are setting all of them
         * to demonstrate how each set works.
         */
        pkManager.setProximityKitSyncNotifier(this);
        pkManager.setProximityKitMonitorNotifier(this);
        pkManager.setProximityKitRangeNotifier(this);
        BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.setBackgroundBetweenScanPeriod(25000L);
        beaconManager.setBackgroundScanPeriod(30000L);
        beaconManager.setForegroundScanPeriod(60000L);
        beaconManager.setForegroundBetweenScanPeriod(5000L);
        pkManager.start();
        try {
            beaconManager.updateScanPeriods();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    public static Map<String, Object> loadConfig() {
        Map<String, Object> settings = new HashMap<>();
        settings.put(
                KitConfig.CONFIG_API_URL,
                "https://proximitykit.radiusnetworks.com/api/kits/10377"
        );
        settings.put(
                KitConfig.CONFIG_API_TOKEN,
                "4b902c897e0525a7fd0fd4f7a0d2422c8feed0775f18eb43f3fca2c7d2f06830"
        );
        settings.put(KitConfig.ALLOW_UNSUPPORTED_GOOGLE_PLAY, true);
      //  settings.put(KitConfig.CONFIG_CELLULAR_DATA, true);
        return settings;
    }

    @Override
    public void didSync() {
        Log.i(Constants.TAG, "didSync(): Sycn'd with server");

        // Access every beacon configured in the kit, printing out the value of an attribute
        // named "myKey"
        for (KitBeacon beacon : pkManager.getKit().getBeacons()) {
            Log.d(
                    Constants.TAG,
                    "For beacon: " + beacon.getProximityUuid() + " " + beacon.getMajor() + " " +
                            beacon.getMinor() + ", the value of welcomeMessage is " +
                            beacon.getAttributes().get("welcomeMessage")
            );
        }

        // Access every geofence configured in the kit, printing out the value of an attribute
        // named "myKey"
        for (KitOverlay overlay : pkManager.getKit().getOverlays()) {

            Log.d(
                    Constants.TAG,
                    "For geofence: (" + overlay.getLatitude() + ", " + overlay.getLongitude() +
                            ") with radius " + overlay.getRadius() + ", the value of myKey is " +
                            overlay.getAttributes().get("myKey")
            );
        }
    }

    @Override
    public void didFailSync(@NonNull Exception e) {
        Log.e(Constants.TAG, "didFailSync() called with exception: " + e);
    }

    public boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (ConnectionResult.SUCCESS == resultCode) {
            Log.d(Constants.TAG, "Google Play services available");
            return true;
        }

        // Taking the easy way out: log it. Then let Google Play generate the appropriate action
        Log.w(Constants.TAG, GooglePlayServicesUtil.getErrorString(resultCode));
        PendingIntent nextAction = GooglePlayServicesUtil.getErrorPendingIntent(
                resultCode,
                this,
                0
        );

        // Make sure we have something to do
        if (nextAction == null) {
            Log.e(Constants.TAG, "Unable to determine action to handle Google Play Services error.");
        }

        // This isn't a crash worthy event
        try {
            nextAction.send(this, 0, new Intent());
        } catch (PendingIntent.CanceledException e) {
            Log.w(Constants.TAG, "Intent was canceled after we sent it.");
        } catch (NullPointerException npe) {
            // Likely on a mod without Google Play but log the exception to be safe
            Log.e(Constants.TAG, "Error occurred when trying to retrieve to Google Play Services.");
            npe.printStackTrace();
        }
        return false;
    }

    @Override
    public void didDetermineStateForGeofence(int i, @NonNull ProximityKitGeofenceRegion proximityKitGeofenceRegion) {
        Log.d(Constants.TAG, "didDeterineStateForGeofence called with region: " + proximityKitGeofenceRegion);

        switch (i) {
            case ProximityKitGeofenceNotifier.INSIDE:
                enterRegion(proximityKitGeofenceRegion);
                break;
            case ProximityKitGeofenceNotifier.OUTSIDE:
                break;
            default:
                Log.d(Constants.TAG, "Received unknown state: " + proximityKitGeofenceRegion);
                break;
        }


    }

    @Override
    public void didEnterGeofence(@NonNull ProximityKitGeofenceRegion proximityKitGeofenceRegion) {
        enterRegion(proximityKitGeofenceRegion);

    }

    private void enterRegion (@NonNull ProximityKitGeofenceRegion proximityKitGeofenceRegion) {
        Log.d(Constants.TAG, "didEnterGeofence:" + proximityKitGeofenceRegion.getName() + ".");
        if (shouldShowAlert(proximityKitGeofenceRegion)) {
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MY_SHARED_PREFERENCES", MODE_APPEND);
            SharedPreferences.Editor editor = sharedPreferences.edit();
//        if (sharedPreferences.getBoolean(proximityKitGeofenceRegion.getName(), false)) {
//            return;
//        }


            if (proximityKitGeofenceRegion.getAttributes().containsKey("timerInterval")) {
                int timerInterval = Integer.valueOf(proximityKitGeofenceRegion.getAttributes().get("timerInterval"));
                Log.d(Constants.TAG, "fetched timerInterval is: "+timerInterval);
                Calendar date = Calendar.getInstance();
                long t = date.getTimeInMillis();
                Date datePlusTimerInterval = new Date(t + (timerInterval * 1000));

                editor.putLong("timerInterval", datePlusTimerInterval.getTime());

                scheduleNotification(getNotification(Config.getCurrentConfig().boatDepartingTitleText, Config.getCurrentConfig().boatDepartingText), timerInterval);
                // NotificationPublisher.scheduleNotification(getApplicationContext(), datePlusTimerInterval, timerInterval, Config.getCurrentConfig().boatDepartingTitleText, Config.getCurrentConfig().boatDepartingText);
                sendMessage();

            }
            editor.putBoolean(proximityKitGeofenceRegion.getName(), true);
            editor.commit();
            if (!hasDepartureMessage(proximityKitGeofenceRegion)) {
                MethodHelper.fireLocalNotification(this, 536345, proximityKitGeofenceRegion.getAttributes().get("title"), proximityKitGeofenceRegion.getAttributes().get("body"), null);
            }else if (shouldShow(proximityKitGeofenceRegion)) {
                if (shouldShowDepartureMessage(proximityKitGeofenceRegion)) {
                    MethodHelper.fireLocalNotification(this, 536345, proximityKitGeofenceRegion.getAttributes().get("departureTitle"), proximityKitGeofenceRegion.getAttributes().get("departureBody"), null);
                }else {
                    setDepartureRegion(proximityKitGeofenceRegion);
                    MethodHelper.fireLocalNotification(this, 536345, proximityKitGeofenceRegion.getAttributes().get("title"), proximityKitGeofenceRegion.getAttributes().get("body"), null);
                }
            }
        }
    }

    private void setDepartureRegion (@NonNull ProximityKitGeofenceRegion proximityKitGeofenceRegion) {
        if (proximityKitGeofenceRegion.getAttributes().get("objectId") == null) {
            return;
        }
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MY_SHARED_PREFERENCES", MODE_APPEND);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("firedMessageObjectId", proximityKitGeofenceRegion.getAttributes().get("objectId"));
        editor.putLong("departureFiredDate", new Date().getTime());
        editor.commit();
    }

    private boolean shouldShowDepartureMessage (@NonNull ProximityKitGeofenceRegion proximityKitGeofenceRegion) {
        if (proximityKitGeofenceRegion.getAttributes().get("objectId") == null) {
            return true;
        }
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MY_SHARED_PREFERENCES", MODE_APPEND);
        if (sharedPreferences.getLong("departureFiredDate", -5) > 0 && sharedPreferences.getString("firedMessageObjectId", null) != null) {
            if (sharedPreferences.getString("firedMessageObjectId", null).equals(proximityKitGeofenceRegion.getAttributes().get("objectId"))
                    && new Date().getTime() - new Date(sharedPreferences.getLong("departureFiredDate", 0)).getTime() < 3600 * 1000 * 3) {


                return true;
            }else {
                return false;
            }
        }else {
            Log.d(Constants.TAG, "shouldShow3");
            return false;
        }
    }

    private boolean shouldShow (@NonNull ProximityKitGeofenceRegion proximityKitGeofenceRegion) {
        if (proximityKitGeofenceRegion.getAttributes().get("objectId") == null) {
            return true;
        }
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MY_SHARED_PREFERENCES", MODE_APPEND);
        if (sharedPreferences.getLong("departureFiredDate", -5) > 0 && sharedPreferences.getString("firedMessageObjectId", null) != null) {
            if (sharedPreferences.getString("firedMessageObjectId", null).equals(proximityKitGeofenceRegion.getAttributes().get("objectId"))
                    || new Date().getTime() - new Date(sharedPreferences.getLong("departureFiredDate", 0)).getTime() > 3600 * 1000 * 3 ) {
                return true;
            }else {
                return false;
            }
        }else {
            return true;
        }
    }

    private boolean hasDepartureMessage (@NonNull ProximityKitGeofenceRegion proximityKitGeofenceRegion) {
        boolean hasMessage = false;
        if (proximityKitGeofenceRegion.getAttributes().get("departureBody") != null && proximityKitGeofenceRegion.getAttributes().get("departureBody").length() > 0) {
            hasMessage = true;
        }
        if (proximityKitGeofenceRegion.getAttributes().get("departureSubtitle") != null && proximityKitGeofenceRegion.getAttributes().get("departureSubtitle").length() > 0) {
            hasMessage = true;
        }
        if (proximityKitGeofenceRegion.getAttributes().get("departureTitle") != null && proximityKitGeofenceRegion.getAttributes().get("departureTitle").length() > 0) {
            hasMessage = true;
        }
        return hasMessage;
    }



    private boolean shouldShowAlert (ProximityKitGeofenceRegion forGeofence) {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MY_SHARED_PREFERENCES", MODE_APPEND);
        if (sharedPreferences.getLong(forGeofence.getName() + "time", 0) > 0) {
            Date date = new Date(sharedPreferences.getLong(forGeofence.getName() + "time", 0));
            int sixHours = 6 * 60 * 60 * 1000; /* ms */
            if ((new Date()).getTime() - date.getTime() > 0) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong(forGeofence.getName() + "time",(new Date()).getTime());
                editor.commit();
                return true;
            }else {
                return false;
            }
        }else {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(forGeofence.getName() + "time",(new Date()).getTime());
            editor.commit();
            return true;
        }
    }

    private void sendMessage() {
        Log.d("sender", "Broadcasting message");
        Intent intent = new Intent(Constants.BroadcastEventNames.enteredGeofenceWithTimer);
        // You can also include some extra data.
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void scheduleNotification(Notification notification, int delayInSeconds) {
        Log.d(Constants.TAG, "SCHEDULE_NOTIFICATION: "+delayInSeconds);
        Intent notificationIntent = new Intent(this, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long futureInMillis = SystemClock.elapsedRealtime() + (delayInSeconds * 1000);
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    private Notification getNotification(String title, String content) {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle(title);
        builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        builder.setContentText(content);
        builder.setSmallIcon(R.drawable.ic_boat_logo_white);
        return builder.build();
    }

    @Override
    public void didExitGeofence(@NonNull ProximityKitGeofenceRegion proximityKitGeofenceRegion) {
        Log.d(Constants.TAG, "didExitGeofence:" + proximityKitGeofenceRegion);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MY_SHARED_PREFERENCES", MODE_APPEND);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(proximityKitGeofenceRegion.getName(), false);
        editor.commit();
    }

    @Override
    public void didDetermineStateForRegion(int state, @NonNull ProximityKitBeaconRegion region) {
        Log.d(Constants.TAG, "didDeterineStateForRegion called with state: " + state + "\tregion: " + region);

        switch (state) {
            case ProximityKitMonitorNotifier.INSIDE:
                String welcomeMessage = region.getAttributes().get("welcomeMessage");
                if (welcomeMessage != null) {
                    Log.d(Constants.TAG, "Beacon " + region + " says: " + welcomeMessage);
                }
                break;
            case ProximityKitMonitorNotifier.OUTSIDE:
                String goodbyeMessage = region.getAttributes().get("goodbyeMessage");
                if (goodbyeMessage != null) {
                    Log.d(Constants.TAG, "Beacon " + region + " says: " + goodbyeMessage);
                }
                break;
            default:
                Log.d(Constants.TAG, "Received unknown state: " + state);
                break;
        }
    }

    @Override
    public void didEnterRegion(@NonNull ProximityKitBeaconRegion region) {
        // In this example, this class sends a notification to the user whenever an beacon
        // matching a Region (defined above) are first seen.
//        Log.d(
//                Constants.TAG,
//                "[didEnterRegion] ENTER beacon region: " + region + " " +
//                        region.getAttributes().get("welcomeMessage")
//        );

        // Attempt to open the app now that we've entered a region if we started in the background

        // Notify the user that we've seen a beacon
    }


    @Override
    public void didExitRegion(@NonNull ProximityKitBeaconRegion region) {
        //Log.d(Constants.TAG, "[didExitRegion] EXIT beacon region: " + region);
    }

    @Override
    public void didRangeBeaconsInRegion(@NonNull Collection<ProximityKitBeacon> beacons,
                                        @NonNull ProximityKitBeaconRegion region) {
        if (beacons.size() == 0) {
            return;
        }
        ProximityKitBeacon nearestBeacon = (ProximityKitBeacon) beacons.toArray()[0];
        for (ProximityKitBeacon beacon : beacons) {
            if (beacon.getDistance() < nearestBeacon.getDistance()) {
                nearestBeacon = beacon;
            }
        }
        final ProximityKitBeacon closestBeacon = nearestBeacon;

        if (closestBeacon != null) {
            if (closestBeacon.getId2() == null) {
                return;
            }
            if (closestBeacon.getId3() == null) {
                return;
            }
        }
        Beacon.getBeacons(new Beacon.GetBeaconsCallback() {
            @Override
            public void gotBeacons(ArrayList<Beacon> beacons) {
                Beacon beaconObject = null;

                for (Beacon beacon : DefaultApplication.beacons) {
                    if (beacon.major == closestBeacon.getId2().toInt() && beacon.minor == closestBeacon.getId3().toInt()) {
                        beaconObject = beacon;
                    }
                }
                Log.d(Constants.TAG, "didRangeBeaconsInRegion1: " + beaconObject);

                if (beaconObject == null) {
                    return;
                }
                Log.d(Constants.TAG, "didRangeBeaconsInRegion2: " + beaconObject);

                if (beaconObject.title == null || beaconObject.details == null) {
                    return;
                }
                Log.d(Constants.TAG, "didRangeBeaconsInRegion3: " + beaconObject);

                Log.d(Constants.TAG, "DISTANCE_TO_BEACON: " + closestBeacon.getDistance() + " REQUIRED_DISTANCE: " + beaconObject.distance);
                if (closestBeacon.getDistance() <= beaconObject.distance && closestBeacon.getDistance() > 0 && beaconObject.shouldFire(getApplicationContext())) {
                    Log.d(Constants.TAG, "DISTANCE_TO_BEACON: " + closestBeacon.getDistance() + " REQUIRED_DISTANCE: " + beaconObject.distance);

                    MethodHelper.fireLocalNotification(getApplicationContext(), 4356, "Fort Sumter Tours", "This is the " +beaconObject.title + ". Tap here to learn more.", beaconObject.objectId);
                }
//                boolean shouldFireSecondary = true;
//                if (beaconFireDates.get("" + beaconObject.objectId) == null) {
//                    shouldFireSecondary = false;
//                    Date date = new Date();
//                    date.setTime(new Date().getTime() - (beaconObject.timeInterval * 2000));
//                    beaconFireDates.put("" + beaconObject.objectId, date);
//                }
//                long diffInMs = new Date().getTime() - beaconFireDates.get("" + beaconObject.objectId).getTime();
//                long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
//                Log.d(Constants.TAG, "CLOSEST_BEACON: " + closestBeacon.getDistance() + " - BeaconObjectDistance: " + beaconObject.distance + " - DiffInSec: " + diffInSec + " - TimeInterval: " + beaconObject.timeInterval);
//
//                if (closestBeacon.getDistance() < beaconObject.distance && closestBeacon.getDistance() > 0 && diffInSec > beaconObject.timeInterval) {
//                    beaconFireDates.put("" + beaconObject.objectId, new Date());
//                    if (beaconObject.hasSecondaryMessage() && shouldFireSecondary) {
//                        long ms = beaconFireDates.get(beaconObject.objectId).getTime() - new Date().getTime();
//                        long secs = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
//                        if (secs < beaconObject.secondaryMessageTimeFrame) {
//                            MethodHelper.fireLocalNotification(getApplicationContext(), 4356, beaconObject.title2, beaconObject.body2);
//                        } else {
//                            MethodHelper.fireLocalNotification(getApplicationContext(), 4356, beaconObject.title, beaconObject.body);
//                        }
//                    } else {
//                        MethodHelper.fireLocalNotification(getApplicationContext(), 4356, beaconObject.title, beaconObject.body);
//                    }
//
//
//                }
            }
        });

    }

    @Override
    public void onBeaconServiceConnect() {
        //loadPkManager();
    }
}
