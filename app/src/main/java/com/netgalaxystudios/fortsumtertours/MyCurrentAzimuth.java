package com.netgalaxystudios.fortsumtertours;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

/**
 * Created by coryciepiela on 4/11/17.
 */

class MyCurrentAzimuth implements SensorEventListener {

    private SensorManager sensorManager;
    private int azimuthTo = 0;
    private OnAzimuthChangedListener mAzimuthListener;
    private Context mContext;

    MyCurrentAzimuth(OnAzimuthChangedListener azimuthListener, Context context) {
        mAzimuthListener = azimuthListener;
        mContext = context;
    }

    void start() {
        Sensor sensor;
        sensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        sensorManager.registerListener(this, sensor,
                SensorManager.SENSOR_DELAY_UI);
    }

    void stop() {
        sensorManager.unregisterListener(this);
    }

    public void setOnShakeListener(OnAzimuthChangedListener listener) {
        mAzimuthListener = listener;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int azimuthFrom;
        azimuthFrom = azimuthTo;

        float[] orientation = new float[3];
        float[] rMat = new float[9];
//        Log.d(Constants.TAG, "Low pass filter has orientation first array index as: "+orientation[0]+" second index: "+orientation[1]+ " third index: "+orientation[2]);
        SensorManager.getRotationMatrixFromVector(rMat, event.values);
        azimuthTo = (int) (Math.toDegrees(SensorManager.getOrientation(rMat, orientation)[0]) + 360) % 360;
//        Log.d(Constants.TAG, "azimuthTo is: " + azimuthTo + " getOrientation returns: " + Math.toDegrees(SensorManager.getOrientation(rMat, orientation)[0]) + " full getOrientation (with + 360) returns: " +
//                Math.toDegrees(SensorManager.getOrientation(rMat, orientation)[0]) + 360 + "getOrientation (with + 360 % 360) returns: " + (Math.toDegrees(SensorManager.getOrientation(rMat, orientation)[0]) + 360) % 360);



        mAzimuthListener.onAzimuthChanged(azimuthFrom, azimuthTo);
    }

    private float[] lowPass(float[] input) {
        float[] output = new float[input.length];
        final float ALPHA = 0.1f;
        for (int i = 0; i < input.length; i++) {
            Log.d(Constants.TAG, "i is: "+i+" input[i] is: "+input[i]+" output[i] is: "+output[i]+" (input[i] - output[i] is: "+(input[i] - output[i]));
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
            Log.d(Constants.TAG, "output[i] after calculation is: "+output[i]);
        }
        return output;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    interface OnAzimuthChangedListener {
        void onAzimuthChanged(float azimuthFrom, float azimuthTo);
    }
}
