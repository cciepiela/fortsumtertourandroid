package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class PoIDetailsActivity extends AppCompatActivity {

    static PoI mPoI;

    private ImageView mBackButton;
    private TextView mHeaderTitleText;
    private ImageView mHeaderImage;
    private TextView mPoiTitleText;
    private TextView mPoiDescriptionText;
    private TextView mPoiMoreInfoButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi_details);

        connectUi();
    }

    private void connectUi() {
        mBackButton = (ImageView)findViewById(R.id.back_button);
        mHeaderTitleText = (TextView)findViewById(R.id.header_title_text);
        mHeaderImage = (ImageView)findViewById(R.id.header_picture);
        mPoiTitleText = (TextView)findViewById(R.id.poi_title_text);
        mPoiDescriptionText = (TextView)findViewById(R.id.poi_description_text);
        mPoiMoreInfoButton = (TextView)findViewById(R.id.poi_more_info_button);
        setListeners();
        populateUi();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mPoiMoreInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebActivity.displayWebActivity(PoIDetailsActivity.this, mPoI.url, mPoI.title);
            }
        });
    }

    private void populateUi(){
        mHeaderTitleText.setText(mPoI.title);
        mPoI.getPoiPicture(new History.ImageCallback() {
            @Override
            public void gotImage(byte[] image) {
                if (image != null){
                    Glide.with(PoIDetailsActivity.this)
                            .load(image)
                            .override(300, 300)
                            .centerCrop()
                            .into(mHeaderImage);
                }
            }
        });
        mPoiTitleText.setText(mPoI.title);
        mPoiDescriptionText.setText(mPoI.bodyText);
    }

    static void display(PoI poI, Activity activity){
        mPoI = poI;
        Intent i = new Intent(activity, PoIDetailsActivity.class);
        activity.startActivity(i);
    }
}
