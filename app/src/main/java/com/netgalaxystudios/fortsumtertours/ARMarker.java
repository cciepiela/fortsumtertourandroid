package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * Created by charlesvonlehe on 5/26/17.
 */

public class ARMarker extends RelativeLayout {
    public boolean animating = false;
    private PoI poI;
    private ImageView imageView;
    private TextView titleTextView;
    private TextView descriptionTextView;
    private TextView readMoreTextView;
    private ARMarkerListener listener;

    public ARMarker(Context context) {
        super(context);
        conntectUI();
    }

    public ARMarker(Context context, AttributeSet attrs) {
        super(context, attrs);
        conntectUI();
    }

    public ARMarker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        conntectUI();
    }

    public void populate(PoI forPoi, final Activity context, ARMarkerListener listener) {
        conntectUI();
        this.listener = listener;
        poI = forPoi;
        forPoi.getPoiPicture(new History.ImageCallback() {
            @Override
            public void gotImage(byte[] image) {
                if (image != null && !context.isDestroyed()) {
                    Glide.with(context).load(image).into(imageView);
                }
            }
        });
        titleTextView.setText(forPoi.title);
        descriptionTextView.setText(forPoi.bodyText);
        readMoreTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ARMarker.this.listener != null) {
                    ARMarker.this.listener.clickedReadMore(poI);
                }
            }
        });
    }

    private void conntectUI () {
        imageView = (ImageView) findViewById(R.id.poi_image);

        titleTextView = (TextView) findViewById(R.id.poi_title);
        descriptionTextView = (TextView) findViewById(R.id.poi_description);
        readMoreTextView = (TextView) findViewById(R.id.poi_read_more);

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

    }



    interface ARMarkerListener {
        void clickedReadMore(PoI forPoi);
    }
}
