package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import uk.co.senab.photoview.PhotoViewAttacher;

public class FloorPlanActivity extends AppCompatActivity {

    private ImageView mHeaderPicture;
    private ImageView mFloorPlanImage;
    private ImageView mBackButton;
    private FloorPlanType mFloorPlanType;
    private TextView mLvlOneButton;
    private TextView mLvlTwoButton;
    private TextView mLvlThreeButton;
    private Boat mBoat;
    private TextView mHeaderTitle;
    private byte[] mFloorPlanImageBytes;
    private PhotoViewAttacher mAttacher;
    private FortSumter mFortSumter;
    private int mTopMargin;
    private Bitmap mBitmap;
    private static int mOrientation;
    private TextView mLegendSlider;
    private ImageView mLegendPicture;
    private boolean mLegendHidden = false;
    private LinearLayout mLegendLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floor_plan);

        if (DefaultApplication.sFloorPlanType != null) {
            mFloorPlanType = DefaultApplication.sFloorPlanType;
        }
        if (DefaultApplication.sBoat != null) {
            mBoat = DefaultApplication.sBoat;
        }
        if (DefaultApplication.sFortSumter != null) {
            mFortSumter = DefaultApplication.sFortSumter;
        }
        connectUi();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mOrientation = newConfig.orientation;
            if (mBoat != null) {
                rotateImage(newConfig.orientation);
            }
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mOrientation = newConfig.orientation;
            if (mBoat != null) {
                rotateImage(newConfig.orientation);
            }
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.d(Constants.TAG, "There was lowMemory!");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void connectUi() {
        mHeaderPicture = (ImageView) findViewById(R.id.header_picture);
        mFloorPlanImage = (ImageView) findViewById(R.id.floor_plan_image_view);
        mLegendLayout = (LinearLayout)findViewById(R.id.legend_layout);
        mAttacher = new PhotoViewAttacher(mFloorPlanImage);
        mAttacher.setMaxScale(4f);
        mAttacher.setMidScale(2f);
        mAttacher.setMinScale(1f);
        mBackButton = (ImageView) findViewById(R.id.back_button);
        mLvlOneButton = (TextView) findViewById(R.id.level_one_button);
        mLvlTwoButton = (TextView) findViewById(R.id.level_two_button);
        mLvlThreeButton = (TextView) findViewById(R.id.level_three_button);
        mHeaderTitle = (TextView) findViewById(R.id.header_title_text);
        mLegendSlider = (TextView) findViewById(R.id.legend_slide_button);
        mTopMargin = (int) MethodHelper.convertDpToPixel(130, FloorPlanActivity.this);
        mLegendPicture = (ImageView) findViewById(R.id.legend_picture);
        MethodHelper.setTextViewTypeFaceRegular(mHeaderTitle, FloorPlanActivity.this);
        if (mFloorPlanType != FloorPlanType.AMENITIES) {
            mLegendPicture.setVisibility(View.GONE);
            mLegendSlider.setVisibility(View.GONE);
        } else {
            if (mBoat != null){
                Glide.with(FloorPlanActivity.this)
                        .load(R.drawable.legend_boat)
                        .into(mLegendPicture);
            } else {
                Glide.with(FloorPlanActivity.this)
                        .load(R.drawable.legend_fort)
                        .into(mLegendPicture);
            }
        }
        setListeners();
        Log.d(Constants.TAG, "mBoat is: " + mBoat + ". And mFortSumter is: " + mFortSumter);
        if (mBoat != null) {
            populateUi(mBoat, null);
        }
        if (mFortSumter != null) {
            populateUi(null, mFortSumter);
        }

    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAttacher.cleanup();
                onBackPressed();
            }
        });
        mLvlOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configureLevelToolbar(LevelButtonSelected.ONE);
                if (mFloorPlanType == FloorPlanType.AMENITIES) {
                    getImageForLevel(1, true, true);
                } else {
                    getImageForLevel(1, false, true);
                }

            }
        });
        mLvlTwoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configureLevelToolbar(LevelButtonSelected.TWO);
                if (mFloorPlanType == FloorPlanType.AMENITIES) {
                    getImageForLevel(2, true, true);
                } else {
                    getImageForLevel(2, false, true);
                }
            }
        });
        mLvlThreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configureLevelToolbar(LevelButtonSelected.THREE);
                if (mFloorPlanType == FloorPlanType.AMENITIES) {
                    getImageForLevel(3, true, true);
                } else {
                    getImageForLevel(3, false, true);
                }
            }
        });
        mLegendSlider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(Constants.TAG, "mLegendSlider was clicked and mLegendHiddin boolean is: "+mLegendHidden);
                if (mLegendHidden) {
                    animateLegend(true);
                    mLegendHidden = false;
                } else {
                    animateLegend(false);
                    mLegendHidden = true;
                }
            }
        });
    }

    private void animateLegend(boolean legendHidden) {
        if (!legendHidden) {
                            TranslateAnimation hideViewAnimation = new TranslateAnimation(0, 300, 0, 0);
//            TranslateAnimation hideViewAnimation = new TranslateAnimation(Animation.ABSOLUTE, 0, Animation.RELATIVE_TO_PARENT, 1.0f, Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0);
            hideViewAnimation.setDuration(500);
            hideViewAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mLegendPicture.setVisibility(View.GONE);
                    mLegendSlider.setText(R.string.less_than);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            mLegendLayout.startAnimation(hideViewAnimation);
        } else {
                TranslateAnimation showViewAnimation = new TranslateAnimation(300, 0, 0, 0);
//            TranslateAnimation showViewAnimation = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 1.0f, Animation.RELATIVE_TO_PARENT, 0, Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0);
            showViewAnimation.setDuration(500);
            showViewAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    mLegendPicture.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mLegendSlider.setText(R.string.greater_than);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            mLegendLayout.startAnimation(showViewAnimation);
        }
    }


    private void populateUi(Boat boat, FortSumter fortSumter) {
        if (boat != null) {
            mFortSumter = null;
            Log.d(Constants.TAG, "Populating for a boat!");
            mHeaderTitle.setText(boat.mTitle);
            boat.getBoatImage(new Boat.BoatImageCallback() {
                @Override
                public void gotBoatImage(byte[] imageData) {
                    if (imageData != null) {
                        Glide.with(FloorPlanActivity.this)
                                .load(imageData)
                                .override(300, 300)
                                .into(mHeaderPicture);
                    } else {
                        Log.d(Constants.TAG, "getting boat image returned null!");
                    }
                }
            });
            if (mFloorPlanType == FloorPlanType.AMENITIES) {
                getImageForLevel(1, true, true);
            } else {
                getImageForLevel(1, false, true);
            }
            configureLevelToolbar(LevelButtonSelected.ONE);
        }
        if (fortSumter != null) {
            mBoat = null;
            Log.d(Constants.TAG, "Populating for Fort Sumter!");
            mHeaderTitle.setText(fortSumter.getTitle());
            fortSumter.getFortSumterImage(new FortSumter.GotFortSumterImageCallback() {
                @Override
                public void gotFortSumterImage(byte[] fortSumterImage) {
                    if (fortSumterImage != null) {
                        Glide.with(FloorPlanActivity.this)
                                .load(fortSumterImage)
                                .centerCrop()
                                .override(300, 300)
                                .into(mHeaderPicture);
                    }
                }
            });
            if (mFloorPlanType == FloorPlanType.AMENITIES) {
                getImageForLevel(1, true, false);
            } else {
                getImageForLevel(1, false, false);
            }
            configureLevelToolbar(LevelButtonSelected.ONE);
        }

    }

    private void rotateImage(int orientation) {
        Bitmap bmp;
        Matrix matrix = new Matrix();
        switch (orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                matrix.postRotate(90);
                bmp = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
                mFloorPlanImage.setImageBitmap(bmp);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                bmp = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
                mFloorPlanImage.setImageBitmap(bmp);
                break;
        }

    }

    private void getImageForLevel(int level, boolean forAmenities, boolean forBoat) {
        Log.d(Constants.TAG, "mOrientation at the start of getImageForLevel is: " + mOrientation);
        if (mOrientation > -1) {
            //Orientation is known.
            if (forBoat) {
                if (forAmenities) {
                    if (mFloorPlanImageBytes == null) {
                        mBoat.getFloorPlanImage(true, level, new Boat.ImageCallback() {
                            @Override
                            public void gotImage(byte[] image) {
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inJustDecodeBounds = true;
                                BitmapFactory.decodeByteArray(image, 0, image.length, options);
                                int imageHeight = options.outHeight;
                                int imageWidth = options.outWidth;
                                String imageType = options.outMimeType;
//                                options.inSampleSize = MethodHelper.calculateInSampleSize(options, mFloorPlanImage.getWidth(), mFloorPlanImage.getHeight());
                                options.inJustDecodeBounds = false;
                                mBitmap = BitmapFactory.decodeByteArray(image, 0, image.length, options);

                                mFloorPlanImageBytes = image.clone();

                                mFloorPlanImage.setImageBitmap(mBitmap);
                                rotateImage(mOrientation);
                                mAttacher.update();
                            }
                        });
                    } else {
                        Log.d(Constants.TAG, "mFloorPlanImageBytes were not null! They are: " + mFloorPlanImageBytes);
                        mBitmap = BitmapFactory.decodeByteArray(mFloorPlanImageBytes, 0, mFloorPlanImageBytes.length);
                        mFloorPlanImage.setImageBitmap(mBitmap);
                        rotateImage(mOrientation);
                        mAttacher.update();
                    }
                } else {
                    if (mFloorPlanImageBytes == null) {
                        mBoat.getFloorPlanImage(false, level, new Boat.ImageCallback() {
                            @Override
                            public void gotImage(byte[] image) {
                                mFloorPlanImageBytes = image;
                                mBitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                                mFloorPlanImage.setImageBitmap(mBitmap);
                                rotateImage(mOrientation);
                                mAttacher.update();
                            }
                        });
                    } else {
                        Log.d(Constants.TAG, "mFloorPlanImageBytes were not null! They are: " + mFloorPlanImageBytes);
                        mBitmap = BitmapFactory.decodeByteArray(mFloorPlanImageBytes, 0, mFloorPlanImageBytes.length);
                        mFloorPlanImage.setImageBitmap(mBitmap);
                        rotateImage(mOrientation);
                        mAttacher.update();
                    }
                }
            } else {
                if (forAmenities) {
                    if (mFloorPlanImageBytes == null) {
                        mFortSumter.getFloorPlanImage(true, new FortSumter.ImageCallback() {
                            @Override
                            public void gotImage(byte[] image) {
                                mFloorPlanImageBytes = image;
                                mBitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                                mFloorPlanImage.setImageBitmap(mBitmap);
                                mAttacher.update();
                            }
                        });
                    } else {
                        Log.d(Constants.TAG, "mFloorPlanImageBytes were not null! They are: " + mFloorPlanImageBytes);
                        mBitmap = BitmapFactory.decodeByteArray(mFloorPlanImageBytes, 0, mFloorPlanImageBytes.length);
                        mFloorPlanImage.setImageBitmap(mBitmap);
                        mAttacher.update();
                    }

                } else {
                    if (mFloorPlanImageBytes == null) {
                        mFortSumter.getFloorPlanImage(false, new FortSumter.ImageCallback() {
                            @Override
                            public void gotImage(byte[] image) {
                                mFloorPlanImageBytes = image;
                                mBitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                                mFloorPlanImage.setImageBitmap(mBitmap);
                                mAttacher.update();
                            }
                        });
                    } else {
                        Log.d(Constants.TAG, "mFloorPlanImageBytes were not null! They are: " + mFloorPlanImageBytes);
                        mBitmap = BitmapFactory.decodeByteArray(mFloorPlanImageBytes, 0, mFloorPlanImageBytes.length);
                        mFloorPlanImage.setImageBitmap(mBitmap);
                        mAttacher.update();
                    }
                }
            }
        } else {
            //Orientation is NOT known.
            if (forBoat) {
                if (forAmenities) {
                    if (mFloorPlanImageBytes == null) {
                        mBoat.getFloorPlanImage(true, level, new Boat.ImageCallback() {
                            @Override
                            public void gotImage(byte[] image) {
                                mFloorPlanImageBytes = image;
                                mBitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                                mFloorPlanImage.setImageBitmap(mBitmap);
                                mAttacher.update();
                            }
                        });
                    } else {
                        Log.d(Constants.TAG, "mFloorPlanImageBytes were not null! They are: " + mFloorPlanImageBytes);
                        mBitmap = BitmapFactory.decodeByteArray(mFloorPlanImageBytes, 0, mFloorPlanImageBytes.length);
                        mFloorPlanImage.setImageBitmap(mBitmap);
                        mAttacher.update();
                    }
                } else {
                    if (mFloorPlanImageBytes == null) {
                        mBoat.getFloorPlanImage(false, level, new Boat.ImageCallback() {
                            @Override
                            public void gotImage(byte[] image) {
                                mFloorPlanImageBytes = image;
                                mBitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                                mFloorPlanImage.setImageBitmap(mBitmap);
                                mAttacher.update();
                            }
                        });
                    } else {
                        Log.d(Constants.TAG, "mFloorPlanImageBytes were not null! They are: " + mFloorPlanImageBytes);
                        mBitmap = BitmapFactory.decodeByteArray(mFloorPlanImageBytes, 0, mFloorPlanImageBytes.length);
                        mFloorPlanImage.setImageBitmap(mBitmap);
                        mAttacher.update();
                    }
                }
            } else {
                if (forAmenities) {
                    if (mFloorPlanImageBytes == null) {
                        mFortSumter.getFloorPlanImage(true, new FortSumter.ImageCallback() {
                            @Override
                            public void gotImage(byte[] image) {
                                mFloorPlanImageBytes = image;
                                mBitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                                mFloorPlanImage.setImageBitmap(mBitmap);
                                mAttacher.update();
                            }
                        });
                    } else {
                        Log.d(Constants.TAG, "mFloorPlanImageBytes were not null! They are: " + mFloorPlanImageBytes);
                        mBitmap = BitmapFactory.decodeByteArray(mFloorPlanImageBytes, 0, mFloorPlanImageBytes.length);
                        mFloorPlanImage.setImageBitmap(mBitmap);
                        mAttacher.update();
                    }

                } else {
                    if (mFloorPlanImageBytes == null) {
                        mFortSumter.getFloorPlanImage(false, new FortSumter.ImageCallback() {
                            @Override
                            public void gotImage(byte[] image) {
                                mFloorPlanImageBytes = image;
                                mBitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                                mFloorPlanImage.setImageBitmap(mBitmap);
                                mAttacher.update();
                            }
                        });
                    } else {
                        Log.d(Constants.TAG, "mFloorPlanImageBytes were not null! They are: " + mFloorPlanImageBytes);
                        mBitmap = BitmapFactory.decodeByteArray(mFloorPlanImageBytes, 0, mFloorPlanImageBytes.length);
                        mFloorPlanImage.setImageBitmap(mBitmap);
                        mAttacher.update();
                    }
                }
            }
        }
    }

    private void configureLevelToolbar(LevelButtonSelected buttonSelected) {
        mFloorPlanImageBytes = null;
        if (mBoat == null) {
            mLvlOneButton.setVisibility(View.GONE);
            mLvlTwoButton.setVisibility(View.GONE);
            mLvlThreeButton.setVisibility(View.GONE);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            lp.setMargins(0, mTopMargin, 0, 0);
            mFloorPlanImage.setLayoutParams(lp);
        } else {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            int botMargin = (int) MethodHelper.convertDpToPixel(50, FloorPlanActivity.this);
            lp.setMargins(0, mTopMargin, 0, botMargin);
            mFloorPlanImage.setLayoutParams(lp);
            switch (buttonSelected) {
                case ONE:
                    mLvlOneButton.setAlpha(1);
                    mLvlTwoButton.setAlpha(0.5f);
                    mLvlThreeButton.setAlpha(0.5f);
                    break;
                case TWO:
                    mLvlTwoButton.setAlpha(1);
                    mLvlOneButton.setAlpha(0.5f);
                    mLvlThreeButton.setAlpha(0.5f);
                    break;
                case THREE:
                    mLvlThreeButton.setAlpha(1);
                    mLvlOneButton.setAlpha(0.5f);
                    mLvlTwoButton.setAlpha(0.5f);
                    break;
            }
            switch (mBoat.mLevels) {
                case 1:
                    mLvlTwoButton.setVisibility(View.GONE);
                    mLvlThreeButton.setVisibility(View.GONE);
                    mLvlOneButton.setLayoutParams(new TableLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 30));
                    break;
                case 2:
                    mLvlThreeButton.setVisibility(View.GONE);
                    mLvlOneButton.setLayoutParams(new TableLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 15));
                    mLvlTwoButton.setLayoutParams(new TableLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 15));
                    break;
                case 3:
                    mLvlOneButton.setLayoutParams(new TableLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10));
                    mLvlTwoButton.setLayoutParams(new TableLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10));
                    mLvlThreeButton.setLayoutParams(new TableLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10));
                    break;
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBoat = null;
        mAttacher.cleanup();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAttacher.cleanup();
    }

    static void displayFloorplanActivity(Activity activity, FloorPlanType planType, int orientation) {
        mOrientation = orientation;
        DefaultApplication.sFloorPlanType = planType;
        Intent i = new Intent(activity, FloorPlanActivity.class);
        activity.startActivity(i);
    }

    private enum LevelButtonSelected {
        ONE, TWO, THREE
    }

    enum FloorPlanType {
        AMENITIES, BEACONS
    }
}
