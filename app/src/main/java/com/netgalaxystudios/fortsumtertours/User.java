package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.parse.GetCallback;
import com.parse.GetFileCallback;
import com.parse.LogInCallback;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by coryciepiela on 3/6/17.
 */

class User {
    private static User currentUser;

    private String objectId = "";
    String username = "";
    String email = "";
    private int accessLevel = 0;
    String firstName;
    String lastName;
    private ParseFile profilePictureFile;
    private File profilePicture;
    AccountType accountType;
    private static boolean newUser = true;
    private static boolean loggedOut = false;
    private GoogleApiClient mGoogleApiClient;

    private User(ParseUser user) {
        super();
        if (user.getObjectId() != null) {
            objectId = user.getObjectId();
        }
        if (user.getUsername() != null) {
            username = user.getUsername();
        }
        if (user.getEmail() != null) {
            email = user.getEmail();
        }
        if (user.getString(Constants.User.firstName) != null) {
            firstName = user.getString(Constants.User.firstName);
        }
        if (user.getString(Constants.User.lastName) != null) {
            lastName = user.getString(Constants.User.lastName);
        }
        if (user.getInt(Constants.User.accessLevel) != 0) {
            accessLevel = user.getInt(Constants.User.accessLevel);
        }
        if (user.getParseFile(Constants.User.profilePicture) != null) {
            profilePictureFile = user.getParseFile(Constants.User.profilePicture);
        }
        if (user.getString(Constants.User.accountType) != null) {
            String accountTypeString = user.getString(Constants.User.accountType);
            switch (accountTypeString) {
                case "Facebook":
                    accountType = AccountType.Facebook;
                    break;
                case "Google":
                    accountType = AccountType.Google;
                    break;
                case "Parse":
                    accountType = AccountType.Parse;
                    break;
            }
        }
    }

    String getUserFullName() {
        return firstName + " " + lastName;
    }

    static User getCurrentUser() {
        if (ParseUser.getCurrentUser() == null) {
            currentUser = null;
        }
        if (currentUser != null) {
            ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    currentUser = null;
                }
            });
            return currentUser;
        }
        currentUser = new User(ParseUser.getCurrentUser());
        return currentUser;
    }

    void getUserProfilePictureFile(final ProfilePictureCallback callback) {
        if (profilePicture != null) {
            callback.gotProfilePictureFile(profilePicture);
        } else if (profilePictureFile != null) {
            profilePictureFile.getFileInBackground(new GetFileCallback() {
                @Override
                public void done(File file, ParseException e) {
                    if (e == null) {
                        profilePicture = file;
                        callback.gotProfilePictureFile(file);
                    } else {
                        Log.e(Constants.TAG, "Error getting Profile Picture file: " + e.getMessage());
                        e.printStackTrace();
                        callback.gotProfilePictureFile(null);
                    }
                }
            });
        } else {
            callback.gotProfilePictureFile(null);
        }
    }

    interface ProfilePictureCallback {
        void gotProfilePictureFile(File pictureFile);
    }

    static boolean userExists() {
        if (ParseUser.getCurrentUser() == null) {
            return false;
        } else if (ParseUser.getCurrentUser().getUsername() == null) {
            return false;
        } else {
            return true;
        }
    }

    static void loginUser(final String username, final String password, final LoginUserCallback callback) {
        User.getUser(username, new GetUserCallback() {
            @Override
            public void gotUser(User user) {
                if (user != null) {
                    ParseUser.logInInBackground(username.replace(" ", "").toLowerCase(), password.replace(" ", "").toLowerCase(), new LogInCallback() {
                        @Override
                        public void done(ParseUser user, ParseException e) {
                            callback.loggedInWithParseException(e == null, false, e);
                        }
                    });
                }else {
                    User.registerUser(username, password, username + "@gmail.com", null, null, null, new RegisterUserCallBack() {
                        @Override
                        public void registeredUser(boolean success, ParseException e) {
                            callback.loggedInWithParseException(success, true, e);
                        }
                    });
                }
            }
        });
    }

    static void getUser (String forUsername, final GetUserCallback callback) {
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo(Constants.User.username, forUsername.replace(" ", "").toLowerCase());
        query.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser object, ParseException e) {
                if (object != null) {
                    callback.gotUser(new User(object));
                }else {
                    callback.gotUser(null);
                }
            }
        });
    }

    private void getGoogleApiClient(Activity activity) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("985055356751-1lt8sj8oq2ksuln65nr0fhqmokq5mgh5.apps.googleusercontent.com")
                .requestEmail().build();
        // .addApi(Plus.API, null)
// .addScope(Plus.SCOPE_PLUS_LOGIN)
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .enableAutoManage(new FragmentActivity(), null)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                // .addApi(Plus.API, null)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {

                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                // .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
    }

    void logout(final Activity activity) {
        MethodHelper.showProgressHUDWithMessage("Logging out...", activity);

        ParseUser.logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    setLoggedInSharedPreferences(false, activity);
                    loggedOut = true;
                    Log.d(Constants.TAG, "No exception when logging the user out!");
                    User.unsetCurrentUser();
                    MethodHelper.hideHUD();
                    MethodHelper.switchToActivity(activity, LoginActivity.class);
                } else {
                    MethodHelper.hideHUD();
                    MethodHelper.showAlert(activity, "Error", "There was an error logging out: " + e.getMessage());
                    Log.e(Constants.TAG, "Error logging ParseUser out: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private static void completeLogin(String username, String password, final LoginUserCallback callback) {
        ParseUser.logOut();
        ParseUser.logInInBackground(username, password, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                boolean isNew = false;
                if (user != null) {
                    User.unsetCurrentUser();
                } else {
                    isNew = true;
                }

                callback.loggedInWithParseException(e == null, isNew, e);
            }
        });
    }


    interface LoginUserCallback {
        void loggedInWithParseException(boolean success, boolean isNew, ParseException e);
    }

    static void unsetCurrentUser() {
        currentUser = null;
    }

    static void storeCurrentUserObjectId(Context context, boolean erase) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.SharedPreferencesKeys.sharedPreferencesKey, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        if (erase) {
            editor.putString(Constants.SharedPreferencesKeys.currentUserObjectId, null);
        } else {
            if (ParseUser.getCurrentUser() != null) {
                editor.putString(Constants.SharedPreferencesKeys.currentUserObjectId, ParseUser.getCurrentUser().getObjectId());
            }
        }
        editor.apply();

    }

    static void loginWithFacebook(final android.app.Activity activity, final LoginUserCallback callback) {
        if (ParseUser.getCurrentUser() != null) {
            ParseUser.logOutInBackground(new LogOutCallback() {
                @Override
                public void done(ParseException e) {
                    completeLoginWithFacebook(activity, callback);
                }
            });
        } else {
            completeLoginWithFacebook(activity, callback);
        }
    }

    static void registerNewUser(final Activity activity, String username, String email, String password, String firstName, String lastName) {
        ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(password);
        user.put(Constants.User.firstName, firstName);
        user.put(Constants.User.lastName, lastName);
        user.put(Constants.User.newUser, true);
        user.put(Constants.User.accountType, "Parse");
        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                MethodHelper.hideHUD();
                if (e == null) {
                    setLoggedInSharedPreferences(true, activity);
                    Log.d(Constants.TAG, "ParseUser registered!");
                    MethodHelper.showAlert(activity, "Success", "Registration successful!");
                    MethodHelper.switchToActivity(activity, MainActivity.class);
                } else {
                    Log.d(Constants.TAG, "ERROR registering user: " + e.getMessage());
                    if (e.getMessage().length() > 0) {
                        MethodHelper.showAlert(activity, "Error", e.getMessage());
                    } else {
                        MethodHelper.showAlert(activity, "Error", "There was an error registering your account. Please try again.");
                    }
                    e.printStackTrace();
                }
            }
        });
    }

    private static void setLoggedInSharedPreferences (boolean loggedIn, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.FortSumter.className, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("LoggedIn", loggedIn);
        editor.commit();
    }

    static void loginWithGoogle(final Context context, GoogleSignInResult result, final MethodHelper.SuccessCallback callback) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String username = acct.getDisplayName();
            final String password = acct.getId();
            final String email = acct.getEmail();
            String firstName = "";
            if (acct.getGivenName() != null) {
                firstName = acct.getGivenName();
            }
            String lastName = "";
            if (acct.getFamilyName() != null) {
                lastName = acct.getFamilyName();
            }
            final Uri photoUrl = acct.getPhotoUrl();
            Log.d(Constants.TAG, "loginWithGoogle: " + username + " password: " + password + " email:" + email + " first:" + firstName + " last:" + lastName + "photo: " + photoUrl);
            if (username == null || password == null || email == null) {
                callback.processCompleted(false);
            } else {
                username = username.replace(" ", "");
                final String finalUsername = username;
                final String finalFirstName = firstName;
                final String finalLastName = lastName;
                ParseUser.logOut();
                getUserForUsername(username, new GetUserCallback() {
                    @Override
                    public void gotUser(User user) {
                        if (user == null) {
                            User.registerUser(finalUsername, password, email, finalFirstName, finalLastName, null, new RegisterUserCallBack() {
                                @Override
                                public void registeredUser(boolean success, ParseException e) {
                                    if (success) {
                                        if (photoUrl != null) {
                                            String url = photoUrl.toString();
                                            new ImageDownloader(url, new ImageDownloader.ImageDownloaderCallback() {
                                                @Override
                                                public void downloadedImage(Bitmap bitmap) {
                                                    if (bitmap != null && ParseUser.getCurrentUser() != null) {
                                                        ParseFile file = MethodHelper.convertBitmapToParseFile(bitmap);
                                                        ParseUser.getCurrentUser().put(Constants.User.profilePicture, file);
                                                        ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                                            @Override
                                                            public void done(ParseException e) {
                                                                if (e == null) {
                                                                    User.unsetCurrentUser();
                                                                }
                                                                setLoggedInSharedPreferences(true, context);
                                                                callback.processCompleted(true);
                                                            }
                                                        });

                                                    } else {
                                                        setLoggedInSharedPreferences(true, context);
                                                        callback.processCompleted(true);
                                                    }
                                                }
                                            }).execute();
                                        } else {
                                            setLoggedInSharedPreferences(true, context);
                                            callback.processCompleted(true);
                                        }
                                    } else {

                                        Log.e(Constants.TAG, "Error registering user with Google+: " + e.getMessage());
                                        e.printStackTrace();
                                        callback.processCompleted(false);
                                    }
                                }
                            });
                        } else {
                            User.loginUser(finalUsername, password, new LoginUserCallback() {
                                @Override
                                public void loggedInWithParseException(boolean success, boolean isNew, ParseException e) {
                                    if (e == null) {
                                        setLoggedInSharedPreferences(true, context);
                                        callback.processCompleted(true);
                                    } else {
                                        Log.e(Constants.TAG, "Error logging in user with Google+: " + e.getMessage());
                                        e.printStackTrace();
                                        callback.processCompleted(false);
                                    }

                                }
                            });
                        }
                    }
                });
            }
        } else {
            // Signed out, show unauthenticated UI.
            Log.d(Constants.TAG, "handleSignInResultError:" + result.getStatus().getStatusMessage());
            Log.d(Constants.TAG, "handleSignInResultError:" + result.getStatus().getStatusCode());
            callback.processCompleted(false);

        }
    }

    static void registerUser(String username, String password, String email, String firstName, String lastName,
                             ParseFile profilePic, final RegisterUserCallBack callBack) {
        final ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        if (firstName != null) {
            user.put(Constants.User.firstName, firstName);
        }
        if (lastName != null) {
            user.put(Constants.User.lastName, lastName);
        }
        if (profilePic != null) {
            profilePic.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        user.signUpInBackground(new SignUpCallback() {
                            @Override
                            public void done(ParseException e) {
                                callBack.registeredUser(e == null, e);
                            }
                        });
                    } else {
                        Log.d(Constants.TAG, "Error saving parseFile: " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
            user.put(Constants.User.profilePicture, profilePic);
        } else {
            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    callBack.registeredUser(e == null, e);
                }
            });
        }
    }

    interface RegisterUserCallBack {
        void registeredUser(boolean success, ParseException e);
    }

    private static void getUserForUsername(String username, final GetUserCallback callback) {
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo(Constants.User.username, username);
        query.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser object, ParseException e) {
                if (object != null) {
                    callback.gotUser(new User(object));
                } else {
                    if (e != null) {
                        Log.d(Constants.TAG, "GET_USER_ERROR: "+e.getMessage());
                    }
                    callback.gotUser(null);
                }
            }
        });
    }

    interface GetUserCallback {
        void gotUser(User user);
    }

    private static String generateGooglePassword(GoogleSignInAccount acct) {
        String email = acct.getEmail();
        String reverseEmail = new StringBuilder(email).reverse().toString();
        return email + reverseEmail + "spiritline";
    }

    private static void createGoogleParseUser(final GoogleSignInAccount acct, final Activity activity) {
        final ParseUser user = new ParseUser();
        if (acct.getPhotoUrl() != null) {
            File pictureFile = new File(acct.getPhotoUrl().getPath());
            final ParseFile parseFile = new ParseFile(pictureFile);
            parseFile.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        user.setUsername(acct.getEmail());
                        user.setPassword(generateGooglePassword(acct));
                        user.setEmail(acct.getEmail());
                        user.put(Constants.User.lastName, acct.getFamilyName());
                        user.put(Constants.User.firstName, acct.getGivenName());
                        user.put(Constants.User.profilePicture, parseFile);
                        user.put(Constants.User.accountType, "Google");
                        user.signUpInBackground(new SignUpCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    MethodHelper.showAlert(activity, "Success", "Registration successful!");
                                    MethodHelper.goBackToFirstActivity(activity);
                                } else {
                                    Log.e(Constants.TAG, "Error creating ParseUser from GoogleSignIn: " + e.getMessage());
                                    e.printStackTrace();
                                }
                            }
                        });
                    } else {
                        Log.e(Constants.TAG, "Error saving parseFile when creating new Google user: " + e.getMessage() + ". Error code is: " + e.getCode());
                        e.printStackTrace();
                    }
                }
            });
        } else {
            user.setUsername(acct.getEmail());
            user.setPassword(generateGooglePassword(acct));
            user.setEmail(acct.getEmail());
            user.put(Constants.User.lastName, acct.getFamilyName());
            user.put(Constants.User.firstName, acct.getGivenName());
            user.put(Constants.User.accountType, "Google");
            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        MethodHelper.showAlert(activity, "Success", "Registration successful!");
                        MethodHelper.goBackToFirstActivity(activity);
                    } else {
                        Log.e(Constants.TAG, "Error creating ParseUser from GoogleSignIn: " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
        }

    }

    static void saveUserInfo(final String firstName, final String lastName, final String email, final String username, final byte[] profileBytes, final SaveUserCallback callback) {
        final ParseUser user = ParseUser.getCurrentUser();
        if (profileBytes != null) {
            ParseFile profilePicture = new ParseFile(profileBytes);
            user.put(Constants.User.profilePicture, profilePicture);
            profilePicture.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        User.unsetCurrentUser();
                        user.put(Constants.User.firstName, firstName.replace(" ", ""));
                        user.put(Constants.User.lastName, lastName.replace(" ", ""));
                        user.setEmail(email);
                        user.setUsername(username);
                        user.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    User.unsetCurrentUser();
                                    callback.userSaved(true, null);
                                } else {
                                    Log.e(Constants.TAG, "Error saving ParseUser information: " + e.getMessage());
                                    e.printStackTrace();
                                    callback.userSaved(false, e.getMessage());
                                }
                            }
                        });
                    } else {
                        Log.e(Constants.TAG, "Error saving Parse User profile picture: " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
        } else {
            user.put(Constants.User.firstName, firstName.replace(" ", ""));
            user.put(Constants.User.lastName, lastName.replace(" ", ""));
            user.setEmail(email);
            user.setUsername(username);
            user.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        callback.userSaved(true, null);
                    } else {
                        Log.e(Constants.TAG, "Error saving ParseUser information: " + e.getMessage());
                        e.printStackTrace();
                        callback.userSaved(false, e.getMessage());
                    }
                }
            });
        }

    }

    interface SaveUserCallback {
        void userSaved(boolean success, String errorMessage);
    }

    private static void completeLoginWithFacebook(final android.app.Activity activity, final LoginUserCallback callback) {
        ArrayList<String> permissions = new ArrayList<String>();
        permissions.add("public_profile");
        permissions.add("email");
        Log.d(Constants.TAG, "loginWithFacebook0");
        ParseFacebookUtils.logInWithReadPermissionsInBackground(activity, permissions, new LogInCallback() {
            @Override
            public void done(final ParseUser user, final ParseException e) {
                Log.d(Constants.TAG, "loginWithFacebook1");
                if (e != null) {
                    Log.d(Constants.TAG, "Error signing FacebookUser in: " + e.getMessage() + ". Error code is: " + e.getCode());
                    callback.loggedInWithParseException(false, false, e);
                    return;
                }
                GraphRequest request = GraphRequest.newMeRequest(
                        AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.d(Constants.TAG, "loginWithFacebook2");
                                if (object == null) {
                                    callback.loggedInWithParseException(false, false, null);
                                } else {
                                    Log.d(Constants.TAG, "JSON: " + object);
                                    User.setCurrentUserForJSONObject(object, activity.getApplicationContext());
                                    ParseUser.getCurrentUser().put(Constants.User.newUser, true);
                                    ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                User.currentUser = new User(ParseUser.getCurrentUser());
                                                callback.loggedInWithParseException(e == null, ParseUser.getCurrentUser().isNew(), null);
                                            } else {
                                                Log.e(Constants.TAG, "Error saving facebook parseUser in background: " + e.getMessage());
                                                e.printStackTrace();
                                            }

                                        }
                                    });
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,picture.type(small),email");
                request.setParameters(parameters);
                request.executeAsync();
            }
        });
    }

    private static void setCurrentUserForJSONObject(JSONObject object, Context context) {
        ParseUser.getCurrentUser().put(Constants.User.accountType, "Facebook");
        try {
            String email = object.getString("email");
            if (email != null) {
                if (ParseUser.getCurrentUser().getEmail() == null) {
                    ParseUser.getCurrentUser().setEmail(email);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String firstName = object.getString("first_name");
            ParseUser.getCurrentUser().put(Constants.User.firstName, firstName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String lastName = object.getString("last_name");
            ParseUser.getCurrentUser().put(Constants.User.lastName, lastName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setProfilePictureFromFacebookUserDictionary(object, context);
    }

    private static void setProfilePictureFromFacebookUserDictionary(JSONObject object, final Context context) {
        new ImageDownloader(getImageURLFromJSONObject(object), new ImageDownloader.ImageDownloaderCallback() {
            @Override
            public void downloadedImage(Bitmap bitmap) {
                if (bitmap != null) {
                    ParseFile file = MethodHelper.convertBitmapToParseFile(bitmap, context.getResources().getConfiguration().orientation);
                    ParseUser.getCurrentUser().put(Constants.User.profilePicture, file);
                    ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d(Constants.TAG, "ProfilePicture saved successfully!");
                            } else {
                                Log.e(Constants.TAG, "Error saving Facebook profile Picture: " + e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        }).execute();

    }

    private static String getImageURLFromJSONObject(JSONObject object) {
        try {
            JSONObject pictureObject = object.getJSONObject("picture");
            if (pictureObject != null) {
                JSONObject dataObject = pictureObject.getJSONObject("data");
                return dataObject.getString("url");

            } else {
                return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    enum AccountType {
        Facebook, Google, Parse
    }

}
