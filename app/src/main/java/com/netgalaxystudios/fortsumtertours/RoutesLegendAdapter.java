package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by coryciepiela on 5/4/17.
 */

class RoutesLegendAdapter extends RecyclerView.Adapter<RoutesLegendAdapter.ViewHolder> {

    private Activity context;
    ArrayList<Object> searchResults = new ArrayList<Object>();

    RoutesLegendAdapter(Activity context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    @Override
    public RoutesLegendAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.routes_recycler_item, parent, false);
        return new RoutesLegendAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RoutesLegendAdapter.ViewHolder holder, final int position) {
        Object object = searchResults.get(position);
        if (object instanceof Route) {
            Route route = (Route) object;
            drawLine(holder.mRouteItemImage, route);
            holder.mRouteItemTitle.setText(route.title);
        } else if (object instanceof RouteAnnotation) {
            RouteAnnotation routeAnnotation = (RouteAnnotation) object;
            holder.mRouteItemTitle.setText(routeAnnotation.title);
            holder.mRouteItemImage.setImageDrawable(context.getResources().getDrawable(R.drawable.google_marker_white));
            holder.mRouteItemImage.setColorFilter(routeAnnotation.pinColor);
        }
    }

    private void drawLine(ImageView imageView, Route route) {
        Bitmap bmp;
        bmp = Bitmap.createBitmap(300, 300, Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(bmp);
        imageView.draw(c);

        Paint p = new Paint();
        p.setColor(route.lineColor);
        p.setStrokeWidth(20);
        p.setPathEffect(new DashPathEffect(new float[] {20, 40, 60}, 0));
        c.drawLine(0, 150, 800, 150, p);
        imageView.setImageBitmap(bmp);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mRouteItemTitle;
        private final ImageView mRouteItemImage;

        ViewHolder(final View itemView) {
            super(itemView);
            mRouteItemTitle = (TextView) itemView.findViewById(R.id.route_item_title);
            mRouteItemImage = (ImageView) itemView.findViewById(R.id.route_item_image);
        }
    }

}
