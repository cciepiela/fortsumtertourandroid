package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by coryciepiela on 3/14/17.
 */

class BoatAdapter extends RecyclerView.Adapter<BoatAdapter.ViewHolder>{
    private Activity context;
    ArrayList<Boat> searchResults = new ArrayList<Boat>();

    BoatAdapter(Activity context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    @Override
    public BoatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.boat_recycler_item, parent, false);
        return new BoatAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final BoatAdapter.ViewHolder holder, final int position) {
        Boat boat = searchResults.get(position);
        holder.mBoatImage.setImageDrawable(null);
        boat.getBoatImage(new Boat.BoatImageCallback() {
            @Override
            public void gotBoatImage(byte[] imageData) {
                if (imageData != null){
                    Glide.with(context).load(imageData).override(300, 300).into(holder.mBoatImage);
                }
            }
        });
        holder.mBoatLocation.setText(boat.getDepartureLocation());
        holder.mBoatTitle.setText(boat.mTitle);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DefaultApplication.sBoat = searchResults.get(position);
                BoatDetailsActivity.displayBoatDetailsActivity(context, searchResults.get(position));
            }
        });
        MethodHelper.setTextViewTypeFaceItalicized(holder.mBoatLocation, context);
        MethodHelper.setTextViewTypeFaceItalicized(holder.mBoatTitle, context);

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView mBoatImage;
        private final TextView mBoatTitle;
        private final TextView mBoatLocation;

        ViewHolder(final View itemView) {
            super(itemView);
            mBoatImage = (ImageView)itemView.findViewById(R.id.boat_recycler_image_view);
            mBoatTitle = (TextView)itemView.findViewById(R.id.boat_recycler_title_text);
            mBoatLocation = (TextView)itemView.findViewById(R.id.boat_recycler_location_text);
        }
    }
}
