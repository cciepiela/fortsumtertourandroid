package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class VrModeSelectionActivity extends AppCompatActivity {

    private TextView mFullScreenButton;
    private TextView mCardBoardButton;
    private TextView mInfoButton;
    private static VrVideo selectedVideo;
    private ImageView mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vr_mode_selection);

        connectUi();
    }

    private void connectUi() {
        mFullScreenButton = (TextView)findViewById(R.id.full_screen_button);
        mCardBoardButton = (TextView)findViewById(R.id.cardboard_button);
        mInfoButton = (TextView)findViewById(R.id.info_button);
        mBackButton = (ImageView)findViewById(R.id.back_button);
        setListeners();
    }

    private void setListeners() {
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VirtualTourPlayerActivity.display(selectedVideo, false, VrModeSelectionActivity.this);
            }
        });
        mCardBoardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VirtualTourPlayerActivity.display(selectedVideo, true, VrModeSelectionActivity.this);
            }
        });
        mInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MethodHelper.showAlert(VrModeSelectionActivity.this, getResources().getString(R.string.cardboard_title), getResources().getString(R.string.cardboard_message));
            }
        });
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    static void displayVrModeSelectionActivity(Activity activity, VrVideo vrVideo){
        selectedVideo = vrVideo;
        Intent i = new Intent(activity, VrModeSelectionActivity.class);
        activity.startActivity(i);
    }
}
