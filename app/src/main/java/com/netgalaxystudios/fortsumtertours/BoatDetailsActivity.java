package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class BoatDetailsActivity extends AppCompatActivity {

    private TextView mBoatFeaturesHeader;
    private TextView mAmenitiesButton;
    private TextView mBeaconLocationsButton;
    private TextView mThreeDToursButton;
    private ImageView mBackButton;
    private static Boat mBoat;
    private TextView mHeaderTitleText;
    private int mOrientation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boat_details);

        connectUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(Constants.TAG, "BoatDetails onResume was called! the");
        connectUi();
    }

    private void connectUi() {
        if (mBoat == null) {
            return;
        }
        mBoatFeaturesHeader = (TextView)findViewById(R.id.boat_features_text);
        mAmenitiesButton = (TextView)findViewById(R.id.amenities_button);
        mBeaconLocationsButton = (TextView)findViewById(R.id.beacon_locations_button);
        mThreeDToursButton = (TextView)findViewById(R.id.three_d_tours_button);
        mBackButton = (ImageView)findViewById(R.id.back_button);
        TextView dimensionsText = (TextView)findViewById(R.id.dimensions_text);
        dimensionsText.setText(String.format(getResources().getString(R.string.dimensions_text), mBoat.mLength, mBoat.mWidth));
        TextView featuresText = (TextView)findViewById(R.id.features_text);
        featuresText.setText(Html.fromHtml(mBoat.mDetails));
        final ImageView headerImage = (ImageView)findViewById(R.id.header_picture);
        mBoat.getBoatImage(new Boat.BoatImageCallback() {
            @Override
            public void gotBoatImage(byte[] imageData) {
                if (imageData != null){
                    Glide.with(BoatDetailsActivity.this).load(imageData).into(headerImage);
                }
            }
        });
        mHeaderTitleText = (TextView)findViewById(R.id.header_title_text);
        mHeaderTitleText.setText(mBoat.mTitle);
        setListeners();
        setFonts();
    }

    private void setListeners() {
        mAmenitiesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FloorPlanActivity.displayFloorplanActivity(BoatDetailsActivity.this, FloorPlanActivity.FloorPlanType.AMENITIES, mOrientation);
            }
        });
        mBeaconLocationsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FloorPlanActivity.displayFloorplanActivity(BoatDetailsActivity.this, FloorPlanActivity.FloorPlanType.BEACONS, mOrientation);
            }
        });
        mThreeDToursButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VirtualTourSelectionActivity.display(BoatDetailsActivity.this, mBoat.mObjectId);
            }
        });
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setFonts(){
        TextView boatFeaturesText = (TextView)findViewById(R.id.features_text);
        MethodHelper.setTextViewTypeFaceRegular(boatFeaturesText, BoatDetailsActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(mHeaderTitleText, BoatDetailsActivity.this);
        MethodHelper.setTextViewTypeFaceRegular(mBoatFeaturesHeader, BoatDetailsActivity.this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mOrientation = newConfig.orientation;
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mOrientation = newConfig.orientation;
        }
    }


    static void displayBoatDetailsActivity(Activity activity, Boat boat){
        mBoat = boat;
        Intent i = new Intent(activity, BoatDetailsActivity.class);
        activity.startActivity(i);
    }
}
