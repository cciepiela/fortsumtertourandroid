package com.netgalaxystudios.fortsumtertours;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by coryciepiela on 3/30/17.
 */

class ImageSlider extends RelativeLayout {
    public ImageSlider(Context context) {
        super(context);
    }

    public ImageSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageSlider(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


}
