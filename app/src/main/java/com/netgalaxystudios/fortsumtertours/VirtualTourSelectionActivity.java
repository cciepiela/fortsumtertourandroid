package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class VirtualTourSelectionActivity extends AppCompatActivity {

    private ImageView mBackButton;
    private VirtualTourAdapter mAdapter = new VirtualTourAdapter(VirtualTourSelectionActivity.this);
    private String forObjectId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtual_tour_selection);

        forObjectId = getIntent().getStringExtra(Constants.VrVideo.forObjectId);

        connectUi();
        if (Build.VERSION.SDK_INT >= 21){
            Log.d(Constants.TAG, "Device information: BOARD - "+ Build.BOARD+" CPU - "+Build.SUPPORTED_ABIS.length+" CPU_ABI - "+Build.CPU_ABI);
        } else if (Build.VERSION.SDK_INT <= 20){
            Log.d(Constants.TAG, "Device information: BOARD - "+ Build.BOARD+" CPU - "+Build.CPU_ABI);
        }

    }

    static void display(Activity activity, @Nullable String forObjectId) {
        Intent i = new Intent(activity, VirtualTourSelectionActivity.class);
        if (forObjectId != null) {
            i.putExtra(Constants.VrVideo.forObjectId, forObjectId);
        }
        activity.startActivity(i);
    }

    private void connectUi() {
        mBackButton = (ImageView)findViewById(R.id.back_button);
        ImageView headerPicture = (ImageView)findViewById(R.id.header_picture);
        Glide.with(VirtualTourSelectionActivity.this)
                .load(R.drawable.spiritline_cruises_fleet)
                .override(300, 300)
                .fitCenter()
                .placeholder(R.drawable.placeholder)
                .into(headerPicture);
        TextView headerTitleText = (TextView)findViewById(R.id.header_title_text);
        MethodHelper.setTextViewTypeFaceRegular(headerTitleText, VirtualTourSelectionActivity.this);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
        setListeners();
        populateUi();
    }

    private void populateUi() {
        MethodHelper.showProgressHUDWithMessage("Loading...", this);
        VrVideo.getVrVideos(forObjectId, new VrVideo.GetVrVideo() {
            @Override
            public void gotVrVideos(ArrayList<VrVideo> vrVideos) {
                MethodHelper.hideHUD();
                mAdapter.searchResults = vrVideos;
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    class VirtualTourAdapter extends RecyclerView.Adapter<VirtualTourAdapter.ViewHolder> {
        private Activity context;
        ArrayList<VrVideo> searchResults = new ArrayList<VrVideo>();

        VirtualTourAdapter(Activity context) {
            this.context = context;
        }

        @Override
        public int getItemCount() {
            return searchResults.size();
        }

        @Override
        public VirtualTourAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.vr_video_recycler_item, parent, false);
            return new VirtualTourAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final VirtualTourAdapter.ViewHolder holder, int position) {
            final VrVideo video = searchResults.get(position);
            video.getImage(new MethodHelper.ImageCallback() {
                @Override
                public void gotImage(byte[] image) {
                    if (!VirtualTourSelectionActivity.this.isDestroyed()) {
                        if (image != null) {
                            Glide.with(context)
                                    .load(image)
                                    .override(100, 100)
                                    .centerCrop()
                                    .into(holder.mVrVideoImageView);
                        }else {
                            Glide.with(context)
                                    .load(R.drawable.placeholder)
                                    .override(100, 100)
                                    .centerCrop()
                                    .into(holder.mVrVideoImageView);
                        }
                    }

                }
            });
            holder.mVrVideoTitle.setText(video.text);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    VrModeSelectionActivity.displayVrModeSelectionActivity(VirtualTourSelectionActivity.this, video);
                }
            });
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private final TextView mVrVideoTitle;
            private final ImageView mVrVideoImageView;

            ViewHolder(final View itemView) {
                super(itemView);
                mVrVideoTitle = (TextView)itemView.findViewById(R.id.title);
                mVrVideoImageView = (ImageView)itemView.findViewById(R.id.picture);
            }
        }
    }

}
