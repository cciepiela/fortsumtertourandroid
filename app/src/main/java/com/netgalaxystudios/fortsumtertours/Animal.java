package com.netgalaxystudios.fortsumtertours;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by coryciepiela on 3/24/17.
 */

class Animal {
    String title;
    String habitatText;
    String factsText;
    ParseFile animalImage;

    Animal(ParseObject parseObject) {
        if (parseObject.getString(Constants.Animal.title) != null){
            title = parseObject.getString(Constants.Animal.title);
        }
        if (parseObject.getString(Constants.Animal.factsText) != null){
            factsText = parseObject.getString(Constants.Animal.factsText);
        }
        if (parseObject.getString(Constants.Animal.habitatText) != null){
            habitatText = parseObject.getString(Constants.Animal.habitatText);
        }
        if (parseObject.getParseFile(Constants.Animal.imageFile) != null){
            animalImage = parseObject.getParseFile(Constants.Animal.imageFile);
        }
    }

    void getAnimalImage(final MethodHelper.ImageCallback callback){
        if (animalImage != null){
            animalImage.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null){
                        callback.gotImage(data);
                    } else {
                        Log.e(Constants.TAG, "Error getting History image: "+e.getMessage());
                        e.printStackTrace();
                        callback.gotImage(null);
                    }
                }
            });
        }
    }

    static void getAnimals(final GetAnimalsCallback callback){
        final ArrayList<Animal> animals = new ArrayList<>();
        ParseQuery<ParseObject> query = getQuery();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null){
                    Log.d(Constants.TAG, "e was null and objects' size is: "+objects.size());
                    for (ParseObject object:objects){
                        Animal animal = new Animal(object);
                        animals.add(animal);
                    }
                    callback.gotAnimals(animals);
                } else {
                    Log.e(Constants.TAG, "Error getting histories: "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private static ParseQuery<ParseObject> getQuery(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.Animal.className);
        query.orderByAscending(Constants.Animal.title);
        return query;
    }

    interface GetAnimalsCallback{
        void gotAnimals(ArrayList<Animal> animals);
    }
}
