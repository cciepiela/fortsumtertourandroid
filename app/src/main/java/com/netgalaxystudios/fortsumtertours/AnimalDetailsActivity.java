package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class AnimalDetailsActivity extends AppCompatActivity {
    private static Animal mAnimal;
    private ImageView mBackButton;
    private TextView mFactsText;
    private TextView mHabitatText;
    private ImageView mAnimalPicture;
    private TextView mAnimalTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_details);

        connectUi();
    }

    private void connectUi() {
        MethodHelper.showProgressHUDWithMessage("Loading...", AnimalDetailsActivity.this);
        mAnimalTitle = (TextView)findViewById(R.id.animal_title);
        mAnimalPicture = (ImageView)findViewById(R.id.animal_picture);
        mHabitatText = (TextView)findViewById(R.id.habitat_text);
        mFactsText = (TextView)findViewById(R.id.facts_text);
        mBackButton = (ImageView)findViewById(R.id.back_button);
        setListeners();
        populateUi();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void populateUi(){
        if (mAnimal != null){
            mAnimalTitle.setText(mAnimal.title);
            mHabitatText.setText(Html.fromHtml(mAnimal.habitatText));
            mFactsText.setText(Html.fromHtml(mAnimal.factsText));
            mAnimal.getAnimalImage(new MethodHelper.ImageCallback() {
                @Override
                public void gotImage(byte[] image) {
                    if (image != null){
                        Glide.with(AnimalDetailsActivity.this)
                                .load(image)
                                .centerCrop()
                                .into(mAnimalPicture);
                        MethodHelper.hideHUD();
                    } else {
                        MethodHelper.hideHUD();
                    }
                }
            });
        } else {
            MethodHelper.hideHUD();
        }
    }

    static void displayAnimalDetailsActivity(Animal animal, Activity activity){
        mAnimal = animal;
        Intent i = new Intent(activity, AnimalDetailsActivity.class);
        activity.startActivity(i);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodHelper.setScreenNameForGa(AnimalDetailsActivity.this);
    }
}
