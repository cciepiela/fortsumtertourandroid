package com.netgalaxystudios.fortsumtertours;

/**
 * Created by coryciepiela on 3/6/17.
 */

class Constants {
    static String TAG = "LogTAG";

    static class Extras {
        static String URL_EXTRA = "URL_EXTRA_035478";
        public static String FOR_OWNER_EXTRA = "FOR_OWNER_EXTRA346783";
        static String TEXT_TYPE_EXTRA = "TEXT_TYPE_EXTRA238946";
        public static String COUPON_OBJECTID_EXTRA = "COUPON_OBJECTID_EXTRA234558";
        static String TITLE_EXTRA = "TITLE_EXTRA34598";
        public static String MESSAGE_EXTRA = "MESSAGE_EXTRA20956";
    }

    static class User {
        static String firstName = "firstName";
        static String lastName = "lastName";
        static String accessLevel = "accessLevel";
        static String profilePicture = "profilePicture";
        public static String objectId = "objectId";
        static String fullName = "fullName";
        static String newUser = "newUser";
        static String accountType = "accountType";
        static String username = "username";
    }

    static class RequestCodes {
        static final int REQUEST_IMAGE_CAPTURE = 1;
        static final int REQUEST_IMAGE_PICKER = 2;
        static final int REQUEST_BARCODE_IMAGE_CAPTURE = 3;
        static final int REQUEST_BARCODE_IMAGE_PICKER = 4;
    }

    static class SharedPreferencesKeys {
        static String sharedPreferencesKey = "COLLECTIVEFORSHAREPREFERENCESKEY34589778930";
        static String currentUserObjectId = "currentUserObjectId";
        public static String milesRadiusKey = "milesRadiusKey";
        static final String sLastMoveTime = "lastVideoViewDate";
        static final String permanentlyVerified = "PermanentlyVerified";
        static final String timerInterval = "timerInterval";
    }

    static class Config {
        static String overView = "overviewText";
        static String faq = "faq";
        static String privacyPolicy = "privacyPolicy";
        static String termsAndConditions = "termsAndConditions";
        static String marineLifeText = "marineLifeText";
        static String ourImpactText = "ourImpactText";
        static String shareUrl = "shareUrl";
        static String supportEmail = "supportEmail";
        static String harborMidpoint = "harborMidpoint";
        static String viewfinderLockDistance = "viewfinderDistance";
        static String locationUpdateInterval = "locationUpdateInterval";
        static String boatDepartingTitleText = "boatDepartingTitleText";
        static String boatDepartingText = "boatDepartingText";
    }

    static class Media {
        static String className = "Media";
        public static String file = "file";
        static String associatedObjectId = "associatedObjectId";
        public static String objectId = "objectId";
    }

    static class Boat {
        static String className = "Boat";
        static String title = "title";
        static String imageFile = "imageFile";
        static String length = "length";
        static String width = "width";
        static String details = "details";
        static String vrVideo = "vrVideo";
        static String amenitiesLvlOneFile = "amenitiesLvlOneFile";
        static String amenitiesLvlTwoFile = "amenitiesLvlTwoFile";
        static String amenitiesLvlThreeFile = "amenitiesLvlThreeFile";
        static String beaconsLvlOneFile = "beaconsLvlOneFile";
        static String beaconsLvlTwoFile = "beaconsLvlTwoFile";
        static String beaconsLvlThreeFile = "beaconsLvlThreeFile";
        static String levels = "levels";
        static final String active = "active";
        static String downtownDepartureRanges = "downtownDepartureRanges";
    }

    static class FortSumter {
        static String className = "FortSumter";
        static String beaconsFile = "beaconsFile";
        static String amenitiesFile = "amenitiesFile";
        static String imageFile = "imageFile";
        static String title = "title";
        static String timeMachine = "timeMachine";
        static String vrVideo = "vrVideo";
        static String details = "details";
    }

    static class History {
        static String className = "History";
        static String title = "title";
        static String objectId = "forObjectId";
        static String text = "text";
        static String imageFile = "imageFile";
    }

    static class PostcardFilter {
        static String className = "PostcardFilter";
        static String filterName = "filterName";
        static String imageFile = "imageFile";
        static String x = "x";
        static String y = "y";
        static String width = "width";
        static String height = "height";
        static String full = "full";
        static String fullWidth = "fullWidth";
        static String fullHeight = "fullHeight";
        static String alignBottom = "alignBottom";
        static String alignTop = "alignTop";
        static String alignRight = "alignRight";
        static String alignLeft = "alignLeft";
        static String centerX = "centerX";
        static String centerY = "centerY";
    }

    static class Animal {
        static String className = "Animal";
        static String title = "title";
        static String factsText = "text";
        static String habitatText = "habitat";
        static String imageFile = "imageFile";
    }

    static class Route {
        static String className = "Route";
        static String lineColor = "lineColor";
        static String lineWidth = "lineWidth";
        static String lats = "lats";
        static String longs = "longs";
        static String dottedLine = "dottedLine";
        static String title = "title";
    }

    static class RouteAnnotation {
        static String className = "RouteAnnotation";
        static String objectId = "objectId";
        static String title = "title";
        static String location = "location";
        static String url = "url";
        static String pinColor = "pinColor";
    }

    static class TimeMachine {
        static String className = "TimeMachine";
        static String title = "title";
        static String subtitle = "subtitle";
        static String firstImageFile = "firstImageFile";
        static String secondImageFile = "secondImageFile";
        static String beforeTitle = "beforeTitle";
        static String afterTitle = "afterTitle";
        static String beforeTitleColor = "beforeTitleColor";
        static String afterTitleColor = "afterTitleColor";
    }

    static class PoI {
        static String className = "ARLocation";
        static String title = "name";
        static String bodyText = "text";
        static String url = "url";
        static String location = "location";
        static String subTitle = "subtitle";
        static String active = "active";
        static String imageFile = "imageFile";
    }

    static class VrVideo {
        static String className = "VRVideo";
        static String text = "text";
        static String seconds = "seconds";
        static String url = "url";
        static String imageFile = "imageFile";
        static String forObjectId = "forObjectId";
    }

    static class Radius{
        static String kitUrl = "https://proximitykit.radiusnetworks.com/api/kits/9027";
        static String kitToken = "89605f5ddaecd3bcc14602d7cf1c9a869d067c84a125bd156bad0adf47e2660e";
    }

    static class FeaturedPhoto{
        static final String className = "Image";
        static final String forMain = "forMain";
        static final String imageFile = "imageFile";
        static final String forObjectId = "forObjectId";
    }

    static class Beacon {
        static String className = "Beacon";
        static String title = "title";
        static String subtitle = "subtitle";
        static String body = "body";
        static String distance = "distance";
        static String minor = "minor";
        static String major = "major";
        static String timeInterval = "timeInterval";
        static String secondaryMessageTimeFrame = "secondaryMessageTimeFrame";
        static String title2 = "title2";
        static String subtitle2 = "subtitle2";
        static String body2 = "body2";
        static String details = "details";
        static String imageFile = "imageFile";
    }

    static class Geofence {
        static String className = "Geofence";
        static String title = "title";
        static String objectId = "objectId";
        static String message = "message";
        static String location = "location";
        static String radius = "radius";

    }

    static class WebUrls{
        static final String spiritLineCruises = "http://spiritlinecruises.com/";
    }

    static class BroadcastEventNames {
        static String enteredGeofenceWithTimer = "enteredGeofenceWithTimer";
    }
}
