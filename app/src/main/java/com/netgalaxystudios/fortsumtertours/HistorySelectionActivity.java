package com.netgalaxystudios.fortsumtertours;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class HistorySelectionActivity extends AppCompatActivity {

    private HistoryAdapter mAdapter = new HistoryAdapter(HistorySelectionActivity.this);
    private ImageView mHeaderPicture;
    private ImageView mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_selection);

        connectUi();
    }

    private void connectUi() {
        mHeaderPicture = (ImageView)findViewById(R.id.header_picture);
        mBackButton = (ImageView)findViewById(R.id.back_button);
        RecyclerView historyRecyclerView = (RecyclerView)findViewById(R.id.history_recycler_view);
        historyRecyclerView.setLayoutManager(new LinearLayoutManager(HistorySelectionActivity.this));
        historyRecyclerView.setAdapter(mAdapter);
        setListeners();
        populateUi();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void populateUi(){
        History.getHistories(DefaultApplication.sFortSumter.objectId, new History.GetHistoriesCallback() {
            @Override
            public void gotHistories(ArrayList<History> histories) {
                Log.d(Constants.TAG, "returned histories' size is: "+histories.size());
                mAdapter.searchResults = histories;
                mAdapter.notifyDataSetChanged();
            }
        });
        Log.d(Constants.TAG, "imageData's size is: "+DefaultApplication.sFortSumter.imageData.length);
        Glide.with(HistorySelectionActivity.this)
                .load(DefaultApplication.sFortSumter.imageData)
                .centerCrop()
                .into(mHeaderPicture);
    }
}
