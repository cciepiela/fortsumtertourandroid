package com.netgalaxystudios.fortsumtertours;

import android.location.Location;
import android.util.Log;

import com.parse.ConfigCallback;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;

/**
 * Created by coryciepiela on 3/9/17.
 */

class Config {
    private static Config currentConfig;
    String privacyPolicy = "";
    String termsAndConditions = "";
    String overviewText = "";
    String marineLifeText = "";
    String ourImpactText = "";
    String faq = "";
    String shareUrl = "";
    String supportEmail = "";
    Location harborMidpoint = null;
    int viewFinderDistance;
    int locationUpdateInterval;
    String boatDepartingTitleText = "Boat Departing";
    String boatDepartingText = "The boat from Fort Sumter is departing. Please make your way back to the boat.";


    private Config(ParseConfig config) {
        super();
        if (config.get(Constants.Config.privacyPolicy)!= null) {
            privacyPolicy = config.getString(Constants.Config.privacyPolicy);
        }
        if (config.get(Constants.Config.termsAndConditions)!= null) {
            termsAndConditions = config.getString(Constants.Config.termsAndConditions);
        }
        if (config.get(Constants.Config.overView) != null){
            overviewText = config.getString(Constants.Config.overView);
        }
        if (config.get(Constants.Config.marineLifeText) != null){
            marineLifeText = config.getString(Constants.Config.marineLifeText);
        }
        if (config.get(Constants.Config.ourImpactText) != null){
            ourImpactText = config.getString(Constants.Config.ourImpactText);
        }
        if (config.get(Constants.Config.faq)!= null) {
            faq = config.getString(Constants.Config.faq);
        }
        if (config.get(Constants.Config.shareUrl)!= null) {
            shareUrl = config.getString(Constants.Config.shareUrl);
        }
        if (config.get(Constants.Config.supportEmail) != null){
            supportEmail = config.getString(Constants.Config.supportEmail);
        }
        if (config.getList(Constants.Config.harborMidpoint) != null && config.getList(Constants.Config.harborMidpoint).size() > 1){
            float latitude = Float.valueOf((String)config.getList(Constants.Config.harborMidpoint).get(1));
            float longitude = Float.valueOf((String)config.getList(Constants.Config.harborMidpoint).get(0));
            harborMidpoint = new Location("");
            harborMidpoint.setLatitude(latitude);
            harborMidpoint.setLongitude(longitude);
            Log.d(Constants.TAG, "FOUND_GEOPOINT: " + harborMidpoint);

        }
        locationUpdateInterval = config.getInt(Constants.Config.locationUpdateInterval);
        viewFinderDistance = config.getInt(Constants.Config.viewfinderLockDistance);
        if (config.get(Constants.Config.boatDepartingTitleText) != null){
            boatDepartingTitleText = config.getString(Constants.Config.boatDepartingTitleText);
        }
        if (config.get(Constants.Config.boatDepartingText) != null){
            boatDepartingText = config.getString(Constants.Config.boatDepartingText);
        }
    }

    //    Location coordinates = new Location("");
//            coordinates.setLatitude(config.getParseGeoPoint(Constants.Config.harborLatLng).getLatitude());
//            coordinates.setLongitude(config.getParseGeoPoint(Constants.Config.harborLatLng).getLongitude());
//    harborLatLngs =  coordinates;

    static Config getCurrentConfig () {
        if (currentConfig == null) {
            currentConfig = new Config(ParseConfig.getCurrentConfig());
            ParseConfig.getInBackground(new ConfigCallback() {
                @Override
                public void done(ParseConfig config, ParseException e) {
                    if (config != null) {
                        currentConfig = new Config(config);
                    }
                }
            });
        }
        return currentConfig;
    }
}
