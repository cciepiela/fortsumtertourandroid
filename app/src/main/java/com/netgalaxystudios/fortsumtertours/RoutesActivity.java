package com.netgalaxystudios.fortsumtertours;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class RoutesActivity  extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private ImageView mBackButton;
    private GoogleMap mMap;
    HashMap<Marker, String> urls = new HashMap<Marker, String>();
    HashMap<Marker, String> titles = new HashMap<Marker, String>();
    private TextView mInfoButton;
    private RelativeLayout mLegendLayout;
    private ImageView mHideLayoutButton;
    private RoutesLegendAdapter mAdapter = new RoutesLegendAdapter(RoutesActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routes);

        connectUi();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void connectUi() {
        mBackButton = (ImageView)findViewById(R.id.back_button);
        mInfoButton = (TextView)findViewById(R.id.info_button);
        mLegendLayout = (RelativeLayout)findViewById(R.id.legend_layout);
        mHideLayoutButton = (ImageView)findViewById(R.id.hide_layout_button);
        RecyclerView legendRecycler = (RecyclerView)findViewById(R.id.legend_recycler);
        legendRecycler.setLayoutManager(new LinearLayoutManager(RoutesActivity.this));
        legendRecycler.setAdapter(mAdapter);
        ImageView headerPicture = (ImageView)findViewById(R.id.header_picture);
        TextView headerTitleText = (TextView)findViewById(R.id.header_title_text);
        MethodHelper.setTextViewTypeFaceRegular(headerTitleText, RoutesActivity.this);
        Glide.with(RoutesActivity.this)
                .load(R.drawable.fs_boat_clipped)
                .override(500, 300)
                .into(headerPicture);
        setListeners();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(Constants.TAG, "info button clicked!");
                mLegendLayout.setVisibility(View.VISIBLE);
                showLegendLayout();
            }
        });
        mHideLayoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(Constants.TAG, "hideLayoutButton was pressed!");
                hideLegendLayout();
            }
        });
    }

    private void showLegendLayout(){
        TranslateAnimation hideViewAnimation = new TranslateAnimation(0, 0, 1000, 0);
        hideViewAnimation.setDuration(1000);
        mLegendLayout.startAnimation(hideViewAnimation);
    }

    private void hideLegendLayout(){
        TranslateAnimation hideViewAnimation = new TranslateAnimation(0, 0, 0, 1000);
        hideViewAnimation.setDuration(1000);
        hideViewAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLegendLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mLegendLayout.startAnimation(hideViewAnimation);
    }

    private void loadSelectedOptions () {
        mMap.clear();
        Route.getRoutes(new Route.RouteCallback() {
            @Override
            public void gotRoutes(ArrayList<Route> routes) {
                mAdapter.searchResults.addAll(routes);
                addRoutes(routes);
            }
        });
        RouteAnnotation.getRouteAnnotations(new RouteAnnotation.RouteAnnotationCallback() {
            @Override
            public void gotRouteAnnotations(ArrayList<RouteAnnotation> routeAnnotations) {
                mAdapter.searchResults.addAll(routeAnnotations);
                mAdapter.notifyDataSetChanged();
                for (RouteAnnotation annotation:routeAnnotations) {
                    LatLng latLng = new LatLng(annotation.coordinate.getLatitude(), annotation.coordinate.getLongitude());
                    MarkerOptions options = new MarkerOptions().
                            position(latLng).
                            icon(getMarkerIcon(annotation)).
                            title(annotation.title);
                    Marker marker = mMap.addMarker(options);
                    urls.put(marker, annotation.url);
                    titles.put(marker, annotation.title);
                }
                mMap.setOnInfoWindowClickListener(RoutesActivity.this);
            }
        });
    }

    // method definition
    public static BitmapDescriptor getMarkerIcon(RouteAnnotation annotation) {
        float[] hsv = new float[3];
        Color.colorToHSV(annotation.pinColor, hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
    private void addRoutes(ArrayList<Route> routes) {
        for (Route route:routes) {
            ArrayList<LatLng> points = new ArrayList<LatLng>();

            for (Location location:route.getCoordinates()) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                points.add(latLng);
            }
            PolylineOptions rectOptions = new PolylineOptions().width(5).color(route.lineColor).visible(true).addAll(points);
            Polyline polyline = mMap.addPolyline(rectOptions);
            if (route.dottedLine) {
                List<PatternItem> pattern = Arrays.<PatternItem>asList(new Dash(30), new Gap(20));
                polyline.setPattern(pattern);
            }
            Log.d(Constants.TAG, "POLYLINE:" + polyline.getPoints());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(new LatLng(32.818720, -79.941942));
                builder.include(new LatLng(32.743585, -79.942125));
                builder.include(new LatLng(32.741438, -79.850602));
                builder.include(new LatLng(32.813026, -79.868037));

                LatLngBounds bounds = builder.build();
                int padding = 0; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                mMap.moveCamera(cu);
                loadSelectedOptions();
                try {
                    mMap.setMyLocationEnabled(true);
                } catch (SecurityException e) {
                    Log.e(Constants.TAG, "Error enabling MyLocationButton for Routes Google Map: "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        String url = urls.get(marker);
        String title = titles.get(marker);
        if (url != null) {
            WebActivity.displayWebActivity(this, url, title);
        }
    }
}
