package com.netgalaxystudios.fortsumtertours;

import android.graphics.Color;
import android.location.Location;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by coryciepiela on 3/29/17.
 */

class Route {
    private String objectId;
    private List<Double> lats;
    private List<Double> longs;
    boolean dottedLine;
    public int lineColor = Color.RED;
    private int lineWidth;
    String title;

    private Route(ParseObject parseObject) {
        objectId = parseObject.getObjectId();
        dottedLine = parseObject.getBoolean(Constants.Route.dottedLine);
        if (parseObject.getString(Constants.Route.lineColor) != null){
            lineColor = Color.parseColor("#" + parseObject.getString(Constants.Route.lineColor));
        }
        if (parseObject.getNumber(Constants.Route.lineWidth) != null){
            lineWidth = (int)parseObject.getNumber(Constants.Route.lineWidth);
        }
        if (parseObject.getList(Constants.Route.lats) != null){
            lats = convert(parseObject.getList(Constants.Route.lats));
        }
        if (parseObject.getList(Constants.Route.longs) != null){
            longs = convert(parseObject.getList(Constants.Route.longs));
        }
        if (parseObject.getString(Constants.Route.title) != null){
            title = parseObject.getString(Constants.Route.title);
        }
    }

    private List<Double> convert(List<Object> stringArray) {
        List<Double> doubles = new ArrayList<>();
        for (Object object:stringArray) {
            String string = (String)object;
            doubles.add(Double.parseDouble(string));
        }
        return doubles;
    }

    static void getRoutes(final RouteCallback callback){
        final ArrayList<Route> routes = new ArrayList<>();
        ParseQuery<ParseObject> routeQuery = ParseQuery.getQuery(Constants.Route.className);
        routeQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null){
                    if (objects.size() > 0){
                        for (ParseObject object:objects){
                            Route route = new Route(object);
                            if (route.lats.size() == route.longs.size() && route.lats.size() > 0){
                                routes.add(route);
                            }
                        }
                        callback.gotRoutes(routes);
                    }
                } else {
                    Log.e(Constants.TAG, "Error getting routes from database: "+e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    ArrayList<Location> getCoordinates () {
        int i = 0;
        ArrayList<Location> coords = new ArrayList<Location>();
        for (Double l:lats) {
            if (lats.size() > i && longs.size() > i) {
                Location location = new Location("");
                location.setLatitude(lats.get(i));
                location.setLongitude(longs.get(i));
                coords.add(location);
            }
            i++;
        }
        return coords;
    }

    interface RouteCallback{
        void gotRoutes(ArrayList<Route> routes);
    }
}
