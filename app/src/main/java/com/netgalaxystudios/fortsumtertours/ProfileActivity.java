package com.netgalaxystudios.fortsumtertours;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;

import tangxiaolv.com.library.EffectiveShapeView;

public class ProfileActivity extends AppCompatActivity {

    private EditText mFirstNameEditText;
    private EditText mLastNameEditText;
    private EditText mEmailEditText;
    private EditText mUsernameEditText;
    private TextView mSaveButton;
    private EffectiveShapeView mProfilePicture;
    private TextView mFirstNameTextView;
    private Uri mProfileUri;
    private ImageView mBackButton;
    private File mProfileFile;
    private byte[] mBytes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        connectUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void connectUi() {
        mFirstNameEditText = (EditText) findViewById(R.id.first_name_edit_text);
        mLastNameEditText = (EditText) findViewById(R.id.last_name_edit_text);
        mEmailEditText = (EditText) findViewById(R.id.email_edit_text);
        mUsernameEditText = (EditText) findViewById(R.id.username_edit_text);
        mSaveButton = (TextView) findViewById(R.id.save_button);
        mProfilePicture = (EffectiveShapeView) findViewById(R.id.profile_picture);
        mFirstNameTextView = (TextView) findViewById(R.id.first_name_text);
        mBackButton = (ImageView) findViewById(R.id.back_button);
        if (User.getCurrentUser().accountType == User.AccountType.Facebook || User.getCurrentUser().accountType == User.AccountType.Google) {
            mUsernameEditText.setEnabled(false);
        }
        populateUi();
        setListeners();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(Constants.TAG, "onDestroy was called!");
        mProfileFile = null;
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(ProfileActivity.this);
                alert.setTitle("Select option");
                alert.setMessage("Please select whether to take a photo or choose one from the library.");
                alert.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                            startActivityForResult(takePictureIntent, Constants.RequestCodes.REQUEST_IMAGE_CAPTURE);
                        }
                    }
                });
                alert.setNegativeButton("Photo Library", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent choosePictureIntent = new Intent();
                        choosePictureIntent.setType("image/*");
                        choosePictureIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(choosePictureIntent, Constants.RequestCodes.REQUEST_IMAGE_PICKER);
                    }
                });
                alert.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveButtonPressed();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
//            if (requestCode == 1) {
//                Bitmap photo = (Bitmap) data.getExtras().get(MediaStore.EXTRA_OUTPUT);
//                byte[] photoBytes = MethodHelper.bitmapToByteArray(photo);
//                Log.d(Constants.TAG, "Retrieved bitmap from camera intent is: "+photo);
//                Glide.with(this).load(photo)
//                        .into(mProfilePicture);
//            }
            mProfileUri = data.getData();
            Log.d(Constants.TAG, "the requestCode is: " + requestCode);
            Log.d(Constants.TAG, "the resultCode is: " + resultCode);
            Log.d(Constants.TAG, "mProfileUri is: " + mProfileUri);
            Log.d(Constants.TAG, "data.getData() returns: " + data.getData());
            if (mProfileUri != null) {
//                MethodHelper.BitmapAsyncTask task = new MethodHelper.BitmapAsyncTask(this);
//                mBytes = convertUriToByteArray(ProfileActivity.this, mProfileUri);
//                mBitmap = task.doInBackground(mProfileUri);
                Glide.with(ProfileActivity.this).load(mProfileUri).into(mProfilePicture);
            }
        } else if (resultCode == RESULT_CANCELED) {
            Log.d(Constants.TAG, "resultCode equals RESULT_CANCELED");
        }
    }

    private void populateUi() {
        User user = User.getCurrentUser();
        Log.d(Constants.TAG, "Populating Ui is fetching this current user from the User class: " + User.getCurrentUser().email);
        String name = getName(user);
        mFirstNameTextView.setText(name);
        if (user.firstName != null) {
            mFirstNameEditText.setText(user.firstName);
        }
        if (user.lastName != null) {
            mLastNameEditText.setText(user.lastName);
        }
        if (user.email != null) {
            mEmailEditText.setText(user.email);
        }
        if (user.username != null) {
            mUsernameEditText.setText(user.username);
        }
        Log.d(Constants.TAG, "user.parseProfilePicture wasn't null!");
        user.getUserProfilePictureFile(new User.ProfilePictureCallback() {
            @Override
            public void gotProfilePictureFile(File pictureFile) {
                if (pictureFile != null) {
                    Log.d(Constants.TAG, "Returned pictureFile is: " + pictureFile);
                    mProfileFile = pictureFile;
                    Log.d(Constants.TAG, "pictureFile's total space is: " + pictureFile.getTotalSpace());
                    Glide.with(ProfileActivity.this)
                            .load(pictureFile)
                            .into(mProfilePicture);
                } else {

                    Log.d(Constants.TAG, "mProfileFile wasn't null! This is what it is: " + mProfileFile);
                    Glide.with(ProfileActivity.this)
                            .load(mProfileFile)
                            .placeholder(R.drawable.anonymous)
                            .into(mProfilePicture);
                }


            }
        });


    }

    private void saveButtonPressed() {
        Log.d(Constants.TAG, "Picture resources when save button was pressed are -- mBytes: " + mBytes + " mProfileUri: " + mProfileUri);
        MethodHelper.showProgressHUDWithMessage("Saving...", ProfileActivity.this);
        byte[] profileBytes = null;
        if (mBytes != null || mProfileUri != null) {
            if (mBytes != null) {
                profileBytes = mBytes;
            }
            if (mProfileUri != null) {
                profileBytes = MethodHelper.convertUriToByteArray(ProfileActivity.this, mProfileUri);
            }
        } else {
            profileBytes = null;
        }
        Log.d(Constants.TAG, "profileBytes in saveButtonPressed are: " + profileBytes);
        if (allFieldsAreFilledOut()) {
            User.saveUserInfo(mFirstNameEditText.getText().toString(),
                    mLastNameEditText.getText().toString(),
                    mEmailEditText.getText().toString(),
                    mUsernameEditText.getText().toString(),
                    profileBytes,
                    new User.SaveUserCallback() {
                        @Override
                        public void userSaved(boolean success, String errorMessage) {
                            MethodHelper.hideHUD();
                            if (success) {
                                mProfileFile = null;
                                MethodHelper.showAlert(ProfileActivity.this, "Success", "Profile information was saved successfully!");
//                                populateUi();
                            } else {
                                MethodHelper.showAlert(ProfileActivity.this, "Error", errorMessage);
                            }
                        }
                    });
        }
    }

    private String getName(User user) {
        String name = "";
        if (user.firstName != null) {
            name = user.firstName;

            if (!name.equals("")) {
                name = name + " " + user.lastName;
            } else {
                name = user.lastName;
            }

            if (name.replace(" ", "").equals("")) {
                name = user.username;
            }
        }
        return name;
    }

    private boolean allFieldsAreFilledOut() {
        if (mUsernameEditText.getText() == null) {
            MethodHelper.hideHUD();
            MethodHelper.showAlert(this, "Username Required", "A username is required. Please enter a username and try again");
            return false;
        }
        if (mFirstNameEditText.getText() == null) {
            MethodHelper.hideHUD();
            MethodHelper.showAlert(this, "First Name Required", "A first name is required. Please enter your first name and try again");
            return false;
        }
        if (mLastNameEditText.getText() == null) {
            MethodHelper.hideHUD();
            MethodHelper.showAlert(this, "Last Name Required", "A last name is required. Please enter your last name and try again");
            return false;
        }
        if (mEmailEditText.getText() == null) {
            MethodHelper.hideHUD();
            MethodHelper.showAlert(this, "Email Required", "An email is required. Please enter your email and try again");
            return false;
        }
        if (mEmailEditText.getText().toString().replace(" ", "").equalsIgnoreCase("")) {
            MethodHelper.hideHUD();
            MethodHelper.showAlert(this, "Email Required", "An email is required. Please enter your email and try again");
            return false;
        }
        if (mUsernameEditText.getText().toString().replace(" ", "").equalsIgnoreCase("")) {
            MethodHelper.hideHUD();
            MethodHelper.showAlert(this, "Username Required", "A username is required. Please enter a username and try again");
            return false;
        }
        if (mFirstNameEditText.getText().toString().replace(" ", "").equalsIgnoreCase("")) {
            MethodHelper.hideHUD();
            MethodHelper.showAlert(this, "First Name Required", "A first name is required. Please enter your first name and try again");
            return false;
        }
        if (mLastNameEditText.getText().toString().replace(" ", "").equalsIgnoreCase("")) {
            MethodHelper.hideHUD();
            MethodHelper.showAlert(this, "Last Name Required", "A last name is required. Please enter your last name and try again");
            return false;
        }
        return true;
    }

}
