package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by coryciepiela on 3/24/17.
 */

class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private Activity context;
    ArrayList<History> searchResults = new ArrayList<History>();

    HistoryAdapter(Activity context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.history_recycler_item, parent, false);
        return new HistoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final HistoryAdapter.ViewHolder holder, int position) {
        final History history = searchResults.get(position);
        holder.mHistoryPicture.setImageDrawable(null);
        history.getHistoryImage(new History.ImageCallback() {
            @Override
            public void gotImage(byte[] image) {
                if (image != null) {
                    Log.d(Constants.TAG, "imageData's length is:" + image.length);
                    Glide.with(context).load(image).override(100, 100).into(holder.mHistoryPicture);
                }
            }
        });
        holder.mHistoryTitle.setText(history.title);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HistoryDetailsActivity.displayHistoryDetailsActivity(history, context);
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mHistoryTitle;
        private final ImageView mHistoryPicture;

        ViewHolder(final View itemView) {
            super(itemView);
            mHistoryTitle = (TextView) itemView.findViewById(R.id.history_recycler_title);
            mHistoryPicture = (ImageView) itemView.findViewById(R.id.history_recycler_picture);
        }
    }
}
