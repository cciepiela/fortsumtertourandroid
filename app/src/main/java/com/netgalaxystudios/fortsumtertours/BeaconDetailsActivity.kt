package com.netgalaxystudios.fortsumtertours

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

class BeaconDetailsActivity : AppCompatActivity() {
    var beacon:Beacon? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        beacon = BeaconDetailsActivity.beacon
        BeaconDetailsActivity.beacon = null
        setContentView(R.layout.activity_beacon_details)
        val backButton = findViewById<ImageView>(R.id.back_button_image_view) as ImageView
        backButton.setOnClickListener {
            onBackPressed()
        }

        (findViewById<TextView>(R.id.title_text_view)).text = beacon?.title
        (findViewById<TextView>(R.id.details_text_view)).text = beacon?.details
        beacon?.getImage { image:ByteArray? ->
            if (image != null) {
                Glide.with(this).load(image).into(findViewById<ImageView>(R.id.image_view))
            }
        }

    }

    companion object {
        var beacon:Beacon? = null

        fun display(activity:Activity, beacon:Beacon) {
            BeaconDetailsActivity.beacon = beacon
            activity.startActivity(Intent(activity, BeaconDetailsActivity::class.java))
        }

    }
}
