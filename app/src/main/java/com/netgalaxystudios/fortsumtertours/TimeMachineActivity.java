package com.netgalaxystudios.fortsumtertours;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;


public class TimeMachineActivity extends AppCompatActivity {

    static TimeMachine mTimeMachine;
    private TextView mToolbarTitle;
    private ImageView mBackButton;
    private SeekBar seekBar;
    private ImageView mStaticImage;
    private ImageView mChangingImage;
    int progress = 0;
    private TextView mChangingImageTitle;
    private TextView mStaticImageTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_machine);

        connectUi();
        seekBar.setPadding(0,0,0,0);
        int screenWidth = this.getResources().getDisplayMetrics().widthPixels;
        seekBar.setMax(screenWidth);
        progress = screenWidth / 2;

        seekBar.setProgress(progress);
        FrameLayout target = (FrameLayout) findViewById(R.id.target);


        ViewGroup.LayoutParams lp = target.getLayoutParams();
        lp.width = progress;
        target.setLayoutParams(lp);
    }


    private void connectUi() {
        seekBar = (SeekBar) findViewById(R.id.seekBar1);
        mStaticImage = (ImageView)findViewById(R.id.static_image);
        mChangingImage = (ImageView)findViewById(R.id.changing_image);
        mChangingImageTitle = (TextView)findViewById(R.id.changing_image_title);
        mStaticImageTitle = (TextView)findViewById(R.id.static_image_title);
        setUpImageTitles();

        mChangingImage.getLayoutParams().width = this.getResources().getDisplayMetrics().widthPixels;;
        mChangingImage.requestLayout();

        mToolbarTitle = (TextView)findViewById(R.id.toolbar_title);
        mBackButton = (ImageView)findViewById(R.id.back_button);
        setListeners();
        populateUi();
    }

    private void setListeners() {
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            @Override

            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                FrameLayout target = (FrameLayout) findViewById(R.id.target);

                progress = progresValue;

                ViewGroup.LayoutParams lp = target.getLayoutParams();
                lp.width = progress;
                target.setLayoutParams(lp);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void populateUi(){
        if (mTimeMachine == null) {
            return;
        }
        mToolbarTitle.setText(mTimeMachine.title);
        mTimeMachine.getTimeMachineFirstImage(false, new MethodHelper.ImageCallback() {
            @Override
            public void gotImage(byte[] image) {
                if (image != null) {
                    Glide.with(TimeMachineActivity.this).load(image).into(mChangingImage);
                }
            }
        });
        mTimeMachine.getTimeMachineSecondImage(false, new MethodHelper.ImageCallback() {
            @Override
            public void gotImage(byte[] image) {
                if (image != null) {
                    Glide.with(TimeMachineActivity.this).load(image).into(mStaticImage);
                }
            }
        });
    }

    private void setUpImageTitles(){
        mChangingImageTitle.setText(mTimeMachine.beforeTitle);
        mChangingImageTitle.setTextColor(Color.parseColor("#"+mTimeMachine.beforeTitleColor));
        mStaticImageTitle.setText(mTimeMachine.afterTitle);
        mStaticImageTitle.setTextColor(Color.parseColor("#"+mTimeMachine.afterTitleColor));
    }

    static void displayTimeMachineActivity(TimeMachine timeMachine, Context context){
        mTimeMachine = timeMachine;
        Intent i = new Intent(context, TimeMachineActivity.class);
        context.startActivity(i);
    }
}
