package com.netgalaxystudios.fortsumtertours;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jwetherell.augmented_reality.activity.AugmentedReality;
import com.jwetherell.augmented_reality.data.ARData;
import com.jwetherell.augmented_reality.ui.Marker;

import java.util.ArrayList;
import java.util.HashMap;

public class ARActivity extends AugmentedReality {
    private HashMap<Marker, PoI> cachedMarkers = new HashMap<Marker, PoI>();
    private TextView mHowToContinueBtn;
    private RelativeLayout mHowToLayout;
    private ImageView mHowToBackBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ARActivity.showRadar = false;
        ARActivity.showZoomBar = false;
        ui_portrait = false;
        zoomLayout.setVisibility(View.GONE);
        useMarkerAutoRotate = false;

    }

    private void connectUi() {
        mHowToContinueBtn = (TextView)findViewById(R.id.overlay_continue_btn);
        mHowToLayout = (RelativeLayout)findViewById(R.id.how_to_use_overlay);
        mHowToBackBtn = (ImageView)findViewById(R.id.overlay_back_btn);
        setListeners();
    }

    private void setListeners() {
        mHowToContinueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHowToLayout.setVisibility(View.GONE);
            }
        });
        mHowToBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void backButtonPressed() {
        super.backButtonPressed();
        onBackPressed();
    }

    @Override
    protected void markerTouched(Marker marker) {
        Log.d(Constants.TAG, "MARKER_TOUCHED");
        PoI poI = cachedMarkers.get(marker);
        if (poI != null) {
            Log.d(Constants.TAG, "CLICKED: "+poI.title);
            PoIDetailsActivity.display(poI, this);

        }else {
            Log.d(Constants.TAG, "POI_WAS_NULL");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (cachedMarkers.size() <= 0) {
            PoI.fetchPoIs(new PoI.PoICallback() {
                @Override
                public void gotPoIs(ArrayList<PoI> poIs) {
                    Log.d(Constants.TAG, "returned poIs size is: "+poIs.size());
                    for (PoI currentPoi:poIs) {
                        final PoI poI = currentPoi;
                        poI.getPoiPicture(new History.ImageCallback() {
                            @Override
                            public void gotImage(byte[] image) {
                                Marker marker = new Marker(ARActivity.this, poI.title, poI.bodyText, image, poI.coordinates.latitude, poI.coordinates.longitude, 0, Color.YELLOW);
                                cachedMarkers.put(marker, poI);
                                ARData.addMarkers(cachedMarkers.keySet());
                            }
                        });

                    }
                }
            });
        }
    }

    static void display(final Activity activity){
        MethodHelper.showProgressHUDWithMessage("Loading...", activity);
        MethodHelper.checkDistanceForViewfinder(activity, new MethodHelper.ViewfinderDistanceCallback() {
            @Override
            public void gotDistance(boolean withinRange) {
                MethodHelper.hideHUD();
                if (withinRange){
                    Intent i = new Intent(activity, ARActivity.class);
                    activity.startActivity(i);
                }else {
                    MethodHelper.showAlert(activity, "Error", "You're too far!");
                }
            }
        });
    }
}
