package com.netgalaxystudios.fortsumtertours;

import android.graphics.Bitmap;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by coryciepiela on 5/8/17.
 */

class FeaturedPhoto {
    boolean forMain;
    private ParseFile imageFile;
    String forObjectId;
    private byte[] imageData;

    private FeaturedPhoto(ParseObject object) {
        if (object.getBoolean(Constants.FeaturedPhoto.forMain) == Boolean.TRUE || object.getBoolean(Constants.FeaturedPhoto.forMain) == Boolean.FALSE) {
            forMain = object.getBoolean(Constants.FeaturedPhoto.forMain);
        }
        if (object.getParseFile(Constants.FeaturedPhoto.imageFile) != null) {
            imageFile = object.getParseFile(Constants.FeaturedPhoto.imageFile);
        }
        if (object.getString(Constants.FeaturedPhoto.forObjectId) != null) {
            forObjectId = object.getString(Constants.FeaturedPhoto.forObjectId);
        }
    }

    void getImage(final MethodHelper.GetImageCallback callback){
        if (imageFile != null){
            imageFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null){
                        callback.gotImage(data);
                    } else {
                        Log.e(Constants.TAG, "Error getting FortSumterImage: "+e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    static void fetchFeaturedPhotos(final GetFeaturedPhotosCallback callback) {
        final ArrayList<FeaturedPhoto> featuredPhotos = new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.FeaturedPhoto.className);
        query.whereEqualTo(Constants.FeaturedPhoto.forMain, true);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    for (ParseObject object:objects){
                        FeaturedPhoto featuredPhoto = new FeaturedPhoto(object);
                        featuredPhotos.add(featuredPhoto);
                    }
                    callback.gotFeaturedPhotos(featuredPhotos);
                } else {
                    Log.e(Constants.TAG, "Error fetching featuredPhotos: " + e.getMessage());
                    e.printStackTrace();
                    callback.gotFeaturedPhotos(null);
                }
            }
        });
    }

    interface GetFeaturedPhotosCallback {
        void gotFeaturedPhotos(ArrayList<FeaturedPhoto> featuredPhotos);
    }
}
